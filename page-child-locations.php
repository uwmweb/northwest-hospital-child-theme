<?php
/*
Template Name: Child Locations Page
*/
?>
<?php \Roots\Sage\Setup\define_current_template('page-child-locations.php'); ?>

<?php get_template_part('templates/page', 'header'); ?>

<?php
$service = ramp_service_linked();
$locations = $service["Facilities"];
?>
<div class="locations-list">
  <?php include(locate_template('templates/content-locations.php')); ?>
</div>
