<?php
/*
Template Name: Location Search Page
*/

use Roots\Sage\Setup;

Setup\define_current_template('page-ramp-search-location.php');
get_template_part('templates/page', 'header');

$ramp_search = ramp_search('clinics');
$locations = $ramp_search["clinics"];
$enabled_clinics = ramp_facilities_linked();

?>

<div class="search">
  <?php get_template_part('templates/search-form', 'locations'); ?>
</div>
<div class="locations-list">
  <?php include(locate_template('templates/content-locations.php')); ?>
</div>