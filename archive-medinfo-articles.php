<?php
/*
Template Name: MedInfo Homepage

*/
\Roots\Sage\Setup\define_current_template('archive-medinfo-articles.php');

$current_issue = get_field("current_medinfo_edition");
$current_issue_patient_story_id = null;
?>
<header class="container">
  <div id="logo">
    <h1 class="sr-only">MedInfo Magazine</h1>
    <img id="medinfo-logo" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/medinfo-logo.svg" alt="medinfo logo" />
  </div>
  <div id="edition">
    <p><?php if( $current_issue ) { echo $current_issue->name; }  ?> | News for the Northwest Hospital Community</p>
  </div>
</header>

<?php
$args = array(
  'post_type' => 'patient-stories',
  'numberposts' => 1,
  'tax_query' => array(
    array(
      'taxonomy' => 'medinfo-edition',
      'terms' => $current_issue
    )
  )
);
$current_issue_patient_story = new WP_Query($args);
if($current_issue_patient_story->have_posts() ) :
  while($current_issue_patient_story->have_posts() ) : ?>
    <?php
    $current_issue_patient_story->the_post();
    $current_issue_patient_story_id = get_the_ID();
    $homepage_headline	= get_field('medinfo_homepage_headline');
    $homepage_lead = get_field('medinfo_homepage_lead');
    $excerpt = get_field('medinfo_homepage_excerpt');
    $header_image = get_field('medinfo_homepage_image');
    ?>

    <?php if ($header_image) :
      $header_image_id = $header_image["id"];
      $size = "full";
      $header_image_src = wp_get_attachment_image_src( $header_image_id, $size );
      $header_image_src = $header_image_src[0];
      $srcset = wp_get_attachment_image_srcset($header_image_id);
      $sizes = "(max-width: 767px) 100vw, (min-width: 768px) 100vw, (min-width: 992px) 100vw, (min-width: 1200px) 100vw";?>

      <div id="header-image">
        <img src="<?php echo $header_image_src ?>" srcset="<?php echo $srcset ?>" sizes="<?php echo $sizes ?>" alt="<?php echo $header_image["alt"] ?>" />
        <div class="container">
          <div id="headline">
            <h2><a href="<?php echo the_permalink(); ?>"><?php echo $homepage_headline; ?></a></h2>
            <?php if($homepage_lead): ?><p><?php echo $homepage_lead; ?></p><?php endif ?>

          </div><!-- /.container -->
        </div><!-- /#headline -->
      </div><!-- /#header-image -->
    <?php endif ?><!-- /if $header_image -->

    <div class="container">
      <div id="content" class="row">
        <div class="col-md-8">
          <div id="excerpt">
            <?php echo $excerpt ?>
            <p><a class="btn btn-blue" href="<?php echo the_permalink(); ?>">Read More</a></p>
          </div><!-- /#excerpt -->
  <?php endwhile ?><!-- /while $current_issue_patient_story->have_posts -->
  <?php wp_reset_postdata(); ?>
<?php endif ?><!-- /if $current_issue_patient_story->have_posts -->
  <?php $args = array(
    'post_type' => array('medinfo-articles', 'patient-stories'),
    'post__not_in' => array($current_issue_patient_story_id),
    'tax_query' => array(
      array(
        'taxonomy' => 'medinfo-edition',
        'terms' => $current_issue,
      )
    ),
    'orderby' => array(
      'menu_order' => 'ASC'
    ),
    'meta_key' => 'hide_from_medinfo_homepage',
    'meta_value' => '0'
  );
  $current_issue_articles = new WP_Query($args);
  $post_count = $current_issue_articles->post_count;
  $current_issue_article_ids = array();
  $current_issue_article_ids[] = $current_issue_patient_story_id;
  if($current_issue_articles->have_posts() ) : ?>

  <?php $num_stories_printed = 0; ?>

  <?php  while($current_issue_articles->have_posts() ) : ?>
    <?php $current_issue_articles->the_post(); ?>
    <?php if( $num_stories_printed == 0 ) : ?>
      <?php // the first entry ?>
      <div class="row">
        <?php include(locate_template('/templates/content-medinfo-homepage-articles.php')); ?>
        <?php $current_issue_article_ids[] = get_the_id(); ?>
        <?php $num_stories_printed++; ?>
    <?php elseif($num_stories_printed % 2 !== 0) : ?>
      <?php // the second entry in a row and there are an even number of entries ?>
        <?php include(locate_template('/templates/content-medinfo-homepage-articles.php')); ?>
        <?php $current_issue_article_ids[] = get_the_id(); ?>
        <?php $num_stories_printed++; ?>
      </div>
    <?php elseif($num_entries_printed % 2 == 0 ) : ?>
      <?php // the first entry in a row ?>
      <div class="row">
        <?php include(locate_template('/templates/content-medinfo-homepage-articles.php')); ?>
        <?php $current_issue_article_ids[] = get_the_id(); ?>
        <?php $num_stories_printed++; ?>
        <?php if($num_stories_printed == $post_count) : ?>
          <?php // the last entry and there are an odd number of entries ?>
          </div>
        <?php endif; ?>
      <?php endif;?>
    <?php endwhile ?><!--/while current_issue_articles->have_posts() -->
    <?php wp_reset_postdata(); ?>
  <?php endif ?><!-- /if $current_issue_articles->have_posts() -->
  <?php $current_issue_article_ids[] = url_to_postid(home_url('medinfo/archive'));
  $current_issue_article_ids[] = url_to_postid(home_url('medinfo/subscribe')); ?>
    <?php $args = array(
      'post_type' => array('patient-stories', 'medinfo-articles'),
      'posts_per_page' => 3,
      'orderby' => array('date' => 'DESC'),
      'post__not_in' => $current_issue_article_ids,
      'meta_key' => 'hide_from_medinfo_homepage',
      'meta_value' => '0'
    );
    $past_issue_articles = new WP_Query($args);
    if($past_issue_articles->have_posts() ) : ?>
    <div id="past-stories" class="hidden-xs hidden-sm">
      <h3>From the previous issue:</h3>
      <div class="row">
    <?php  while($past_issue_articles->have_posts() ) : ?>
        <?php
        $past_issue_articles->the_post();
        $header_image = get_field('image');
        $header_image_id = $header_image["id"];
        $size = "full";
        $header_image_src = wp_get_attachment_image_src( $header_image_id, $size );
        $header_image_src = $header_image_src[0];
        $srcset = wp_get_attachment_image_srcset($header_image_id);
        $sizes = "(max-width: 767px) 100vw, (min-width: 768px) 40vw, (min-width: 992px) 40vw, (min-width: 1200px) 40vw";
        $title	= get_field('medinfo_homepage_headline') ? get_field('medinfo_homepage_headline') : get_the_title();
        ?>

        <div class="past-story">
          <a href="<?php the_permalink() ?>">
            <img class="story-image" src="<?php echo $header_image_src ?>" srcset="<?php echo $srcset ?>" sizes="<?php echo $sizes ?>" alt="<?php echo $header_image["alt"] ?>" />
          </a>
          <h2><?php echo $title; ?></h2>
          <a href="<?php the_permalink() ?>">Read More &raquo;</a>
        </div>
      <?php endwhile ?><!--/while current_issue_articles->have_posts() -->
      <p><a class="btn btn-green" href="<?php echo  trailingslashit(home_url('medinfo/archive')) ?>">See Previous Issues</a></p>
    </div><!--/.row -->
      <?php wp_reset_postdata(); ?>


    </div><!-- /#past-stories -->
    <?php endif ?><!-- /if $past_issue_articles->have_posts() -->
        </div><!-- /.col-md-8 -->
        <div class="col-md-4">
          <div id="right-sidebar">
            <?php get_template_part('templates/medinfo-subscribe-cta'); ?>
            <div class="featured">
              <?php if( have_rows('save_the_date')) :
                while(have_rows('save_the_date')) : the_row();?>
                <h3>Save the Date</h3>
                <h4><?php the_sub_field('title'); ?></h4>
                <div class="featured-content">
                  <p>
                    <?php $image = get_sub_field('image'); ?>
                    <img src="<?php echo $image["url"] ?>" alt="<?php echo $image["alt"] ?>" />
                    <?php the_sub_field('date'); ?>
                  </p>
                  <p><?php the_sub_field('description'); ?></p>
                  <a href="<?php the_sub_field('call_to_action_url') ?>"><?php the_sub_field('call_to_action_text') ?></a>
                </div>
              <?php endwhile;
              endif;?>

              <?php if( have_rows('featured_class')) :
                while(have_rows('featured_class')) : the_row();?>
                <h3>Featured Class</h3>
                <div class="featured-content">
                  <h4><?php the_sub_field('title'); ?></h4>
                  <p>
                    <?php $image = get_sub_field('image'); ?>
                    <img src="<?php echo $image["url"] ?>" alt="<?php echo $image["alt"] ?>" />
                    <?php the_sub_field('date'); ?>
                  </p>
                  <p><?php the_sub_field('description'); ?></p>
                  <p><a href="<?php the_sub_field('call_to_action_url') ?>"><?php the_sub_field('call_to_action_text') ?></a></p>


                </div>
              <?php endwhile;
              endif;?>
            </div><!--/.featured -->

            <?php include (locate_template('/templates/meet-the-docs.php')); ?>
          </div><!-- /#right-sidebar -->
        </div><!-- /.col-md-4 -->
    </div><!-- /.row -->
  </div><!--.container
