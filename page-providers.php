<?php /*The loading of this template is determined by page-bios.php */ ?>
<?php \Roots\Sage\Setup\define_current_template('page-providers.php'); ?>
<?php use Roots\Sage\NWH_Extras; ?>

<script>window.sessionStorage.setItem("isProviderSearchPage", "false");</script>
<?php $bios_url = NWH_Extras\get_provider_url(); ?>
<p id="breadcrumbs">
  <span xmlns:v="http://rdf.data-vocabulary.org/#">
    <span typeof="v:Breadcrumb">
      <a href="http://nwhospital.wpengine.com" rel="v:url" property="v:title">Home</a> &raquo;
      <span rel="v:child" typeof="v:Breadcrumb">
        <a href="<?php echo $bios_url ?>" rel="v:url" property="v:title">Care Providers</a> &raquo;
          <span class="breadcrumb_last">
            <?php echo $provider["FullName"] ?>
          </span>
      </span>
    </span>
  </span>
</p>

<h1><?php echo $provider["FullName"] ?></h1>
<section class="provider">
  <img class="provider-photo" src="<?php echo NWH_Extras\get_provider_image($provider["ImageUrl"]) ?>" alt="<?php echo $provider["FullName"] ?>" />

  <?php if($provider["ShowAppointmentRequest"] == 1 || $provider["ShowReturningPatient"] == 1 || $provider["AcceptingAppointments"] == 1) : ?>
    <div class="uwm-widgets">
      <?php if($provider["ShowAppointmentRequest"] == 1) : ?>
        <a class="request-an-appointment widget" target="_blank" href="https://www.uwmedicine.org/request-an-appointment?provider=<?php echo (urlencode(trim($provider["FullName"])) . "&" . "providerid=" . $provider["Id"]) ?>">Request an Appointment</a>
      <?php endif ?>
      <?php if ($provider["ShowReturningPatient"] == 1) : ?>
        <a class="eCare widget" href="http://www.uwmedicineecare.org">Log in to eCare</a>
      <?php endif ?>
      <?php if( $provider["AcceptingAppointments"] == 1 ) : ?>
        <p class="accepting-patients"><i class="fa fa-check-square-o "></i> <em>Accepting new patients</em></p>
      <?php endif ?>
    </div>
  <?php endif ?><!-- /ShowAppointmentRequest, ShowReturningPatient -->

  <?php if(!empty($provider["Languages"]) || !empty($provider["Specialties"]) || !empty($provider["Expertises"]) ) : ?>
  <div class="provider-data">
  <?php if(!empty($provider["Languages"])) : ?>
    <div class="provider-languages">
    <?php if(count($provider["Languages"]) > 1) : ?>
      <h3>Languages</h3>
    <?php else : ?>
      <h3>Language</h3>
    <?php endif ?>
      <ul class="list-unstyled">
      <?php foreach($provider["Languages"] as $language ) : ?>
        <li><?php echo $language["Name"]; ?></li>
      <?php endforeach ?><!-- /languages as language -->
      </ul>
    </div>
  <?php endif ?><!-- /Languages -->

  <?php if(!empty($provider["Expertises"])) : ?>
     <div class="provider-expertise">
       <?php if(count($provider["Expertises"]) > 1) : ?>
         <h3>Specialties</h3>
       <?php else : ?>
         <h3>Specialty</h3>
       <?php endif ?>
       <ul class="list-unstyled">
       <?php foreach($provider["Expertises"] as $expertise ) : ?>
         <li><?php echo $expertise["Name"]; ?></li>
       <?php endforeach ?><!-- /expertises as expertise -->
       </ul>
    </div>
  <?php endif ?><!-- /Expertises -->
</div>
<?php endif ?><!-- /Languages, Specialties, Expertises -->


  <div class="content">

    <?php if ( $provider["FullBio"] ) : ?>
      <h3>Bio</h3>
      <p><?php echo $provider["FullBio"] ?></p>
    <?php endif ?><!-- /FullBio -->



    <?php if ( $provider["PatientCarePhilosophy"] ) : ?>
      <h3>Patient Care Philosophy</h3>
      <p><?php echo $provider["PatientCarePhilosophy"] ?></p>
    <?php endif ?><!-- /PatientCarePhilosophy -->

    <?php if ( $provider["ClinicalInterests"] ) : ?>
      <h3>Clinical Interests</h3>
      <p><?php echo $provider["ClinicalInterests"] ?></p>
    <?php endif ?><!-- /ClinicalInterests -->

    <?php if ( $provider["ResearchInterests"] ) : ?>
      <h3>Research Interests</h3>
      <p><?php echo $provider["ResearchInterests"] ?></p>
    <?php endif ?><!-- /ResearchInterests -->

    <?php $education = $provider["Education"]; ?>
    <?php if ( $education ) : ?>
      <h3 id="education">Education</h3>
      <table class="table table-striped table-bordered">
    		<tr>
    			<th>Institution</th>
          <th>Credential</th>
          <th>Year</th>
    		</tr>
        <?php foreach ( $education as $entry ) : ?>
        <tr>
    			<td><?php echo $entry["Institution"]; ?></td>
          <td><?php echo $entry["Type"]; ?></td>
          <td><?php if ($entry["Year"] == "0") : echo ""; else: echo $entry["Year"]; endif; ?></td>
    		</tr>
      <?php endforeach ?><!-- /education as entry -->
    	</table>
    <?php endif ?><!-- /education -->

    <?php $degrees = $provider["Degrees"]; ?>
    <?php if ( $degrees ) : ?>
      <table class="table table-striped table-bordered">
    		<tr>
    			<th>Degree</th>
          <th>Institution</th>
          <th>Year</th>
    		</tr>
        <?php foreach ( $degrees as $entry ) : ?>
        <tr>
    			<td><?php echo $entry["Degree"]; ?></td>
          <td><?php echo $entry["Institution"]; ?></td>
          <td><?php if ($entry["Year"] == "0") : echo ""; else: echo $entry["Year"]; endif; ?></td>
    		</tr>
      <?php endforeach ?> <!-- degrees as entry -->
    	</table>
    <?php endif ?><!-- /degrees -->


    <?php $certifications = $provider["BoardCertifications"]; ?>
    <?php if ( $certifications ) : ?>
      <h3>Board Certifications</h3>
      <table class="table table-striped table-bordered">
    		<tr>
    			<th>Certification</th>
          <th>Specialty</th>
          <th>Year</th>
    		</tr>
        <?php foreach ( $certifications as $entry ) : ?>
        <tr>
    			<td><?php echo $entry["boardName"]; ?></td>
          <td><?php echo $entry["specialtyName"]; ?></td>
          <td><?php if ($entry["certifiedYear"] == "0") : echo ""; else: echo $entry["certifiedYear"]; endif; ?></td>
    		</tr>
      <?php endforeach ?><!-- /certifications as entry -->
      </table>
    <?php endif ?><!-- /certifications -->

    <?php $publications = $provider["Publications"]; ?>
    <?php if ( $publications ) : ?>
      <h3>Publications</h3>
        <?php foreach ( $publications as $entry ) : ?>

          <p><?php echo( $entry["Authors"] . " <em>" . $entry["ArticleTitle"] . "</em> " .
                         $entry["JournalYear"] . " " . $entry["JournalMonth"] . "; " .
                         $entry["JournalVolume"] . "; " . $entry["JournalIssue"] . "; " .
                         $entry["PagingNumber"] ) ?>

      <?php endforeach ?><!-- /publications as entry -->
    <?php endif ?><!-- /publications -->
  </div><!-- .content -->
</section>
