<?php \Roots\Sage\Setup\define_current_template('archive-press-releases.php'); ?>

<p id="breadcrumbs">
  <span xmlns:v="http://rdf.data-vocabulary.org/#">
    <span typeof="v:Breadcrumb">
      <a href="http://nwhospital.wpengine.com" rel="v:url" property="v:title">Home</a> &raquo;
      <span rel="v:child" typeof="v:Breadcrumb">
        <a href="<?php echo trailingslashit(home_url('about-northwest-hospital')) ?>" rel="v:url" property="v:title">About Northwest Hospital</a> &raquo;
          <span class="breadcrumb_last">
            <?php echo "News" ?>
          </span>
      </span>
    </span>
  </span>
</p>

<div class="page-header">
  <h1>News</h1>
</div>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php the_posts_pagination( array(
  'screen_reader_text' => 'Press Release Page Navigation',
  'next_text' => 'Older <span class="hidden-xs">Press Releases</span> &raquo;',
  'prev_text' => '&laquo; Newer <span class="hidden-xs">Press Releases</span>',
) ); ?>
