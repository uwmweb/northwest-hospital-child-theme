<?php
/**
 * Template Name: Landing Page
 */
?>
<?php use \Roots\Sage\NWH_Extras; ?>
<?php \Roots\Sage\Setup\define_current_template('page-landing.php'); ?>
  <div id="content">
      <?php the_content(); ?>
  </div>

