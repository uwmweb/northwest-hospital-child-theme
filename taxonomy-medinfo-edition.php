<?php \Roots\Sage\Setup\define_current_template('taxonomy-medinfo-edition.php'); ?>

<?php
  global $wp_query;
  $term = $wp_query->get_queried_object();
  $edition = $term->name; ?>

<p id="breadcrumbs">
  <span xmlns:v="http://rdf.data-vocabulary.org/#">
    <span typeof="v:Breadcrumb">
      <a href="http://nwhospital.wpengine.com" rel="v:url" property="v:title">Home</a> &raquo;
      <span rel="v:child" typeof="v:Breadcrumb">
        <a href="<?php echo trailingslashit(home_url('medinfo')) ?>" rel="v:url" property="v:title">MedInfo</a> &raquo;

            <span class="breadcrumb_last">
              <?php echo $edition; ?>
            </span>

      </span>
    </span>
  </span>
</p>


<div class="page-header">
  <h1>MedInfo Magazine <?php echo $edition; ?></h1>
</div>
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<div class="patient-story-row">
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>
</div>
