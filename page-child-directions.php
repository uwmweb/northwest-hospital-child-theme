<?php
/**
 * Template Name: Child Directions page
 */
?>
<?php \Roots\Sage\Setup\define_current_template('page-child-providers.php'); ?>

<?php
  $location = ramp_facility_linked();
  $location_page_title = get_the_title(wp_get_post_parent_id($post->ID));
  $building_name = $location["LocationName"];

  $address_string = $location["AddressLine1"] . " ";
  if($location["AddressLine2"] ) { $address_string .= $location["AddressLine2"] . " "; }
  if($location["RoomNumber"] ) { $address_string .= $location["RoomNumber"] . " "; }
  $address_string .= $location["City"] . ", " . $location["State"] . " " . trim($location["ZipCode"], " ");
?>

<?php if ( function_exists('yoast_breadcrumb') )
{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

<div class="page-header">
  <h1><?php echo $location_page_title; ?></h1>
</div>

<h2><?php echo get_the_title(); ?></h2>
<?php the_content(); ?>

<div id="map-container">
  <iframe width="100%" height="100%" src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode($address_string)?>&amp;zoom=14&amp;key=AIzaSyAClfj1QHSxjcwOYbXdrwT4w9ocopzoJo0"></iframe>
</div>

<div id="address">
  <h3><?php echo $building_name ?></h3>
  <p><?php echo $location["AddressLine1"]; ?><br />
    <?php if($location["AddressLine2"]) { echo $location["AddressLine2"] . "<br />"; } ?>
    <?php if($location["RoomNumber"]) { echo $location["RoomNumber"] . "<br />"; } ?>
    <?php echo $location["City"] ?>, <?php echo $location["State"] ?> <?php echo $location["ZipCode"] ?><br />
    <?php $phone_number = $location["PhoneNumber"]; ?>
    <a class="phone" href="tel:+1<?php echo $phone_number; ?>">
    <?php echo substr($phone_number, 0, 3);
    echo ".";
    echo substr($phone_number, 3, 3);
    echo ".";
    echo substr($phone_number, 6, 4);?></a></p>
</div>

<?php if( $location["DrivingDirections"] ) : ?>
  <h3>Driving Directions to <?php echo $building_name; ?></h3>
  <?php echo $location["DrivingDirections"] ?>
<?php endif ?>

<?php if( $location["BusDirections"] ) : ?>
  <h3>Bus Directions to <?php echo $building_name; ?></h3>
  <?php echo $location["BusDirections"] ?>
<?php endif ?>

<?php if( $location["ParkingDirections"] ) : ?>
  <h3>Parking at <?php echo $building_name; ?></h3>
  <?php echo $location["ParkingDirections"] ?>
<?php endif ?>

<?php if( $location["AccessibilityNotes"] ) : ?>
  <h3>Accessibility Notes</h3>
  <?php echo $location["AccessibilityNotes"] ?>
<?php endif ?>

<?php if( $location["AdditionalInformation"] ) : ?>
  <h3>Additional Information</h3>
  <?php echo $location["AdditionalInformation"] ?>
<?php endif ?>
