<?php $parent_template = get_page_template_slug( $post->post_parent );
$parent_heading = get_the_title($post->post_parent);
$parent_link = get_permalink($post->post_parent); ?>

<article class="page-entry">
  <header>
    <h2 class="entry-title">
      <a href="<?php the_permalink(); ?>">
        <?php the_title(); ?>
        <?php if( $parent_template === "page-location.php") { echo "&mdash; " . $parent_heading; } ?>
      </a></h2>
    <?php if( $post->post_parent && ($parent_template !== "page-location.php") ) : ?>
      <p class="search-filed-under">Filed Under: <a href="<?php echo $parent_link ?>"><?php echo $parent_heading ?></a></p>
    <?php endif ?>
    <?php if (get_post_type() === 'post') { get_template_part('templates/entry-meta'); } ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>
