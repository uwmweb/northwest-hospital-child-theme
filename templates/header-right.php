
<!-- <?php if( is_active_sidebar('header-right')) : ?>
  <section class="appointments-cta">
    <?php dynamic_sidebar('header-right'); ?>
  </section>
<?php endif ?> -->

<?php if( ! is_front_page() ) : ?>
<div class="search-form">
  <form role="search" method="get" class="search-form" action="<?php echo home_url( 'search', 'relative' ); ?>">
  <div class="input-group">
    <label>
      <span class="sr-only"><?php echo _x( 'Search :', 'label' ) ?></span>
      <input id="typeahead-header" type="search" class="form-control search-field typeahead"
          placeholder="<?php echo esc_attr_x( 'Search for...', 'placeholder' ) ?>"
          value="<?php echo trim(esc_attr(get_query_var("query"))) ?>" name="query"
          title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
    </label>
    <span class="input-group-btn"  >
      <button class="btn" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="sr-only">Search</span></button>
    </span>
  </div>
</form>
</div>
<?php endif; ?>

<?php if (is_active_sidebar('make-a-gift')) : ?>
  <div class="make-a-gift">
    <?php dynamic_sidebar('make-a-gift'); ?>
  </div>
<?php endif; ?>
