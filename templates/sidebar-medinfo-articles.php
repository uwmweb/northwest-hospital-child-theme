<?php $edition = get_the_terms( $post->ID,'medinfo-edition');
$edition = $edition[0];?>
<?php if($edition) : ?>
  <div id="sidebar-nav-toggle">
    <h3 id="sidebar-nav-heading">
      <a id="sidebar-nav-heading-link" href="<?php echo get_term_link($edition) ?>">MedInfo <?php echo $edition->name; ?> <span class="visible-xs-inline">Menu <i id="sidebar-nav-heading-down-arrow" class="fa fa-chevron-down"></i></span></a>
    </h3>
  </div>

  <?php $patient_story_args = array(
    'post_type' => array('patient-stories'),
    'tax_query' => array(
      array(
        'taxonomy' => 'medinfo-edition',
        'terms' => $edition
      )
    ),
    'orderby' => array(
      'menu_order' => 'ASC'
    )
  );
  $medinfo_articles_args = array(
    'post_type' => array('medinfo-articles'),
    'tax_query' => array(
      array(
        'taxonomy' => 'medinfo-edition',
        'terms' => $edition
      )
    ),
    'orderby' => array(
      'menu_order' => 'ASC'
    )
  );
  $edition_patient_stories = new WP_Query($patient_story_args);
  $edition_articles = new WP_Query($medinfo_articles_args);
  if($edition_articles->have_posts() || $edition_patient_stories->have_posts() ) : ?>
    <ul class="mobile-collapse">
      <?php while($edition_patient_stories->have_posts() ) :
        $edition_patient_stories->the_post() ?>
        <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
      <?php endwhile ?>

      <?php while($edition_articles->have_posts() ) :
        $edition_articles->the_post() ?>
        <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
      <?php endwhile ?>
    </ul>
  <?php endif ?>
<?php else : ?>

  <div id="sidebar-nav-toggle">
    <h3 id="sidebar-nav-heading">
      <a id="sidebar-nav-heading-link" href="<?php echo trailingslashit(home_url('medinfo')) ?>">MedInfo Magazine <span class="visible-xs-inline">Menu <i id="sidebar-nav-heading-down-arrow" class="fa fa-chevron-down"></i></span></a>
    </h3>
  </div>

  <ul class="mobile-collapse">
    <li <?php echo (is_page('archive') ? 'class="current_page_item"' : ""); ?>><a href="<?php echo trailingslashit(home_url('medinfo/archive')) ?>">Archive</a></li>
    <li <?php echo (is_page('subscribe') ? 'class="current_page_item"' : ""); ?>><a href="<?php echo trailingslashit(home_url('medinfo/subscribe')) ?>">Subscribe</a></li>
  </ul>
<?php endif ?>
