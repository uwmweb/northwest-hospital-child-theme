<?php $edition = get_the_terms($post->ID, 'medinfo-edition'); ?>
<?php if($edition) : ?>
  <?php include(locate_template('/templates/sidebar-medinfo-articles.php')); ?>
  <div class="hidden-xs">
<?php endif ?>
  <div id="sidebar-nav-toggle">

    <h3 id="sidebar-nav-heading">
      <a id="sidebar-nav-heading-link" href="<?php echo trailingslashit(site_url())?>patient-stories">Patient Stories <span class="visible-xs-inline">Menu <i id="sidebar-nav-heading-down-arrow" class="fa fa-chevron-down"></i></span></a>
    </h3>
  </div>
  <?php $args = array(
      'taxonomy' => 'patient-story-category',
      'title_li' => ''
    ); ?>
  <ul class="mobile-collapse">
    <?php wp_list_categories($args); ?>
  </ul>

  <p class="hidden-xs"><a id="share-patient-story" class="btn btn-gold" href="<?php echo trailingslashit(home_url()); ?>about-northwest-hospital/patient-stories/share-your-story/">Share Your Story</a></p>
<?php if(isset($edition)) : ?>
</div>
<?php endif ?>
