<div id="sidebar-nav-toggle">
  <h3 id="sidebar-nav-heading">
    <a id="sidebar-nav-heading-link-xs" href="#/"><?php echo $location["Name"]; ?> Menu <i id="sidebar-nav-heading-down-arrow" class="fa fa-chevron-down"></i></a>
    <a id="sidebar-nav-heading-link-sm" href="#/">About This Clinic</a>
  </h3>
</div>

<ul class="mobile-collapse">
  <?php
    wp_list_pages( array('title_li'=>'','depth'=>0,'child_of'=>$post->ID, 'sort_column'=>'menu_order'));
  ?>
</ul>
