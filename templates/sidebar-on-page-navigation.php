<?php
use Roots\Sage\NWH_Extras;
  /*
  * On-page navigation will vary depending on where a page is in the tree.
  *
  * In general, at most two levels of navigation will be displayed on a page.
  *
  * The on-page navigation for a page at the third, fourth or deeper level of
  * the hierarchy will begin with the page's immediate parent unless a custom
    nav parent is set.
  *
  * If a parent page is set to serve as the custom nav parent
  * for all its descendants (controlled on the page edit page), the parent page
  * and all its descendants will show on-page navigation from the parent
  * to the deepest level in that family.
  *
  * For examples, please see the site documentation page at /documentation.
  */

  // set the context for the current page
  $ancestors = get_post_ancestors($post->ID);
  $num_ancestors = count($ancestors);
  $descendants = get_pages(array('child_of' => $post->ID));

  // initialize variables for echoing
   $nav_heading;
   $nav_heading_link;
   $nav_pages;

  // pages hidden from navigation globally
  $pages_hidden_from_nav = get_posts(array(
    'post_type' => 'page',
    'numberposts' => -1,
    'meta_key' => 'hide_from_on_page_navigation',
    'meta_value' => 1,
    'meta_compare' => '='
  ));
  $pages_hidden_from_nav = wp_list_pluck($pages_hidden_from_nav, 'ID');
  $hide_pages_from_nav = implode(",", $pages_hidden_from_nav);

  // initalize variables to check for nav parent
  $nav_parent = null;
  $set_as_custom_nav_parent = get_field('navigation_parent_all_descendants', $post->ID);

  /* Check for medical services 'provider and location' setting
   * If enabled, set this page as nav_parent
   */
   //$add_medical_service_provider_location = NWH_Extras\add_medical_services_nav($post->ID);

  if( $set_as_custom_nav_parent ) {
    // set this page as nav parent
    $nav_parent = $post;

  }
  else {

    // check for a nav parent further up the tree
    foreach( $ancestors as $ancestor_id ) {
      $set_as_custom_nav_parent = get_field('navigation_parent_all_descendants', $ancestor_id);

      if( $set_as_custom_nav_parent ) {
        // we found a nav parent further up the tree, this nav parent wins
        $nav_parent = get_post($ancestor_id);
      }
    }
  }

  if( $nav_parent ) {
    /* there's a custom nav parent set
       set the custom parent as the nav parent and display all its descendants */

    $nav_heading = get_the_title($nav_parent);
    $nav_heading_link = get_permalink($nav_parent);
    $nav_depth = 0;

    $nav_heading_item = "<a href='" . $nav_heading_link . "'> " . $nav_heading . " Home</a>";
    $nav_pages = "<li class='visible-xs-block'>" . $nav_heading_item . "</li>";

    // if( $add_medical_service_provider_location ) {
    //   $nav_pages .=
    //       "<li><a href='#/'>Providers</a></li>
    //       <li><a href='#/'>Locations</a></li>";
    //
    //   $nav_depth = 1;
    // }

    $nav_pages .= wp_list_pages( array('title_li'=>'','depth'=>$nav_depth, 'child_of'=>$nav_parent->ID, 'exclude' => $hide_pages_from_nav, 'sort_column'=>'menu_order', 'echo'=>0 ));



  }
  else {

    // there's no custom nav parent set, let's figure out which nav parent to set
    if( !$ancestors ) {
      /* this is a top-level page
         set this page as nav parent and display its children */
      $nav_parent = $post;

    }
    elseif ($num_ancestors == 1 && !$descendants) {
      /* this is a second-level page with no children
         set this page's parent as nav parent and display its children */
      $nav_parent = get_post($post->post_parent);
    }
    elseif ($num_ancestors == 1 && $descendants) {
      /* this is a second-level page with children
         set this page as nav parent and display its children */
      $nav_parent = $post;

    }
    elseif ($num_ancestors > 1) {
      // this page is three levels or deeper because it has at least 2 parents
      if ($num_ancestors == 2 && $descendants) {
        /* this is a third-level page with children
           set this page as nav parent and display its children */
        $nav_parent = $post;

      } else {
        /* this is a third-level page or deeper that may or may not have children
           set this page's parent as nav parent and display its children, if any
           these pages will likely have a custom nav parent set */
        $nav_parent = get_post($ancestors[0]);
      }
    }

    // we found the nav_parent
    // set heading, headding link, and nav_pages based on nav_parent
    $nav_heading = get_the_title($nav_parent);
    $nav_heading_link = get_permalink($nav_parent);
    $nav_depth = 1;

    $nav_heading_item = "<a href='" . $nav_heading_link . "'> " . $nav_heading . " Home</a>";
    $nav_pages = "<li class='visible-xs-block'>" . $nav_heading_item . "</li>";

    // if( $add_medical_service_provider_location ) {
    //   $nav_pages .=
    //       "<li><a href='#/'>Providers</a></li>
    //       <li><a href='#/'>Locations</a></li>";
    // }

    $nav_pages .= wp_list_pages( array('title_li'=>'','depth'=>$nav_depth, 'child_of'=>$nav_parent->ID, 'exclude' => $hide_pages_from_nav, 'sort_column'=>'menu_order', 'echo'=>0 ));
  }
?>
<div id="sidebar-nav-toggle">
  <h3 id="sidebar-nav-heading">
    <a id="sidebar-nav-heading-link" href="<?php echo $nav_heading_link; ?>"><?php echo $nav_heading; ?> <span class="visible-xs-inline">Menu <i id="sidebar-nav-heading-down-arrow" class="fa fa-chevron-down"></i></span></a>
  </h3>
</div>

<ul class="mobile-collapse">
  <?php echo $nav_pages; ?>
</ul>

<?php $left_sidebar = get_field("left_sidebar");
  if ($left_sidebar) {
      echo $left_sidebar;
  }
?>

<?php get_template_part('templates/sidebar', 'key-contacts'); ?>
