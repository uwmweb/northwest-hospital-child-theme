<?php \Roots\Sage\Setup\define_current_template('content-single-clinic-resources.php'); ?>

<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
    </header>

    <?php
    $right_sidebar = get_field("right_sidebar");
    $has_widgets = have_rows("right_sidebar_widgets");

    if ( $right_sidebar || $has_widgets ) : ?>
      <div class="row">
        <div id="content" class="col-md-8">
          <?php the_content(); ?>
        </div>
        <div id="right-sidebar" class="col-md-4">
          <?php get_template_part("templates/sidebar-right-widgets"); ?>
          <?php echo $right_sidebar; ?>
        </div>
      </div>
    <?php else : ?>
      <?php the_content(); ?>
    <?php endif; ?>
  </article>
<?php endwhile; ?>

<p><a id="goBack" class="btn btn-blue">&laquo; Back</a></p>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('a#goBack').click(function(){
		parent.history.back();
		return false;
	});
});
</script>
