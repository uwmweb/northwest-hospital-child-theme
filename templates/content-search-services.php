<h2>Medical Services</h2>
  <ul class="list-group row">
    <?php foreach($services as $service) : ?>
      <li class="list-group-item col-xs-6">
        <a href="<?php echo($service["WpUrl"]) ?>"><?php echo($service["Title"]) ?></a>
      </li>
    <?php endforeach ?>
  </ul>
