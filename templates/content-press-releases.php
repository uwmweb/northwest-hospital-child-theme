<div class="press-release-entry">
	<p><?php the_title(); ?></p>
	<p><a href="<?php the_permalink(); ?>">NWH Press Release, <?php the_field('original_release_date'); ?></a></p>
</div>
