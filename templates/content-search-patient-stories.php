<article class="patient-story-entry">
  <header>
    <?php $parent_heading = get_the_title($post->post_parent);
    $parent_link = get_permalink($post->post_parent);
    $header_image = get_field("image"); ?>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>">Patient Story: <?php the_title(); ?></a></h2>
  </header>
  <div class="entry-summary">
    <?php if ($header_image) : ?>
      <img src="<?php echo esc_url($header_image["url"]) ?> ?>" />
    <?php endif ?>
    <?php the_excerpt(); ?>
  </div>
</article>
