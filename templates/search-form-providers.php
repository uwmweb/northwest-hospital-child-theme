<?php use Roots\Sage\NWH_Extras; ?>
<form role="search" method="get" id="provider-search" class="search-form" action="<?php echo NWH_Extras\get_provider_url(); ?>">
  <div class="form-group">
    <div id="query" class="input-group">
      <label>
        <span class="sr-only"><?php echo _x( 'Search :', 'label' ) ?></span>
        <input id="typeahead-providers" type="search" class="form-control search-field typeahead"
            placeholder="<?php echo esc_attr_x( 'Search by provider name or specialty', 'placeholder' ) ?>"
            value="<?php echo trim(esc_attr(get_query_var("query"))) ?>" name="query"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
      </label>
      <span class="input-group-btn">
        <button class="btn" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="sr-only">Search</span></button>
      </span>
    </div><!-- /.input-group -->
  </div>
  <div class="form-group">
    <div id="filters">
      <div id="specialty" class="input-group">
        <label for="specialtySelect" class="sr-only">Specialty</label>
        <select name="specialty" id="specialtySelect" class="form-control"><option value="" selected>Filter by Specialty</option>
          <option value='63,140'>Allergy &amp; Immunology</option>
          <option value='51'>Anesthesiology</option>
          <option value='174,173'>Behavioral Health</option>
          <option value='105,153,189,252,125,52,145,99,295,146,154,155,158,156'>Cancer Care</option>
          <option value='7,130,86'>Cardiac Services/Cardiology</option>
          <option value='200,198'>Colorectal Services</option>
          <option value='101'>Critical Care Medicine</option>
          <option value='11'>Dermatology</option>
          <option value='12,14,138'>Diabetes Care</option>
          <option value='33'>Ear, Nose &amp; Throat (Otolaryngology)</option>
          <option value='286,276'>Electrophysiology-Arrhythmias</option>
          <option value='13,291'>Emergency Medicine</option>
          <option value='12,14,138'>Endocrinology</option>
          <option value='31'>Eye Care</option>
          <option value='53,36'>Family Medicine</option>
          <option value='17'>Gastroenterology</option>
          <option value='172,19'>Geriatrics</option>
          <option value='54,125'>Gynecology</option>
          <option value='21'>Hematology</option>
          <option value='22'>Hepatology</option>
          <option value='201'>Hospice &amp; Palliative Medicine</option>
          <option value='79'>Hospital Medicine</option>
          <option value='2'>Infectious Disease Medicine</option>
          <option value='23'>Internal Medicine</option>
          <option value='25'>Kidney Care</option>
          <option value='165'>Lactation Services</option>
          <option value='38'>Lung Care</option>
          <option value='126'>Midwifery</option>
          <option value='25'>Nephrology</option>
          <option value='77,16,24,26,109,110,191,45'>Neurology</option>
          <option value='27,104'>Neurosurgery</option>
          <option value='28,126'>Nutrition</option>
          <option value='126,102'>Obstetrics &amp; Gynecology</option>
          <option value='105,154,155,153,158,189,156,252,125,52,145,99,295,146'>Oncology</option>
          <option value='190,31'>Ophthalmology</option>
          <option value='277,57'>Oral &amp; Maxillofacial Surgery</option>
          <option value='92,32,93,95,96,94,304,98,133,134'>Orthopedics</option>
          <option value='8,60'>Pain Medicine</option>
          <option value='62,59'>Pathology</option>
          <option value='34'>Pediatrics</option>
          <option value='121'>Physical Therapy</option>
          <option value='139,9,183'>Plastic &amp; Reconstructive Surgery </option>
          <option value='89'>Podiatry</option>
          <option value='65,35'>Preventive Medicine</option>
          <option value='53,36'>Primary Care</option>
          <option value='37'>Psychiatry</option>
          <option value='38'>Pulmonology</option>
          <option value='58'>Radiation Oncology</option>
          <option value='285,3,39'>Radiology &amp; Imaging Services</option>
          <option value='159,299,49'>Reproductive Care</option>
          <option value='56'>Rheumatology</option>
          <option value='42,149'>Sleep Medicine</option>
          <option value='43'>Spine</option>
          <option value='44'>Sports Medicine</option>
          <option value='215,114,199,216,214,46,76'>Surgery</option>
          <option value='130,303'>Thoracic &amp; Cardiovascular Surgery</option>
          <option value='210,204,207,47'>Transplantation Services</option>
          <option value='48'>Travel Medicine</option>
          <option value='187,49'>Urology</option>
          <option value='74,50'>Vascular Surgery</option>
          <option value='66,176'>Weight-Loss Surgery/Bariatrics</option>
          <option value='159,54,126,102,73'>Women's Health</option>
          <option value='181'>Wound Care</option>
        </select>
      </div>

      <div id="language" class="input-group">
        <label for="languageSelect" class="sr-only">Language</label>
        <select name="language" id="languageSelect" class="form-control">
          <option value="" selected>Filter by Language</option>
        </select>
      </div>

      <div id="gender" class="input-group">
        <label for="genderSelect" class="sr-only">Gender</label>
        <select name="gender" id="genderSelect" class="form-control">
          <option value="F,M" selected>Filter by Gender</option>
          <option value="F">Female</option>
          <option value="M">Male</option>
        </select>
      </div>
      <p id="clear">
        <button type="button" id="clearButton" class="btn disabled" disabled="disabled"><span>Clear</span> <span class="visible-xs-inline visible-sm-inline">Filters</span></button>
      </p>
    </div>
    <p id="toggleFilters">
      <button type="button" id="toggleFiltersButton" class="btn">Show Filters</button>
    </p>
    <p id="activeFilterNotification"></p>
  </div><!-- /.form-group -->
</form>
<hr />

  