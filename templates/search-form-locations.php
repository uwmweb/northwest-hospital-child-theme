<?php $affiliated_medical_services = affiliated_medical_services(); ?>
<form id="location-search" role="search" method="get" class="search-form" action="<?php echo home_url( 'locations', 'relative' ); ?>">
  <div class="form-group"?>
    <div id="query" class="input-group" >
      <label>
        <span class="sr-only"><?php echo _x( 'Search :', 'label' ) ?></span>
        <input id="typeahead-locations" type="search" class="form-control search-field typeahead"
            placeholder="<?php echo esc_attr_x( 'Find a location by name', 'placeholder' ) ?>"
            value="<?php echo esc_attr(get_query_var("query")) ?>" name="query"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
      </label>
      <span class="input-group-btn"  >
        <button class="btn" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="sr-only">Search</span></button>
      </span>
    </div><!-- /.input-group -->
  </div><!-- /.form-group -->
</form>
<form id="location-filters" role="form" class="search-form">
   <div class="form-group">
    <div id="filters">
      <div id="zip" class="input-group">
        <label for="zipInput">Zip Code</label>
        <input name="uZip" id="zipInput" type="text" class="form-control">
      </div>

      <div id="distance" class="input-group">
        <label for="distanceSelect">Maximum Distance</label>
        <select name="uDist" id="distanceSelect" class="form-control">
          <option value="1">1 mile</option>
          <option value="5">5 miles</option>
          <option value="10" selected>10 miles</option>
          <option value="15">15 miles</option>
          <option value="50">50 miles</option>
          <option value="100">100 miles</option>
          <option value="0">Unlimited</option>
        </select>
      </div>
      <div id="filterButtons">
        <p>
          <button type="button" id="distFilterButton" class="btn"><span>Apply</span></button>
          <span id="or">OR</span>
          <button type="button" id="useLocButton" class="btn" >Use Current Location</i></button>
          <button type="button" id="clearButton" class="btn disabled" disabled="disabled"><span>Clear</span> <span class="visible-xs-inline visible-sm-inline">Filters</span></button>
        </p>
      </div>
    </div><!-- /#filters -->
    <p id="toggleFilters">
      <button type="button" id="toggleFiltersButton" class="btn">Show Filters</button>
    </p>
    <p id="activeFilterNotification"></p>
  </div><!-- /.form-group -->
</form>
<hr />