<?php if( have_rows("right_sidebar_widgets") ) : ?>
  <div class="uwm-widgets">
  <?php while( have_rows("right_sidebar_widgets") ) : the_row();
    $widget = get_sub_field("widget");
    if( $widget == "doctor" ) : ?>
      <a class="find-a-doctor widget btn" href="<?php echo trailingslashit(site_url())?>bios">Find a Doctor</a>
    <?php elseif ($widget == "appt" ) : ?>
      <a class="request-an-appointment widget btn" href="https://www.uwmedicine.org/request-an-appointment">Request an Appointment</a>
    <?php elseif ($widget == "eCare" ) : ?>
      <a class="eCare widget btn" href="http://www.uwmedicineecare.org">Log in to eCare</a>
    <?php elseif ($widget == "virtual" ) : ?>
      <a class="virtual-care widget btn" href="http://www.uwmedicine.org/locations/virtual-clinic">Get Virtual Care</a>
    <?php endif;
  endwhile; ?>
  </div>
<?php endif;
