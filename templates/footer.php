<footer class="content-info">
  <div class="container">
    <div id="footer-logo">
      <p><a href="<?= esc_url(home_url('/')); ?>">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/logo-white.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/dist/images/logo-white.png 1x, <?php echo get_stylesheet_directory_uri() ?>/dist/images/logo-white-2x.png 2x" alt="UW Medicine Northwest Hospital Logo" />
      </a></p>

    </div><!-- /#footer-logo -->

    <div id="footer-contact-information">
      <?php
      if (is_active_sidebar('footer-contact-information')) :
        dynamic_sidebar('footer-contact-information');
      endif;
      ?>
    </div><!-- /#footer-contact-information -->

    <div id="footer-social-media">
      <h3>Stay Connected</h3>
      <a href="https://www.facebook.com/NorthwestHospital" title="Northwest Hospital &amp; Medical Center on Facebook" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle-thin fa-stack-2x"></i>
          <i class="fa fa-facebook fa-stack-1x"></i>
        </span>
      </a>

      <a href="https://twitter.com/NWHospSeattle" title="@NWHospSeattle on Twitter" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle-thin fa-stack-2x"></i>
          <i class="fa fa-twitter fa-stack-1x"></i>
        </span>
      </a>

      <a href="https://www.linkedin.com/company/27485" title="Northwest Hospital &amp; Medical Center on LinkedIn" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle-thin fa-stack-2x"></i>
          <i class="fa fa-linkedin fa-stack-1x"></i>
        </span>
      </a>
    </div><!-- /#footer-social-media -->

    <div id="footer-search">
      <form role="search" method="get" class="search-form" action="<?php echo home_url( 'search', 'relative' ); ?>">
      <div class="input-group">
        <label>
          <span class="sr-only"><?php echo _x( 'Search :', 'label' ) ?></span>
          <input id="typeahead-footer" type="search" class="form-control search-field typeahead"
              placeholder="<?php echo esc_attr_x( 'Search for...', 'placeholder' ) ?>"
              value="<?php echo trim(esc_attr(get_query_var("query"))) ?>" name="query"
              title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
        </label>
        <span class="input-group-btn"  >
          <button class="btn" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="sr-only">Search</span></button>
        </span>
      </div>
      </form>
    </div><!-- /#footer-search -->
  </div><!-- /.container -->
</footer>

<div id="footer-menus">
  <div class="container-fluid">
    <div id="visitor-menu">
      <?php
      if (has_nav_menu('footer_visitor_menu')) :
        wp_nav_menu(array('theme_location' => 'footer_visitor_menu'));
      endif;
      ?>
    </div><!-- /#visitor-menu -->

    <div id="administrative-menu">
      <?php
      if (has_nav_menu('footer_administrative_menu')) :
        wp_nav_menu(array('theme_location' => 'footer_administrative_menu'));
      endif;
      ?>
    </div><!-- /#staff-menu -->
  </div><!-- /.container -->
</div><!-- /#footer-menus -->
<div id="footer-copyright">
  <p>&copy; <?php echo date("Y"); ?> University of Washington.<br />All rights reserved.</p>
</div>
