<?php $header_image = get_field('image'); ?>

	<div class="patient-story-entry">
		<?php if ($header_image) :
			$header_image_id = $header_image["id"];
			$size = "medium_large";
			$header_image_src = wp_get_attachment_image_src( $header_image_id, $size );
			$header_image_src = $header_image_src[0];
			$srcset = wp_get_attachment_image_srcset($header_image_id);
			$sizes = "(max-width: 767px) 100vw, (min-width: 768px) 502px, (min-width: 992px) 667px, (min-width: 1200px) 730px";?>
      <a href="<?php the_permalink(); ?>"><img src="<?php echo $header_image_src ?>" srcset="<?php echo $srcset ?>" sizes="<?php echo $sizes ?>" alt="<?php echo $header_image["alt"] ?>" /></a>
		<?php endif; ?>
		<h3><?php the_title(); ?></h3>
		<?php the_excerpt(); ?>
		<a href="<?php the_permalink(); ?>">Read the full story</a>
	</div>
