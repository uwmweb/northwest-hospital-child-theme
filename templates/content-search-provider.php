<?php use Roots\Sage\NWH_Extras; ?>
<h2>Providers</h2>
<!-- Populated by Handlebars Template --><div class="suggested-expertise-search"></div><!-- End Handlbars -->
<!-- Populated by Handlebars Template --><div class="suggested-providers-returned"></div><!-- End Handlbars -->
<?php if(empty($providers)) : ?>
  <div id="provider-no-results" class="alert alert-warning provider-entry">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/provider-image-not-available.jpg" alt="Silhouette of a person" />
    <p>Sorry, no results found. Are you looking for a provider? <strong>Search</strong> a <a href="/bios">full list of providers</a>.</p>
    <p class="search-btn"><a class="btn btn-blue" href="<?php echo home_url("/bios") ?>">Search All Providers</a></p>

  </div>
<?php elseif(!empty($providers)) : ?>
  <?php $providers_max = min(count($providers), 3); ?>
  <?php foreach(array_slice($providers, 0, $providers_max) as $provider ) : ?>
    <?php $provider_url = NWH_Extras\get_provider_url($provider["UniqueName"]) ?>
    <div class="provider-entry">
      <h3><a href="<?php echo $provider_url ?>"><?php echo $provider["FullName"] ?></a></h3>
      <a href="<?php echo $provider_url ?>">
        <img src="<?php echo NWH_Extras\get_provider_image($provider["ImageUrl"]) ?>" alt="<?php echo $provider["FullName"] ?>" />
      </a>
      <p><?php echo $provider['BioIntro'] ?></p>
    </div>
  <?php endforeach ?>

  <?php if( count($providers) > $providers_max ) : ?>
    <a class="btn btn-blue" href="<?php echo $provider_search_url ?>">More Providers</a>
  <?php endif ?>
  <?php if( count($providers) <= $providers_max ) : ?>
    <a class="btn btn-blue" href="<?php echo NWH_Extras\get_provider_url(); ?>">Search All Providers</a>
  <?php endif ?>
<?php endif ?>
