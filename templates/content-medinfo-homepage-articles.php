<?php $header_image = get_field('image');
$header_image_id = $header_image["id"];
$size = "full";
$header_image_src = wp_get_attachment_image_src( $header_image_id, $size );
$header_image_src = $header_image_src[0];
$srcset = wp_get_attachment_image_srcset($header_image_id);
$sizes = "(max-width: 767px) 100vw, (min-width: 768px) 40vw, (min-width: 992px) 40vw, (min-width: 1200px) 40vw";?>

<div class="story">
  <a href="<?php the_permalink() ?>">
    <img class="story-image" src="<?php echo $header_image_src ?>" srcset="<?php echo $srcset ?>" sizes="<?php echo $sizes ?>" alt="<?php echo $header_image["alt"] ?>" />
  </a>
  <div class="content">
    <h2><?php the_title(); ?></h2>
    <?php the_excerpt(); ?>
    <a href="<?php the_permalink() ?>">Read More</a>
  </div>
</div>
