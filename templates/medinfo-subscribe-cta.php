<div id="medinfo-subscribe-cta">
  <h3>Get MedInfo in your inbox</h3>
  <p>We'll send you a notice when we publish new stories.</p>
  <a href="<?php echo trailingslashit(home_url('medinfo/subscribe')) ?>" class="btn btn-green">Subscribe</a>
</div>
