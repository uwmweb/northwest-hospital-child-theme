<form role="search" method="get" class="search-form" action="<?php echo home_url( 'search', 'relative' ); ?>">
  <div id="query" class="input-group">
    <label>
      <span class="sr-only"><?php echo _x( 'Search :', 'label' ) ?></span>
      <input id="typeahead-default" type="search" class="form-control search-field typeahead"
          placeholder="<?php echo esc_attr_x( 'Search for...', 'placeholder' ) ?>"
          value="<?php echo trim(esc_attr(get_query_var("query"))) ?>" name="query"
          title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
    </label>
    <span class="input-group-btn"  >
      <button class="btn" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="sr-only">Search</span></button>
    </span>
  </div><!-- /.input-group -->
</form>
