<?php \Roots\Sage\Setup\define_current_template('content-single-patient-stories.php'); ?>

<?php if ( function_exists('yoast_breadcrumb') )
{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

<?php while (have_posts()) : the_post(); ?>
  <?php // Variables
	$header_image 	= get_field('image');
	$splash_text 	= get_field('splash_text');
	$story_leader	= get_field('story_leader');
  ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>

      <?php if ($header_image) :
        $header_image_id = $header_image["id"];
        $size = "full";
        $header_image_src = wp_get_attachment_image_src( $header_image_id, $size );
        $header_image_src = $header_image_src[0];
        $srcset = wp_get_attachment_image_srcset($header_image_id);
        $sizes = "(max-width: 767px) 100vw, (min-width: 768px) 502px, (min-width: 992px) 667px, (min-width: 1200px) 730px";?>

      <div id="header-image">
        <img src="<?php echo $header_image_src ?>" srcset="<?php echo $srcset ?>" sizes="<?php echo $sizes ?>" alt="<?php echo $header_image["alt"] ?>" />
      </div>
    <?php endif; ?>
    </header>

    <?php
    $right_sidebar = get_field("right_sidebar");
    $has_medinfo_edition = get_the_terms($post->ID, 'medinfo-edition');
    if ( $right_sidebar || $has_medinfo_edition ) : ?>
          <p class="splash-text"><?php echo $splash_text ?></p>
          <p class="story-leader"><?php echo $story_leader ?></p>
          <div id="right-inset">
              <?php if( have_rows('providers')) : ?>
                <div class="meet-the-docs hidden-xs">
                <?php while(have_rows('providers')) : the_row();?>
                  <div class="doc">
                    <?php $provider_slug = get_sub_field('provider_slug'); ?>
                    <img src="https://webservices.uwmedicine.org/api/bioimage/<?php echo $provider_slug ?>.jpg" alt="<?php the_sub_field('full_name'); ?>" />
                    <h4><?php the_sub_field('full_name'); ?></h4>
                    <p><?php the_sub_field('primary_location'); ?></p>

                    <a href="<?php echo Roots\Sage\NWH_Extras\get_provider_url($provider_slug); ?>">Read full bio</a>
                  </div>
              <?php endwhile; ?>
            </div><!--/.meet-the-docs -->
              <?php endif;?>

            <?php get_template_part('templates/medinfo-subscribe-cta'); ?>
            <?php get_template_part("templates/sidebar-right-widgets"); ?>
          </div>
          <?php the_content(); ?>
          <a class="btn btn-green" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>"><i class="fa fa-facebook" aria-hidden="true"><span class="sr-only">Facebook</span></i> Share</a>
          <a class="btn btn-green" href="https://twitter.com/home?status=<?php echo('Patient+Story:+'); echo(urlencode(the_title())); ?>%20via%20%40NWHospSeattle%20<?php echo the_permalink(); ?>"><i class="fa fa-twitter" aria-hidden="true"><span class="sr-only">Twitter</span></i>Tweet</a>
          <p class="visible-xs-block"><a id="share-patient-story" class="btn btn-gold" href="<?php echo trailingslashit(home_url()); ?>about-northwest-hospital/patient-stories/share-your-story/">Share Your Story</a></p>
        </div>
    <?php else : ?>
      <p class="splash-text"><?php echo $splash_text ?></p>
      <p class="story-leader"><?php echo $story_leader ?></p>
      <?php the_content(); ?>
      <a class="btn btn-green" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>"><i class="fa fa-facebook" aria-hidden="true"><span class="sr-only">Facebook</span></i> Share</a>
      <a class="btn btn-green" href="https://twitter.com/home?status=<?php echo(urlencode(the_title())); ?>%20via%20%40NWHospSeattle%20<?php echo the_permalink(); ?>"><i class="fa fa-twitter" aria-hidden="true"><span class="sr-only">Twitter</span></i>Tweet</a>
      <p class="visible-xs-block"><a id="share-patient-story" class="btn btn-gold" href="<?php echo trailingslashit(home_url()); ?>about-northwest-hospital/patient-stories/share-your-story/">Share Your Story</a></p>
    <?php endif; ?>
  </article>
<?php endwhile; ?>
