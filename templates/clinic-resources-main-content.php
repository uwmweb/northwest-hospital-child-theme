<div class="clinic-resources">
  <?php while(have_rows("main_content_clinic_resources")) : the_row(); ?>
    <?php
    $heading = get_sub_field('clinic_resource_heading');
    $category = get_sub_field('select_clinic_resource_categories');
    $selected_posts = get_sub_field('select_clinic_resource_posts');

    if( $category ) {
      $args = array(
        'posts_per_page' => 0,
        'post_status' => array('publish', 'inherit'),
        'post_type' => array('post', 'page', 'attachment', 'clinic-resources'),
        'orderby' => 'menu_order',
      	'tax_query' => array(
          array(
			      'taxonomy' => 'clinic-resource-categories',
  			    'field'    => 'term_id',
  			    'terms'    => array_values($category),
          ),
    		),
    	);
      $clinic_resource_tax_posts = get_posts( $args );
    } ?>
    <h2><?php echo $heading ?></h2>
    <?php if($clinic_resource_tax_posts || $selected_posts) : ?>
    <ul>
      <?php if($selected_posts) : ?>
        <?php foreach($selected_posts as $post) : setup_postdata($post); ?>
          <?php $post_type = get_post_type($post); ?>
          <?php if($post_type == 'attachment') : ?>
            <li><?php echo the_attachment_link(); ?></li>
          <?php else : ?>
            <li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
          <?php endif ?>
        <?php endforeach ?>
        <?php wp_reset_postdata(); ?>
      <?php endif ?>
      <?php if($clinic_resource_tax_posts) : ?>
        <?php foreach($clinic_resource_tax_posts as $post) : setup_postdata( $post); ?>
          <?php $post_type = get_post_type($post); ?>
          <?php if($post_type == 'attachment') : ?>
            <li><?php echo the_attachment_link(); ?></li>
          <?php else : ?>
            <li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
          <?php endif ?>
        <?php endforeach ?>
        <?php wp_reset_postdata(); ?>
      <?php endif ?>
    </ul>
  <?php endif ?>
  <?php unset($heading); unset($category); unset($selected_posts); unset($clinic_resource_tax_posts); ?>
  <?php endwhile ?>
</div>
