<?php \Roots\Sage\Setup\define_current_template('content-single-press-releases.php'); ?>
<p id="breadcrumbs">
  <span xmlns:v="http://rdf.data-vocabulary.org/#">
    <span typeof="v:Breadcrumb">
      <a href="http://nwhospital.wpengine.com" rel="v:url" property="v:title">Home</a> &raquo;
      <span rel="v:child" typeof="v:Breadcrumb">
        <a href="<?php echo trailingslashit(home_url('about-northwest-hospital')) ?>" rel="v:url" property="v:title">About Northwest Hospital</a> &raquo;
          <span rel="v:child" typeof="v:Breadcrumb">
            <a href="<?php echo trailingslashit(home_url('about-northwest-hospital/news')) ?>" rel="v:url" property="v:title">News</a> &raquo;
            <span class="breadcrumb_last">
              <?php echo the_title(); ?>
            </span>
          </span>
      </span>
    </span>
  </span>
</p>

<?php while (have_posts()) : the_post(); ?>
  <?php // Variables
	$date_picker 	= get_field('original_release_date');
	$media_contact 	= get_field('media_contact');
	$phone_number	= get_field('phone_number');
	$email			= get_field('email');
  ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <p>
	    Original Release: <?php echo $date_picker; ?><br/>
        Media Contact: <?php echo $media_contact; ?><br/>
        <?php echo $phone_number; ?><br/>
        Email: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
      </p>

    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  </article>
<?php endwhile; ?>
