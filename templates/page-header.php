<?php use Roots\Sage\Titles; ?>
<?php use Roots\Sage\NWH_Extras; ?>

<?php if( $post ) : ?>
  <?php if( NWH_Extras\add_provider_location_buttons($post->ID) ) : ?>
    <div class="page-header medical-services">
      <div class="medical-services title">
        <h1><?= Titles\title(); ?></h1>
      </div>
      <div class="medical-services providers visible-xs-block">
        <p><a class="btn" href="#">Providers</a></p>
      </div>
      <div class="medical-services locations visible-xs-block">
         <p><a class="btn" href="#">Locations</a></p>
      </div>
    </div>
  <?php else : ?>
    <div class="page-header">
      <h1><?= Titles\title(); ?></h1>
    </div>
  <?php endif ?>
<?php else : ?>
  <div class="page-header">
    <h1><?= Titles\title(); ?></h1>
  </div>
<?php endif ?>
