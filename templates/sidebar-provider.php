<?php use Roots\Sage\LocationLookup;
$locations = $provider["Locations"];

if( !$locations) {
  $locations = LocationLookup\get_location($provider["UniqueName"]);
} ?>

<?php if($locations) : ?>
  <div class="provider-sidebar">
  <h3>Provider Locations</h3>
  <?php foreach( $locations as $location ) : ?>
    <?php if( $location["IsPrimary"] == "1" ) : ?>
      <p id="primary-flag">Primary Location</p>
      <div class="primary location">
    <?php else : ?>
      <div class="location">
    <?php endif ?>
      <?php if($location["WpUrl"]) : ?>
      <?php $target_blank = ((strpos($location["WpUrl"], site_url()) !== 0) ? "target=_blank" : false); ?>
        <h4><a class="location-page" href="<?php echo $location["WpUrl"] ?>" <?php echo $target_blank ?>><?php echo $location["Name"]; ?></a></h4>
      <?php else : ?>
        <h4><?php echo $location["Name"]; ?></h4>
      <?php endif ?>
      <p><?php echo $location["AddressLine1"]; ?><br />
        <?php if($location["AddressLine2"]) { echo $location["AddressLine2"] . "<br />"; } ?>
        <?php if($location["RoomNumber"]) {echo $location["RoomNumber"] . "<br />"; } ?>
        <?php echo $location["City"] ?>, <?php echo $location["State"] ?> <?php echo $location["ZipCode"] ?><br />
        <?php if($location["PhoneNumber"]) :
          $phone_number = $location["PhoneNumber"]; ?>
          <a class="phone" href="tel:+1<?php echo $phone_number; ?>">
          <?php echo substr($phone_number, 0, 3);
          echo ".";
          echo substr($phone_number, 3, 3);
          echo ".";
          echo substr($phone_number, 6, 4);?></a></p>
        <?php endif ?>
      <?php $address_string = $location["AddressLine1"] . " ";
      if($location["AddressLine2"] ) { $address_string .= $location["AddressLine2"] . " "; }
      if($location["RoomNumber"] ) { $address_string .= $location["RoomNumber"] . " "; }
      $address_string .= $location["City"] . ", " . $location["State"] . " " . trim($location["ZipCode"], " "); ?>
      <p><a target="_blank" title="View this location on a Google Map" class="map" href="https://maps.google.com/maps?q=<?php echo urlencode($address_string); ?>&amp;hnear=<?php echo urlencode($address_string); ?>&amp;t=m&amp;z=14&amp;gl=us/">Map It &nbsp;<i class="fa fa-map-marker"></i></a></p>
      </div>
  <?php endforeach ?>
  </div>
<?php endif ?>
