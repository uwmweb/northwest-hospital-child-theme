<?php use Roots\Sage\NWH_Extras; ?>
  <li class="provider-container" data-gender="<?php echo($provider["Gender"]);?>">
    <div class="provider-entry">
      <div class="provider-name">
        <a href="<?php echo NWH_Extras\get_provider_url($provider["UniqueName"]) ?>">
          <h3 class="fullName" data-last-initial="<?php echo(substr($provider["LastName"],0, 1)) ?>"><?php echo $provider["FullName"] ?></h3>
        </a>
      </div>

      <div class="provider-photo">
        <a href="<?php echo NWH_Extras\get_provider_url($provider["UniqueName"]) ?>">
          <img class="lazyload" data-src="<?php echo NWH_Extras\get_provider_image($provider["ImageUrl"]) ?>" alt="<?php echo $provider["FullName"] ?>" />
        </a>
          <p class="accepting-patients"><?php if( $provider["AcceptingAppointments"] == 1 ) : ?><i class="fa fa-check-square-o "></i> <em>Accepting new patients</em><?php else: ?>&nbsp;<?php endif ?></p>
        <?php if( $provider["ShowAppointmentRequest"] == 1 ) : ?>
          <p class="appointment-request"><a href="https://www.uwmedicine.org/request-an-appointment?provider=<?php echo (urlencode($provider["FullName"]) . "&" . "providerid=" . $provider["Id"]) ?>" class="btn btn-blue">Schedule Appointment</a></p>
        <?php endif ?>
      </div>

      <div class="provider-bio">
        <?php if( !empty($provider["BioIntro"])) : ?>
          <p class="bio"><?php echo strip_tags($provider['BioIntro']) ?></p>
        <?php endif ?>
        <div class="row">
          <?php if(!empty($provider["Languages"])) : ?>
          <div class="languages" data-languages="<?php foreach($provider["Languages"] as $language ) {echo($language["Name"] . ','); } ?>">
            <?php if(count($provider["Languages"]) > 1 ) : ?>
              <h3>Languages</h3>
            <?php else : ?>
              <h3>Language</h3>
            <?php endif ?>
            <p>
              <?php $more = false; if(count($provider["Languages"]) > 3) { $more = true; }
              $provider["Languages"] = array_slice($provider["Languages"], 0, 3); ?>
              <?php foreach($provider["Languages"] as $key => $expertise ) : ?>
                <?php echo trim($expertise["Name"]); if( $key < (count($provider["Languages"]) - 1) ){ echo ", "; } ?>
              <?php endforeach ?>
              <?php if($more) { echo "&hellip;"; } ?>
            </p>
          </div>  
          <?php endif ?>

          <?php if(!empty($provider["Expertises"])) : ?>
          <div class="expertises expertiseIds expertiseNames" data-expertise-ids="<?php foreach($provider["Expertises"] as $expertise ) {echo($expertise["Id"] . ','); } ?>" data-expertise-names="<?php foreach($provider["Expertises"] as $expertise ) {echo($expertise["Name"] . ','); } ?>"> 
            <?php if(count($provider["Expertises"]) > 1 ) : ?>
              <h3>Specialties &amp; Expertise</h3>
            <?php else : ?>
              <h3>Specialty / Expertise</h3>
            <?php endif ?>
            <p>
              <?php $more = false; if(count($provider["Expertises"]) > 3) { $more = true; }
              $provider["Expertises"] = array_slice($provider["Expertises"], 0, 3); ?>
              <?php foreach($provider["Expertises"] as $key => $expertise ) : ?>
                <?php echo trim($expertise["Name"]); if( $key < (count($provider["Expertises"]) - 1) ){ echo ", "; } ?>
              <?php endforeach ?>
              <?php if($more) { echo "&hellip;"; } ?>
            </p>
          </div>
          <?php endif ?>
        </div>
      </div>
    </div>
  </li>
