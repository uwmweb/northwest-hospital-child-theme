<?php
use Location\Coordinate;
//use Location\Formatter\Coordinate\DecimalDegrees;
use Location\Distance\Vincenty;

$lat = get_query_var('uLat', 0);
$lng = get_query_var('uLng', 0);
$dist = get_query_var('uDist', 0);

$coord = null;
if($lat && $lng) {
  $coord = new Coordinate($lat,$lng);
}

if($coord) {

  function meters_to_miles($meters) {
    $METERS_PER_MILE = 1609.34; 
    return round(floatval($meters) / $METERS_PER_MILE, 2);
  }

  function compare_distance($a, $b) {
    if($a["Distance"] == $b["Distance"]) {
      return 0;
    }

    return($a["Distance"] < $b["Distance"]) ? -1 : 1;
  }

  foreach($locations as $key => $location) {
    $lat = $location["Latitude"];
    $lng = $location["Longitude"];
    $clinic_location = new Coordinate($lat, $lng);
    $distance = meters_to_miles($coord->getDistance($clinic_location, new Vincenty()));
    $locations[$key]["Distance"] = $distance;

    if($dist > 0) {
      if(round($distance) >= $dist) {
        unset($locations[$key]);
      }
    }
  }

  uasort($locations, 'compare_distance');

} ?>

<?php if(count($locations) == 0 ) : ?>
  <p class="alert alert-warning">Sorry, no results found. Please try your search again with different search terms or filters, or <a href="/locations">browse a full list of locations</a>.</p>
<?php endif ?>

<?php foreach($locations as $location ) : ?>

  <?php $search_result_image = get_field("location_search_result_image", $location["PageId"]); ?>
  <?php $target_blank = ((strpos($location["WpUrl"], site_url()) !== 0) ? "target=_blank" : false); ?>
  <div class="location-entry" data-lat="<?php echo $location['Latitude'] ?>" data-lng="<?php echo $location['Longitude'] ?>">
    <div class="location-image">
      <a href="<?php echo $location["WpUrl"] ?>" <?php echo $target_blank ?>><img src="<?php echo $search_result_image["url"] ?>" /></a>
    </div>
    <div class="location-info">
      <p class="name"><a href="<?php echo $location["WpUrl"] ?>" <?php echo $target_blank ?>><?php echo $location["Name"]; ?></a></p>
      <p class="address"><?php if(($location["LocationName"]) && ($location["LocationName"] !== $location["Name"])) {echo $location["LocationName"] . "<br />"; } ?>
        <?php echo $location["AddressLine1"]; ?><br />
        <?php if($location["AddressLine2"]) { echo $location["AddressLine2"] . "<br />"; } ?>
        <?php if($location["RoomNumber"]) { echo $location["RoomNumber"] . "<br />"; } ?>
        <?php echo $location["City"] ?>, <?php echo $location["State"] ?> <?php echo $location["ZipCode"] ?>
      </p>
      <p>
        <span class="phone"><?php $phone_number = $location["PhoneNumber"]?>
          <?php if($phone_number) {
            echo "Phone: ";
            echo substr($phone_number, 0, 3);
            echo ".";
            echo substr($phone_number, 3, 3);
            echo ".";
            echo substr($phone_number, 6, 4);
          } ?></span>
        <span class="fax">
          <?php $fax_number = $location["FaxNumber"];
          if( $fax_number ) {
            $fax_number_formatted = substr($fax_number, 0, 3) ."." . substr($fax_number, 3, 3) . "." . substr($fax_number, 6, 4);
            echo ("Fax: " . $fax_number_formatted);
          }
          ?>
        </span>
      </p>
      <?php if( $location["Distance"] ) : ?>
        <p><strong>Approximate Distance</strong>: <?php echo $location["Distance"]; ?> miles</p>
      <?php endif; ?>
    </div>
  </div>
<?php endforeach ?>
