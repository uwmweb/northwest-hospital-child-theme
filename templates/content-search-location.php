<h2>Clinic Locations</h2>
<?php if(empty($locations)) : ?>
  <div class="map-container">
    <div id="map">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/map-dim.jpg" alt="Map of North Seattle with black overlay and no clinic location markers" />
    </div>
  </div>
  <div class="alert alert-warning">
    <p>Sorry, no results found. Are you looking for a location? <strong>Search</strong> a <a href="/locations">full list of locations</a>.</p>
    <p><a class="btn btn-blue" href="<?php echo home_url("/locations") ?>">Search All Locations</a></p>

  </div>
<?php elseif(!empty($locations)) : ?>
  <?php $locations_max = min(count($locations), 3);
  $labels = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J"); ?>

  <div id="map-container">
    <div id="map"></div>
  </div>

  <div id="locations-listing">
    <?php $i = 0; ?>
    <?php foreach(array_slice($locations, 0, $locations_max) as $location ) : ?>
      <div class="row">
        <div class="col-xs-12"><p><a href="<?php echo $location["WpUrl"] ?>"><?php echo $labels[$i]; ?>: <?php echo $location["Name"]; ?></a></p></div>
        <div class="col-xs-4 col-sm-6"><span class="directions"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<a href="<?php echo $location["WpUrl"] ?>directions">Directions</a></span></div>
        <div class="col-xs-4 col-sm-6"><span class="description"><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;<a href="<?php echo $location["WpUrl"] ?>">Clinic Information</a></span></div>
      </div>
      <?php $i++; ?>
    <?php endforeach ?>
    <?php if( count($locations) > $locations_max ) : ?>

      <a class="btn btn-blue" href="<?php echo $location_search_url; ?>">More Locations</a>
    <?php endif ?>
  </div>

  <script>
  function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
      disableDefaultUI: true
    });

    var locations = <?php echo json_encode($locations); ?>;
    var markers_max = Math.min(locations.length, 3);
    var bounds = new google.maps.LatLngBounds();

    var labels = <?php echo json_encode($labels); ?>;

    for (i = 0; i < markers_max; i++) {
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i]['Latitude'], locations[i]['Longitude']),
        label: labels[i],
        map: map});

      //extend the bounds to include each marker's position
      bounds.extend(marker.position);
    }

    google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
      map.setZoom(map.getZoom()-1);

      if (this.getZoom() > 15) {
        this.setZoom(14);
      }
    });
    map.fitBounds(bounds);
  };
  </script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAClfj1QHSxjcwOYbXdrwT4w9ocopzoJo0&callback=initMap">
  </script>
<?php endif ?>
