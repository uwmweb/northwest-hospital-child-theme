<?php

if( isset($key_contact_parent) ) {
  $posts = get_field('selected_key_contacts', $key_contact_parent->ID);
}
else {
  $posts = get_field('selected_key_contacts');
}

if( $posts ): ?>
<div id="key-contacts">
  <div id="sidebar-key-contacts-heading">
    <h2>Key Contacts</h2>
  </div>
  <div id="sidebar-key-contacts-content">
  <?php foreach( $posts as $post): // variable must be called $post ?>
    <?php setup_postdata($post); ?>
    <?php
    // main repeater for key contacts in the selected post object
    while( have_rows('key_contact') ): the_row(); ?>

    <h3><?php the_sub_field('contact_title', false); ?></h3>

    <?php the_sub_field('contact_address'); ?>

    <?php // check for phone number rows (sub repeater)
      if( have_rows('contact_phone') ): ?>
        <p><ul>
        <?php // loop through contact_phone rows (sub repeater)
        while( have_rows('contact_phone') ): the_row(); ?>
          <li><?php the_sub_field('phone_number', false); ?></li>
        <?php endwhile; //while( have_rows('contact_phone') ): ?>
        </ul></p>
      <?php endif; // if( have_rows('contact_phone') ): ?>

    <?php if( get_sub_field('contact_hours_of_operation')) : ?>
      <p>Hours of Operation: <br /><?php the_sub_field('contact_hours_of_operation'); ?></p>
    <?php endif ?>

    <?php endwhile; // while( have_rows('key_contact') ):?>

    <?php // display selected global contacts
    $field = get_field_object('include_global_contacts');
    $values = $field['value'];
    $choices = $field['choices']; ?>

    <?php wp_reset_postdata(); ?>

    <?php $global_contacts = array();
    if( $values ) {
      foreach( $values as $v ) {
        $global_contacts[] = get_post($v);
      }
    } ?>

    <?php if( $global_contacts ): ?>
      <?php foreach( $global_contacts as $post ): ?>
        <?php setup_postdata($post); ?>
        <?php while( have_rows('key_contact') ): the_row(); ?>

        <h3><?php the_sub_field('contact_title', false); ?></h3>

        <?php // check for phone number rows (sub repeater)
          if( have_rows('contact_phone') ): ?>
            <ul>
            <?php // loop through contact_phone rows (sub repeater)
            while( have_rows('contact_phone') ): the_row(); ?>
              <li><?php the_sub_field('phone_number', false); ?></li>
            <?php endwhile; //while( have_rows('contact_phone') ): ?>
            </ul>
          <?php endif; // if( have_rows('contact_phone') ): ?>

        <?php endwhile; // while( have_rows('key_contact') ):?>
      <?php endforeach; // foreach( $posts as $post): ?>
    <?php endif; // if( $posts ): ?>

  <?php endforeach; //foreach( $posts as $post ): ?>
  </div>
<?php wp_reset_postdata(); ?>
</div>
<?php endif; ?>
