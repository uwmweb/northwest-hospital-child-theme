<?php $service = ramp_service_linked(); ?>

<?php
  $header_image = get_field("image");
  if ($header_image) :
    $header_image_id = $header_image["id"];
    $size = "medium_large";
    $header_image_src = wp_get_attachment_image_src( $header_image_id, $size );
    $header_image_src = $header_image_src[0];
    $srcset = wp_get_attachment_image_srcset($header_image_id);
    $sizes = "(max-width: 767px) 100vw, (min-width: 768px) 502px, (min-width: 992px) 667px, (min-width: 1200px) 730px"; ?>
  <div id="header-image">
    <img src="<?php echo $header_image_src ?>" srcset="<?php echo $srcset ?>" sizes="<?php echo $sizes ?>" alt="<?php echo $header_image["alt"] ?>" />
  </div>
<?php endif; ?>

<?php
$right_sidebar = get_field("right_sidebar");
$has_widgets = have_rows("right_sidebar_widgets");
$clinic_resources_main_content = have_rows("main_content_clinic_resources");
$clinic_resources_right_sidebar = have_rows("right_sidebar_clinic_resources");

if ( $right_sidebar || $has_widgets || $clinic_resources_right_sidebar ) : ?>
  <div class="row">
    <div id="content" class="col-md-8">
      <?php the_content(); ?>
      <?php if( $clinic_resources_main_content ) {
        get_template_part("templates/clinic-resources-main-content");
      } ?>
    </div>
    <div id="right-sidebar" class="col-md-4">
      <?php get_template_part("templates/sidebar-right-widgets"); ?>
      <?php echo $right_sidebar ?>
      <?php get_template_part("templates/clinic-resources-right-sidebar"); ?>
    </div>
  </div>
<?php else : ?>
  <?php the_content(); ?>
  <?php if( $clinic_resources_main_content ) {
    get_template_part("templates/clinic-resources-main-content");
  } ?>
<?php endif; ?>

<?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>')); ?>
