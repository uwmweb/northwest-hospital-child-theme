<div class="meet-the-docs">
  <?php if( have_rows('providers')) : ?>
    <h3>Meet the Doctors</h3>
    <?php while(have_rows('providers')) : the_row();?>
      <div class="doc">
        <?php $provider_slug = get_sub_field('provider_slug');
        $custom_photo = get_sub_field('custom_photo');
        $custom_link = get_sub_field('custom_link'); ?>
        <img src="<?php if( ! $custom_photo ) { echo "https://webservices.uwmedicine.org/api/bioimage/" . $provider_slug . ".jpg"; } else { echo $custom_photo["url"]; } ?>" alt="<?php the_sub_field('full_name'); ?>" />
        <h4><?php the_sub_field('full_name'); ?></h4>
        <p><?php the_sub_field('primary_location'); ?></p>
        <a href="<?php if( ! $custom_link ) { echo Roots\Sage\NWH_Extras\get_provider_url($provider_slug); } else { echo $custom_link; } ?>" alt="<?php the_sub_field('full_name'); ?>">Read full bio</a>
      </div>
  <?php endwhile;
  endif;?>
</div><!--/.meet-the-docs -->
