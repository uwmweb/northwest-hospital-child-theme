<?php if( have_rows('homepage_alert_notification') ) : ?>
  <?php while(have_rows('homepage_alert_notification')) : the_row(); ?>
    <?php $display_alert = get_sub_field('display_alert');
    $alert_page = get_sub_field('select_alert_page');
    $alert_color = get_sub_field('alert_color');
    $alert_text = get_sub_field('alert_homepage_text'); ?>

    <script type="text/javascript">
    var displayAlert = <?php if($display_alert == true ) { echo 'true'; } else { echo 'false'; }; ?>;
    jQuery(function(){
      if( displayAlert ) {
        megaMenu = jQuery('#mega-menu-primary_navigation');
        megaMenu.append('<li class="mega-menu-item hompeage-alert hidden-xs">' +
        '<a id="toggle-alert" class="mega-menu-link" style="color: <?php echo $alert_color; ?>">Alert</a></li>')

        jQuery( "#toggle-alert" ).on("click.fadeToggle", function() {
          jQuery( "#homepage-alert-container" ).fadeToggle("fast");
        });

        jQuery( "#alert-close-button" ).on("click.fadeToggle", function() {
          jQuery( "#homepage-alert-container" ).fadeToggle("fast");
        });
      }
    });
    </script>

  <?php if($display_alert) : ?>
    <div id="homepage-alert-container" class="container">
      <div class="row">
        <div id="homepage-alert" style="background-color: <?php echo $alert_color ?>">
          <div id="alert-close-button" class="hidden-xs">
            <a id="close-button"><i class="fa fa-times-circle-o fa-2x"></i></a>
          </div>
          <div class="col-sm-10 col-sm-offset-1">
            <p class="alert-text"><?php echo $alert_text ?></p>
            <p class="alert-call-to-action">For specific details click Learn More.</p>
          </div>
          <div id="cta-button">
            <p><a class="alert-call-to-action-button btn btn-white" style="color: <?php echo $alert_color ?>;" href="<?php echo $alert_page; ?>">Learn More</a></p>
          </div>
        </div>
      </div>
    </div>
  <?php endif ?>
  <?php endwhile ?>
<?php endif ?>


<div id="homepage-slider-images-container" class="container-fluid">
  <?php if (have_rows('homepage_slider_images')) : ?>
  <div id="homepage-slider-images" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
    <?php $slide_count = 0; ?>
    <?php while (have_rows('homepage_slider_images')) : the_row();
      $title = get_sub_field('title');
      $hook = get_sub_field('hook');
      $call_to_action = get_sub_field('call_to_action');
      $url = get_sub_field('url');
      $image = get_sub_field('image');
      $mobile_image = get_sub_field('mobile_image');
      ?>
    <?php if ($slide_count === 0) : ?>
    <div class="item active">
    <?php else: ?>
    <div class="item">
    <?php endif ?>
      <div class="desktop-image hidden-xs" style="background-image: url(<?php echo $image['url']?>);"></div>
      <div class="mobile-image visible-xs-block" style="background-image: url(<?php echo $mobile_image['url']?>);"></div>
      <div class="carousel-caption">
        <h3><a href="<?php echo $url;?>"><?php echo $title;?></a></h3>
        <p><?php echo $hook;?></p>
        <a class="btn" href="<?php echo $url;?>"><?php echo $call_to_action;?></a>
      </div><!-- /.carousel-caption -->
    </div><!-- /.item [active] -->
    <?php $slide_count++;
    endwhile; ?>
    </div><!-- /.carousel-inner -->

    <!-- Indicators -->
    <ol class="carousel-indicators">
      <?php $indicator_count = 0;
      while (have_rows('homepage_slider_images')) : the_row();
        if ($indicator_count === 0) : ?>
      <li data-target="#homepage-slider-images" data-slide-to="0" class="active"></li>
      <?php else: ?>
      <li data-target="#homepage-slider-images" data-slide-to="<?php echo $indicator_count; ?>"></li>
      <?php endif;
      $indicator_count++;
      endwhile; ?>
    </ol><!-- /.carousel-indicators -->
  </div><!-- /#homepage-slider-images -->
  <?php endif; ?>
</div>
<div id="homepage-search-box-container" class="container">
  <div class="row">
    <div id="large-search-box">
      <?php get_template_part('templates/search-form', 'large') ?>
    </div>
  </div>
</div>

<div id="homepage-featured-pages-container" class="container-fluid">
  <div class="row">
    <div id="homepage-featured-pages">
      <div id="maps" class="circle">
        <a href="<?php echo trailingslashit(site_url())?>patient-information/maps-directions-numbers/">
          <span class="icon">
            <i class="fa fa-map-marker fa-2x"><span class="sr-only">Map Marker Icon</span></i>
          </span>
          <p>Maps &amp; Numbers</p>
        </a>
      </div>
      <div id="careers" class="circle">
        <a href="<?php echo trailingslashit(site_url())?>about-northwest-hospital/careers/">
          <span class="icon">
            <i class="fa fa-users fa-2x"></i>
          </span>
          <p>Career Opportunities</p>
        </a>
      </div>
      <div id="billpay" class="circle">
        <a href="<?php echo trailingslashit(site_url())?>patient-information/patient-financial-services/">
          <span class="icon">
            <i class="fa fa-credit-card fa-2x"></i>
          </span>
          <p>Online Bill Pay</p>
        </a>
      </div>
      <div id="give" class="circle">
        <a href="<?php echo trailingslashit(site_url())?>about-northwest-hospital/giving-to-northwest-hospital/">
          <span class="icon">
            <i class="fa fa-heart fa-2x"></i>
          </span>
          <p>Ways to Give</p>
        </a>
      </div>
      <div id="uwmed" class="circle">
        <a href="http://uwmedicine.org" target="_blank">
          <span class="icon">
            <svg enable-background="new 0 0 55.456 55.393" height="2em" id="Layer_1" version="1.1" viewBox="0 0 55.456 55.393" width="2.218em" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px"><path d="M24.521,16.487l-0.179-1.861c-2.177-0.068-3.986-0.393-5.488-0.873c-0.689,0.585-2.156,1.735-4.209,2.887C17.163,17.015,20.354,17.015,24.521,16.487z M12.157,8.41c-2.44,1.281-5.333,1.69-7.548,1.788c0.612,0.816,1.231,1.616,1.926,2.361 c3.013-0.662,5.421-1.778,6.705-2.46C12.74,9.48,12.392,8.892,12.157,8.41z M13.003,6.085c0.298,0.117,0.519,0.375,0.59,0.687c0.056,0.229,1.468,5.516,10.561,5.901l-0.005-0.045c0,0-3.191-1.409-2.82-5.714c0.371-4.304-2.82-6.976-6.456-4.675 C11.238,4.54,7.305,5.356,4.93,4.984C2.555,4.614,0.66,5.322,2.406,7.359c0.259,0.302,0.508,0.608,0.749,0.916c2.284,0.022,6.225-0.244,8.948-2.09C12.369,6.005,12.706,5.968,13.003,6.085z M14.665,11.536c-1.067,0.607-3.363,1.78-6.397,2.601c0.952,0.72,2.068,1.339,3.446,1.809c2.245-1.024,4.004-2.218,5.077-3.032C15.956,12.491,15.253,12.021,14.665,11.536z M38.666,12.914c1.073,0.814,2.833,2.008,5.077,3.032c1.378-0.47,2.494-1.089,3.445-1.809c-3.033-0.821-5.329-1.994-6.396-2.601 C40.202,12.021,39.5,12.491,38.666,12.914z M42.216,10.099c1.284,0.682,3.692,1.798,6.705,2.459c0.695-0.745,1.314-1.544,1.926-2.361C48.632,10.1,45.738,9.69,43.299,8.41C43.063,8.892,42.715,9.48,42.216,10.099z M50.526,4.984 c-2.375,0.372-6.308-0.445-9.944-2.746c-3.637-2.3-6.827,0.371-6.457,4.675c0.371,4.305-2.82,5.714-2.82,5.714l-0.004,0.045 c9.093-0.385,10.504-5.671,10.561-5.901c0.071-0.312,0.292-0.569,0.591-0.687c0.297-0.117,0.635-0.081,0.9,0.099 c2.724,1.847,6.664,2.113,8.948,2.09c0.242-0.307,0.49-0.613,0.749-0.916C54.795,5.322,52.9,4.614,50.526,4.984z M31.113,14.625 l-0.179,1.861c4.167,0.528,7.358,0.528,9.876,0.153c-2.053-1.151-3.519-2.301-4.209-2.887C35.1,14.233,33.291,14.558,31.113,14.625z M34.405,20.238c-1.496-0.552-3.033-0.117-3.433,0.97c-0.401,1.087,0.486,2.415,1.981,2.967c0.55,0.203,1.105,0.271,1.608,0.224 c-0.171,1.501-1.172,3.674-5.145,5.076c-0.294,0.104-0.585,0.211-0.873,0.32l0.478-20.672c1.614-0.571,2.771-2.105,2.771-3.915 c0-2.295-1.86-4.156-4.156-4.156c-2.295,0-4.156,1.86-4.156,4.156c0,1.781,1.123,3.296,2.697,3.887l0.481,20.788 c-0.361-0.14-0.727-0.277-1.099-0.408c-3.972-1.401-4.973-3.574-5.145-5.076c0.503,0.047,1.058-0.021,1.608-0.224 c1.495-0.552,2.382-1.879,1.981-2.967c-0.401-1.087-1.938-1.521-3.433-0.97c-1.014,0.375-1.748,1.106-1.99,1.878 c-1.825,2.864,0.278,7.31,8.133,10.178l0.169,7.312c-2.726-1.34-3.699-2.595-3.699-3.676c0-1.188,2.078-2.375,2.078-2.375 l-2.078-0.965c0,0-1.336,0.679-1.336,3.117c0,2.309,2.331,3.99,5.065,5.214l0.16,6.938c-0.66-0.439-2.332-1.708-2.332-3.32 c0-0.77,0.502-1.538,1.211-2.18l-1.148-0.566c-0.84,0.909-0.953,1.775-0.953,2.449c0,1.079,0.426,3.062,3.225,3.723l0.098,4.238 c0.006,0.238,0.196,0.425,0.425,0.425s0.419-0.187,0.426-0.425l0.097-4.22c2.867-0.643,3.299-2.651,3.299-3.74 c0-0.938-0.219-2.247-2.315-3.527c6.624-3.548,5.211-6.757,1.041-9.189c6.291-2.846,7.909-6.782,6.249-9.401 C36.158,21.35,35.423,20.614,34.405,20.238z M30.532,44.539c0,1.679-1.812,2.986-2.407,3.37l0.146-6.326 C29.479,42.312,30.532,43.424,30.532,44.539z M28.314,39.721l0.158-6.803C35.22,35.547,30.322,38.671,28.314,39.721z"></path></svg>
          </span>
          <p>UW Medicine</p>
        </a>
      </div>
    </div>
  </div>
</div>
<div id="homepage-call-to-action-boxes-container" class="container">
  <div id="homepage-call-to-action-boxes">
    <div class="row">
      <div class="col-md-8">
        <div class="row">
          <?php if (have_rows('first_call_to_action_box')) : ?>
          <?php while (have_rows('first_call_to_action_box')) : the_row();
            $title = get_sub_field('title');
            $hook = get_sub_field('hook');
            $call_to_action = get_sub_field('call_to_action');
            $url = get_sub_field('url');
            $first_image = (get_sub_field('image') ? get_sub_field('image') : null);
            ?>
          <div id="first" class="cta-box">
            <a href="<?php echo $url;?>"><img src="<?php echo $first_image['url'];?>" alt="<?php echo $first_image['alt'];?>" /></a>
            <div class="cta-content">
              <h3><?php echo $title;?></h3>
              <p><?php echo $hook;?></p>
              <p><a class="btn" href="<?php echo $url;?>"><?php echo $call_to_action;?></a>
            </div><!-- /.cta-content -->
          </div><!-- /#first .cta-box -->
          <?php endwhile; ?>
          <?php endif; ?>

          <?php $second_rows = get_field('second_call_to_action_box'); ?>
          <?php if( $second_rows ) : ?>
          <div id="second" class="cta-box carousel slide" data-ride="carousel" data-interval="false">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
            <?php $second_slide_count = 0; ?>
            <?php while (have_rows('second_call_to_action_box')) : the_row();
              $title = get_sub_field('title');
              $hook = get_sub_field('hook');
              $call_to_action = get_sub_field('call_to_action');
              $url = get_sub_field('url');
              $second_image = get_sub_field('image');
              ?>
              <?php if ($second_slide_count === 0) : ?>
              <div class="item active">
              <?php else: ?>
              <div class="item">
              <?php endif ?>
                <a href="<?php echo $url;?>"><img src="<?php echo $second_image['url'];?>" alt="<?php echo $second_image['alt'];?>" /></a>
                <div class="cta-content">
                  <h3><?php echo $title;?></h3>
                  <p><?php echo $hook;?></p>
                  <p><a class="btn" href="<?php echo $url;?>"><?php echo $call_to_action;?></a>
                </div><!-- /.cta-content -->
              </div><!-- /.item [active] -->
            <?php $second_slide_count++;
            endwhile; ?><!-- /while have_rows('second_call_to_action_box') -->
            </div><!-- /.carousel-inner -->

            <!-- Controls -->
            <?php if( count($second_rows) > 1 ) : ?>
            <div class="controls">
              <a class="left carousel-control" href="#second" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>

              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php $second_indicator_count = 0;
                while (have_rows('second_call_to_action_box')) : the_row();
                  if ($second_indicator_count === 0) : ?>
                <li data-target="#second" data-slide-to="0" class="active"></li>
                <?php else: ?>
                <li data-target="#second" data-slide-to="<?php echo $second_indicator_count; ?>"></li>
                <?php endif; ?> <!-- /if ($second_indicator_count === 0) -->
                <?php $second_indicator_count++;
                endwhile; ?> <!-- /while have_rows('second_call_to_action_box') -->
              </ol><!-- /.carousel-indicators -->

              <a class="right carousel-control" href="#second" role="button" data-slide="next">
                <span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div><!-- /.controls -->
          <?php endif ?><!-- /if count($second_rows) > 1 -->
          </div><!-- /#second .cta-box .carousel .slide -->
        <?php endif ?><!-- /if $second_rows -->
        </div><!-- /.row -->

        <div class="row">
          <?php if (have_rows('third_call_to_action_box')) : ?>
          <?php while (have_rows('third_call_to_action_box')) : the_row();
            $title = get_sub_field('title');
            $hook = get_sub_field('hook');
            $call_to_action = get_sub_field('call_to_action');
            $url = get_sub_field('url');
            $third_image = (get_sub_field('image') ? get_sub_field('image') : null);
            ?>
          <div id="third" class="cta-box">
            <a href="<?php echo $url;?>"><img src="<?php echo $third_image['url'];?>" alt="<?php echo $third_image['alt'];?>" /></a>
            <div class="cta-content">
              <h3><?php echo $title;?></h3>
              <p><?php echo $hook;?></p>
              <p><a class="btn" href="<?php echo $url;?>"><?php echo $call_to_action;?></a>
            </div><!-- /.cta-content -->
          </div><!-- /#third .cta-box -->
          <?php endwhile; ?>
          <?php endif; ?>

          <?php if (have_rows('fourth_call_to_action_box')) : ?>
          <?php while (have_rows('fourth_call_to_action_box')) : the_row();
            $title = get_sub_field('title');
            $hook = get_sub_field('hook');
            $call_to_action = get_sub_field('call_to_action');
            $url = get_sub_field('url');
            $fourth_image = (get_sub_field('image') ? get_sub_field('image') : null);
            ?>
          <div id="fourth" class="cta-box">
            <a href="<?php echo $url;?>"><img src="<?php echo $fourth_image['url'];?>" alt="<?php echo $fourth_image['alt'];?>" /></a>
            <div class="cta-content">
              <h3><?php echo $title;?></h3>
              <p><?php echo $hook;?></p>
              <p><a class="btn" href="<?php echo $url;?>"><?php echo $call_to_action;?></a>
            </div><!-- /.cta-content -->
          </div><!-- /#fourth .cta-box -->
          <?php endwhile; ?>
          <?php endif; ?>

        </div><!--- /.row -->
      </div><!-- /.col-md-8 -->
      <div id="fifth" class="col-md-4">
        <?php if( have_rows('medinfo_box')) : ?>
        <div id="medinfo">
          <h3>MedInfo Magazine</h3>
          <?php while (have_rows('medinfo_box')) : the_row(); ?>
            <?php $medinfo_image = get_sub_field('cover_image') ?>
            <a href="<?php trailingslashit(site_url())?>/medinfo/">
              <img src="<?php echo $medinfo_image['url']; ?>" />
            </a>
              <p><?php echo get_sub_field('hook'); ?> <a href="<?php trailingslashit(site_url())?>/medinfo/">See the latest&nbsp;issue&nbsp;&raquo;</a></p>
          <?php endwhile; ?>
        </div><!-- /#medinfo -->
        <div id="separator"></div>
        <?php endif; ?>

        <div id="homepage-rss">
          <h3>Latest News from HSNewsBeat</h3>
          <?php the_field("rss_feed") ?>
          <div class="row">
          <?php while(have_rows('additional_news_buttons')) : the_row();
            $text = get_sub_field('button_text');
            $url = get_sub_field('url');
            $new_tab = get_sub_field('open_in_new_tab'); ?>

            <p class="homepage-rss-button"><a class="btn" href="<?php echo $url ?>" <?php if($new_tab) { echo('target="_blank"');} ?>><?php echo $text ?></a></p>
          <?php endwhile ?>
          </div><!-- /.row -->
        </div><!-- /#homepage-rss -->
      </div><!-- /#fifth -->
    </div><!-- /.row -->
  </div><!-- /#homepage-call-to-action-boxes -->
</div><!-- /#homepage-call-to-action-boxes-container -->
<div id="homepage-events-calendar-container" class="container">
  <div class="row">
    <div id="homepage-events-calendar" class="hidden-xs">
      <h2>Classes, Seminars and Events</h2>
  	  <div id="event-list">
  		<div style="width:100%;">
  			<script type="text/javascript">
  				$Trumba.addSpud({
  				webName: "sea_nwh-events",
  				spudType: "upcoming",
  				openInNewWindow: false,
  				teaserBase: "<?php echo trailingslashit(site_url())?>patient-information/classes/northwest-hospital-events-calendar/",
  	            url: {headinglevel: 2}  });
  			</script>
  		</div>
  	  </div>
  	  <div id="event-calendar">
  	  	<div style="width:100%;">
  			<script type="text/javascript">
  				$Trumba.addSpud({
  					webName: "sea_nwh-events",
  	                spudType: "datefinder",
  	                openInNewWindow: false,
  	                teaserBase: "<?php echo trailingslashit(site_url())?>patient-information/classes/northwest-hospital-events-calendar/",
  	                url: {headinglevel: 2}
                  });
  			</script>
  		  </div>
  	  </div>
    </div><!-- /#homepage-events-calendar -->
    <div id="featured-events">
      <h2>Save the Date</h2>
      <?php $featured_events_spotlight_box = get_field('featured_events_spotlight_box'); ?>
      <?php if( $featured_events_spotlight_box ) : ?>
      <div id="spotlight-box" class="cta-box carousel slide" data-ride="carousel" data-interval="false">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
        <?php $spotlight_box_slide_count = 0; ?>
        <?php while (have_rows('featured_events_spotlight_box')) : the_row();
          $title = get_sub_field('title');
          $image = get_sub_field('image');
          $description = get_sub_field('description');
          $call_to_action = get_sub_field('call_to_action');
          $url = get_sub_field('url');
          ?>
          <?php if ($spotlight_box_slide_count === 0) : ?>
          <div class="item active">
          <?php else: ?>
          <div class="item">
          <?php endif ?>
            <div class="cta-content">
              <h3><?php echo $title;?></h3>
              <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" />
              <?php echo $description; ?>
              <?php if($call_to_action): ?><p class="call-to-action"><a class="btn btn-green" href="<?php echo $url;?>"><?php echo $call_to_action;?></a></p><?php endif ?>
            </div><!-- /.cta-content -->
          </div><!-- /.item [active] -->
        <?php $spotlight_box_slide_count++;
        endwhile; ?>
        </div><!-- /.carousel-inner -->

        <!-- Controls -->
        <?php if( count($featured_events_spotlight_box) > 1 ) : ?>
          <div class="controls">
          <a class="left carousel-control" href="#spotlight-box" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>

          <!-- Indicators -->
          <ol class="carousel-indicators">
            <?php $spotlight_box_indicator_count = 0;
            while (have_rows('featured_events_spotlight_box')) : the_row();
              if ($spotlight_box_indicator_count === 0) : ?>
            <li data-target="#spotlight-box" data-slide-to="0" class="active"></li>
            <?php else: ?>
            <li data-target="#spotlight-box" data-slide-to="<?php echo $spotlight_box_indicator_count; ?>"></li>
            <?php endif;
            $spotlight_box_indicator_count++;
            endwhile; ?>
          </ol><!-- /.carousel-indicators -->

          <a class="right carousel-control" href="#spotlight-box" role="button" data-slide="next">
            <span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          </div><!-- /.controls -->
          <?php endif; ?>
        </div><!-- /#spotlight-box .cta-box .carousel .slide -->
      <?php endif?>
    </div><!-- /#featured-events -->
    <div class="visible-xs-block" id="calendar-button">
      <p><a class="btn" href="<?php echo trailingslashit(site_url())?>patient-information/classes/northwest-hospital-events-calendar/">View Calendar of Events</a></p>
    </div>
  </div><!-- /.row -->
</div><!-- /.container -->
<?php $locations_map = have_rows('locations_map'); ?>
<?php if( $locations_map ) : ?>
  <?php while( have_rows('locations_map') ) : the_row(); ?>
    <?php $map_image = get_sub_field('map_image'); ?>
    <div id="homepage-clinic-map-container" class="container-fluid">
      <a id="map-link" href="<?php echo get_sub_field('location_page')?>">
        <div id="homepage-clinic-map" style="background-image: url(<?php echo $map_image['url']?>);">
          <span class="sr-only">Northwest Hospital Location Directory</span>
        </div><!-- /#homepage-clinic-map -->
      </a>
    </div><!-- /.container-fluid -->
  <?php endwhile ?>
<?php endif ?>
