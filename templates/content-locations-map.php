<script>
function initMap() {

  var map = new google.maps.Map(document.getElementById('map'), {
    disableDefaultUI: true
  });

  var locations = <?php echo json_encode($locations); ?>;
  var locations_max = <?php echo $locations_max; ?>;
  var bounds = new google.maps.LatLngBounds();

  var labels = <?php echo json_encode($labels); ?>;

  for (i = 0; i < locations_max; i++) {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i]['Latitude'], locations[i]['Longitude']),
      label: labels[i],
      map: map});

    //extend the bounds to include each marker's position
    bounds.extend(marker.position);
  }
  //now fit the map to the newly inclusive bounds
  map.fitBounds(bounds);

  var listener = google.maps.event.addListener(map, "idle", function () {
    map.setZoom(11);
    google.maps.event.removeListener(listener);
  });
};
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAClfj1QHSxjcwOYbXdrwT4w9ocopzoJo0&callback=initMap">
</script>
