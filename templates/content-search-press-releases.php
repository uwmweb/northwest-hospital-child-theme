<article class="press-release-entry">
  <header>
    <?php $parent_heading = get_the_title($post->post_parent);
    $parent_link = get_permalink($post->post_parent); ?>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <p><span class="entry-type">Press Release</span> &mdash; Original Release Date: <?php echo get_field("original_release_date") ?></p>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>
