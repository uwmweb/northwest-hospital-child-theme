<?php
  // Here we're setting on-page navigation to be the same as
  // /about-northwest-hospital

  // Set Nav Parent to the /about-northwest-hospital page
  $nav_parent = get_post(11);
  $nav_heading = get_the_title($nav_parent);
  $nav_heading_link = get_permalink($nav_parent);
  $nav_depth = 1;
  $nav_pages = wp_list_pages( array('title_li'=>'','depth'=>$nav_depth, 'child_of'=>$nav_parent->ID, 'sort_column'=>'menu_order', 'echo'=>0 )); ?>

  <div id="sidebar-nav-toggle">
    <h3 id="sidebar-nav-heading">
      <a id="sidebar-nav-heading-link" href="<?php echo $nav_heading_link; ?>"><?php echo $nav_heading; ?> <span class="visible-xs-inline">Menu <i id="sidebar-nav-heading-down-arrow" class="fa fa-chevron-down"></i></span></a>
    </h3>
  </div>
  <ul class="mobile-collapse">
    <?php echo $nav_pages; ?>
  </ul>

  <?php $left_sidebar = get_field("left_sidebar", $nav_parent->ID);
    if ($left_sidebar) {
        echo $left_sidebar;
    }
  ?>

  <?php
  /*  Set a local variable and use include syntax so variable is accessible
   *  from the referenced file.
   */
  $key_contact_parent = $nav_parent;
  include(locate_template('templates/sidebar-key-contacts.php')); ?>
