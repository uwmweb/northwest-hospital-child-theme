<?php
/**
 * Template Name: Child Providers page
 */
?>
<?php \Roots\Sage\Setup\define_current_template('page-child-providers.php'); ?>
<?php use Roots\Sage\NWH_Extras; ?>
<?php
  $location = ramp_facility_linked();
  $service = ramp_service_linked(); ?>

  <?php if( $location ) {
    $providers = $location["Providers"];
  }
  elseif ( $service ) {
    $providers = $service["Providers"];
  }

  $parent_heading = get_the_title($post->post_parent);
?>

<?php if ( function_exists('yoast_breadcrumb') )
{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

<div class="page-header">
  <h1><?php echo $parent_heading; ?></h1>
</div>

<?php
    $right_sidebar = get_field("right_sidebar");
    $has_widgets = have_rows("right_sidebar_widgets");

    if ( $right_sidebar || $has_widgets ) : ?>
      <div class="row">
        <div id="content" class="col-md-8">
          <h2><?php echo get_the_title(); ?></h2>
          <?php the_content(); ?>
        </div>
        <div id="right-sidebar" class="col-md-4">
          <?php get_template_part("templates/sidebar-right-widgets"); ?>
          <?php echo $right_sidebar; ?>
        </div>
      </div>
    <?php else : ?>
      <h2><?php echo get_the_title(); ?></h2>
      <?php the_content(); ?>
  <?php endif ?>

<?php if($providers) : ?>
  <ul class="providers list list-unstyled row">
  <?php foreach ($providers as $provider) : ?>
    <?php include(locate_template('templates/content-providers.php')); ?>
  <?php endforeach ?>
</div>
<?php else : ?>
  <p>I'm sorry, we couldn't locate any care providers for this location.</p>
<?php endif ?>
