<?php \Roots\Sage\Setup\define_current_template('archive-patient-stories.php'); ?>
<section id="patient-stories-archive">
  <?php if ( function_exists('yoast_breadcrumb') )
  {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

  <div class="page-header">
    <h1>Patient Stories</h1>
  </div>

  <?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
      <?php _e('Sorry, no results were found.', 'sage'); ?>
    </div>
    <?php get_search_form(); ?>
  <?php endif; ?>

  <?php $post_count = $wp_query->post_count; ?>
  <?php $posts_per_page = $wp_query->query_vars['posts_per_page']; ?>

  <?php $num_entries_printed = 0; ?>
  <?php while (have_posts()) : the_post(); ?>
    <?php if( $num_entries_printed == 0 ) : ?>
      <?php // the first entry ?>
      <div class="patient-story-row">
        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php $num_entries_printed++; ?>
    <?php elseif($num_entries_printed % 2 !== 0) : ?>
      <?php // the second entry in a row and there are an even number of entries ?>
        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php $num_entries_printed++; ?>
      </div>
    <?php elseif($num_entries_printed % 2 == 0 ) : ?>
      <?php // the first entry in a row ?>
      <div class="patient-story-row">
        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php $num_entries_printed++; ?>
        <?php if($num_entries_printed == $post_count) : ?>
          <?php // the last entry and there are an odd number of entries ?>
          </div>
        <?php endif; ?>
    <?php endif;?>
  <?php endwhile; ?>
  <div class="pagination-container">
    <?php the_posts_pagination( array(
                                  'prev_text'          => __( '&laquo; Previous <span class="hidden-xs">Patient Stories</span>', 'sage' ),
                                  'next_text'          => __( '<span class="visible-xs-inline-block">Next &raquo;</span><span class="hidden-xs">More Patient Stories &raquo;</span>', 'sage' ),
                                  'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'sage' ) . ' </span>',
    	                        ) );
    ?>
  </div>
</section>
