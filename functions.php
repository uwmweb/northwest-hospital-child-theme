<?php
/**
 * NWH includes
 *
 * The $nwh_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$nwh_includes = array(
  'lib/nwh_assets.php',    // Scripts and stylesheets
  'lib/nwh_setup.php',     // Theme setup
  'lib/nwh_wrapper.php',   // Wrapper Setup
  'lib/nwh_extras.php',    // Extra functions
  'lib/Location/GeometryInterface.php', // Location Library (https://github.com/mjaschen/phpgeo)
  'lib/Location/Coordinate.php',
  'lib/Location/Ellipsoid.php',
  'lib/Location/Distance/DistanceInterface.php',
  'lib/Location/Distance/Vincenty.php',
  'lib/Location/Formatter/Coordinate/FormatterInterface.php',
  'lib/Location/Exception/NotConvergingException.php',
  'lib/Location/Exception/NotMatchingEllipsoidException.php',
  'lib/League/Csv/autoloader.php',
);

foreach ($nwh_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }
  require_once $filepath;
}
unset($file, $filepath);
