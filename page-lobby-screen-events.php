<?php
/*
Template Name: Lobby Screen Events Page

This is a minimal template for use on the NWH Lobby Screen to display events

*/
use League\Csv;
use League\Csv\Reader;

include_once(ABSPATH . WPINC . '/rss.php');

$calendar = wp_remote_get("https://www.trumba.com/calendars/sea_nwh-events.csv");

$reader = Reader::createFromString($calendar["body"]);
$keys = ["subject", "startDate", "startTime", "endDate", "endTime", "allDay", "description", "location", "webLink", "eventTemplate", "campusRoom", "eventTypes"];
$results = $reader->fetchAssoc($keys);

date_default_timezone_set('America/Los_Angeles');

?>

<h1><?php the_title(); ?></h1>

<?php

$startDate = null;
$endDate = null;
$currentDate = null;

foreach($results as $index => $row) {
    // Skip the header row
    if($index == 0) {continue;}
    
    // Set the start date to the date of the first result and initialize currentDate
    if($index == 1) {
        $startDate = new DateTime($row["startDate"]);
        $currentDate = $startDate;
        //echo("<h2>" . $currentDate->format("l, F j, Y") . "</h2>");
        $endDate = $startDate->modify("+14 days")->format('n/d/Y');        
    }

    // Display 15 events at most
    if($index <= 13) {
        // Iterate the rows through the end date
        if(strcmp($row["startDate"], $endDate) <= 1) {
            // If the event starts on the current date, then just output the event details
            if( strcmp($row["startDate"], $currentDate->format('n/d/Y')) == 0) : ?>

                <tr>
                    <td><?php $st = DateTime::CreateFromFormat('G:i:s', $row["startTime"]); echo str_replace('m', '.m.', $st->format("g:i a")); ?></td>
                    <td><?php echo $row["subject"]; ?></td>
                    <td><?php echo $row["campusRoom"]; ?></td>
                </tr>
            <?php // If the event starts on a new date, close the previous table, create a header, a new table and then output the event details
            else : 
                $currentDate = new DateTime($row["startDate"]); 
                if($index !== 1) : ?>
                    </tbody>
                    </table>
                <?php endif; ?>
                <h2><?php echo $currentDate->format("l, F j, Y"); ?></h2>
                <table class="table table-striped" style="width:100%;">
                    <thead>
                        <th style="width: 15%;">Time</th>
                        <th style="width: 50%;">Event</th>
                        <th style="width: 35%;">Campus Room</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php $st = DateTime::CreateFromFormat('G:i:s', $row["startTime"]); echo str_replace('m', '.m.', $st->format("g:i a")); ?></td>
                            <td><?php echo $row["subject"]; ?></td>
                            <td><?php echo $row["campusRoom"]; ?></td>
                        </tr>
            <?php endif; 
        }    
    } else {
        echo("</tbody></table>");
        break;
    }
}
?>
<p id="more">For more details, visit: nwhospital.org/events</p>