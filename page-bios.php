<?php
/*
Template Name: Bio Master Page

This template determines whether to display the individual provider template
or the provider search template. Extra classes are added to the individual
provider template in extras.php.

*/
?>

<?php $provider = ramp_provider();

  if( $provider ) {
    include(locate_template('page-providers.php'));
  } else {
    include(locate_template('page-ramp-search-provider.php'));
  }

?>
