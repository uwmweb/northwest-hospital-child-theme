<?php
/**
 * Template Name: Front Page
 */
?>
<?php use Roots\Sage\NWH_Extras; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'home'); ?>
<?php endwhile; ?>