var checkSize = function() {

  if( jQuery(".sidebar").css("float") !== "left" ) {
    // do this stuff when the screen is xs size
    // bootstrap media query removes float from sidebar in xs

    // move key contacts below content
    jQuery("#key-contacts").detach().insertAfter(".main");

    // default to hiding the sidebar menu
    jQuery(".mobile-collapse").hide();

    // disable sidebar nav heading link
    jQuery( "#sidebar-nav-heading-link" ).off("click.disableLink").on("click.disableLink", function(e) {
      e.preventDefault();
    });

    // add click toggle to sidebar nav heading div
    jQuery( "#sidebar-nav-toggle" ).off("click.toggle").on("click.toggle", function() {
      jQuery( ".mobile-collapse" ).toggle( "fast", function() {
        // Change down chevron to close sign
        if ( jQuery("#sidebar-nav-heading-down-arrow").prop("class") === "fa fa-chevron-down") {
          jQuery("#sidebar-nav-heading-down-arrow").prop("class", "fa fa-times-circle");
        }
        else {
          jQuery("#sidebar-nav-heading-down-arrow").prop("class", "fa fa-chevron-down");
        }
      });
    });
  }
  else if ( jQuery(".sidebar").css("float") === "left" ) {
    //do the following when the screen is sm size or up

    // make sure sidebar nav is showing
    jQuery(".mobile-collapse").show();

    // re-enable sidebar nav heading link
    jQuery( "#sidebar-nav-heading-link" ).off("click.disableLink");

    // remove click toggle from sidebar nav heading
    jQuery("#sidebar-nav-toggle").off("click.toggle");

    // move key contacts back into sidebar
    jQuery("#key-contacts").detach().appendTo(".sidebar");
  }
};

var setCurrentPageClass = function() {
  // see if we're on a page matching a custom nav element and add proper class
  var url = jQuery(location).attr('href').split("/");

  // using length - 2 because most WP URLs have trailing slash
  var pageurl = url[url.length - 2];

  jQuery(".sidebar ul li a").each(function(){
      //TODO optimize for cases when there's no trailing slash on URL
      link = jQuery(this).prop("href").split("/");

      if(link[link.length - 2] === pageurl) {
        jQuery(this).parent('li').addClass("current_page_item");
      }
    });
};

// when the document is ready
jQuery(function() {
  // move sidebar element to before main element for poper stacking
  // this can't be done in templates because of wordpress template hierarchy
  jQuery(".sidebar.move-to-top").detach().insertBefore(".main");

  setCurrentPageClass();
  checkSize();

  var initial_width = jQuery(window).width();
  // run test on resize of the window
  jQuery(window).resize(function() {
    if (jQuery(window).width() !== initial_width) {
      checkSize();
      initial_width = jQuery(window).width();
    }
  });
});


// https://www.fourfront.us/blog/jquery-window-width-and-media-queries
