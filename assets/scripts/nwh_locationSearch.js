jQuery(document).ready(function(){

  // Helper Functions
  var convertZip = function(zipCode, callback) {
    if(zipCode) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({'address': zipCode }, function(results, status){
        var lat = 0;
        var lng = 0;
        if(status === 'OK') {
          lat = results[0].geometry.location.lat();
          lng = results[0].geometry.location.lng();
          callback([lat, lng]);
        }
      });
    }
  };

  var getAddrForLocation = function(coords, callback) {
    if(coords.latitude && coords.longitude) {
      var geocoder = new google.maps.Geocoder();
      var latLng = {lat: parseFloat(coords.latitude), lng: parseFloat(coords.longitude)};
      
      geocoder.geocode({'location': latLng}, function(results, status){
        if(status === 'OK') {
          callback(results);
        }
      });
    }
  };
  
  var getQueryStringValue = function (key) {
      var query = location.search;
      var reg = new RegExp(key + '=([^&?]*)', 'i');
      var string = reg.exec(query);
      return string ? string[1] : null;
  };

  var sortLocationsByDist = function(lat, lng, dist) {
    var enteredQuery = jQuery('#typeahead-locations').val();
    var query = getQueryStringValue('query');

    if(enteredQuery) {
      query = enteredQuery.replace(/['!"#$%&\\()*+,.\/:;<=>?@\[\\\]^_`{|}~']/g, "").replace(/\s{2,}/g, " ").toLowerCase().split(/[\s+]|[\-]/).join("+");
    }

    if(lat && lng && dist) {
      var lld = "uLat=" + lat + "&uLng=" + lng;

      if(dist >= 0 ) {
        lld += "&uDist=" + dist;
      }

      if(query) {
        window.location.search = 'query=' + query + "&" + lld;
      } else {
        window.location.search = lld;
      }
    }
  };

  // Remove Zip, Dist from Session Storage on fresh loads of /locations
  var uLat = getQueryStringValue("uLat");
  var uLng = getQueryStringValue("uLng");

  if( uLat === null && uLng === null) {
    window.sessionStorage.removeItem("enteredZip");
    window.sessionStorage.removeItem("enteredDist");
  }

  var enteredZip = window.sessionStorage.getItem("enteredZip");
  var enteredDist = window.sessionStorage.getItem("enteredDist");

  // Add notificatoin, populate Zip and Distance from Session Storage
  if(enteredZip) {
    jQuery("#zipInput").val(enteredZip);
    jQuery("select[name=uDist]").val(enteredDist);  
    jQuery("#clearButton").removeClass("disabled");
    jQuery("#clearButton").attr("disabled",false);

    var notification = "";      

    if(enteredDist === "0" ) {
       notification = "These results are sorted by their distance away from zip code <strong>" + enteredZip + "</strong>.";
    } else {
        notification = "These results are filtered to display locations within <strong>" + enteredDist + " ";    
        if (enteredDist === 1) {
            notification += "mile ";
        } else {
            notification += "miles "; 
        }
        notification += "</strong> of zip code <strong>" + enteredZip + "</strong>.";
    }      
    jQuery("#activeFilterNotification").html(notification).css('display', 'block');
  }
  
  // Add uLat, uLng, uDist to location-search form
  jQuery("#location-search").submit(function(ev){
    //ev.preventDefault();
    var uLat = getQueryStringValue("uLat");
    var uLng = getQueryStringValue("uLng");
    var uDist = getQueryStringValue("uDist");

    if(uLat && uLng) {
      jQuery(this).append('<input type="hidden" name="uLat" value=' + uLat + ' />');
      jQuery(this).append('<input type="hidden" name="uLng" value=' + uLng + ' />');
    }

    if(uDist) {
      jQuery(this).append('<input type="hidden" name="uDist" value=' + uDist + ' />');
    }
  });

  // Set Events for Buttons
  jQuery("#location-filters").submit(function(ev){
    ev.preventDefault();

    var enteredZip = jQuery("#zipInput").val();
    var enteredDist = jQuery("#distanceSelect").val();
    
    if(enteredZip && enteredDist) {
      // check if we already have coords for this zipcode
      var storedZip = window.sessionStorage.getItem("enteredZip");
      if(storedZip === enteredZip) {
        var zipCoordsAvailable = window.sessionStorage.getItem("zipCoordsAvailable");
        if(zipCoordsAvailable === "true") {
          window.sessionStorage.setItem("enteredDist", enteredDist);
          sortLocationsByDist(window.sessionStorage.getItem("zipLat"), window.sessionStorage.getItem("zipLng"), enteredDist);
        }
      } else {
        window.sessionStorage.setItem("enteredZip", enteredZip);
        window.sessionStorage.setItem("enteredDist", enteredDist); 
        convertZip(enteredZip, function(coords) {
          if(coords) {
            var lat = coords[0];
            var lng = coords[1];
            window.sessionStorage.setItem("zipLat", lat);
            window.sessionStorage.setItem("zipLng", lng);
            window.sessionStorage.setItem("zipCoordsAvailable", true);
            sortLocationsByDist(lat, lng, enteredDist);
          } else {
            window.sessionStorage.setItem("zipCoordsAvailable", false);    
          }
        });
      }
    } else if (enteredZip === ""){
      jQuery("#clearButton").trigger("click");
    }
  });

  jQuery("#useLocButton").on("click", function(ev) {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        window.sessionStorage.setItem("locLat", lat);
        window.sessionStorage.setItem("locLng", lng);
        window.sessionStorage.setItem("locCoordsAvailable", true);
        getAddrForLocation(position.coords, function(results){
          if(results[0]) {
            window.sessionStorage.setItem('locAddressAvailable', true);
            window.sessionStorage.setItem('locAddress', results[0].formatted_address);
            window.sessionStorage.setItem("locFilterActive", true);
            sortLocationsByDist(lat, lng, -1);
          } else {
            window.sessionStorage.setItem('locAddressAvailable', false);
            window.sessionStorage.removeItem('locAddress');
          }
        });
      });
      window.sessionStorage.removeItem("enteredZip");
      window.sessionStorage.removeItem("enteredDist");
    } else {
      window.sessionStorage.setItem("locCoordsAvailable", false);
    }
  });

  jQuery("#distFilterButton").on("click", function(ev) {
    jQuery("#location-filters").submit();
  });

  jQuery("#zipInput").on("keydown", function(ev) {
    if(ev.which === 13) {
      jQuery("#location-filters").submit();
    }
  });

  jQuery("#clearButton").on("click", function(ev){
    window.sessionStorage.removeItem("enteredZip");
    window.sessionStorage.removeItem("enteredDist");
    window.sessionStorage.removeItem("locFilterActive");
    window.location.search = '';
  });

  // Display message when location filter is active
  var locFilterActive = window.sessionStorage.getItem("locFilterActive");
  if(locFilterActive === "true") {
    var uDist = getQueryStringValue("uDist");
    uLat = getQueryStringValue("uLat");
    uLng = getQueryStringValue("uLng");

    if(!uDist) {
      if(uLat && uLng) {
        jQuery("#clearButton").removeClass("disabled");
        jQuery("#clearButton").attr("disabled",false);
        var locAddress = window.sessionStorage.getItem('locAddress');
        var notificationLocFilter = "These results are sorted by their distance away from <strong>" + locAddress + "</strong>.";
        jQuery("#activeFilterNotification").html(notificationLocFilter).css('display', 'block');
      } else {
        window.sessionStorage.setItem("locFilterActive", false);
      }
    } else if ( uDist) {
      // remove locFilterActive if there if uDist is present since that means we're doing a Zip search
      window.sessionStorage.removeItem("locFilterActive");
    }
  }

  // Toggle display of "Show/Hide Filters" Button and save state to sessionStorage    
  jQuery("#toggleFiltersButton").on("click", function (ev) {
    if (jQuery(this).text() === "Show Filters") {
        jQuery("#filters").show(200);
        jQuery(this).text("Hide Filters");
        window.sessionStorage.setItem("filterDisplayState", "displayed");
    } else if(jQuery(this).text() === "Hide Filters") {
        jQuery("#filters").hide(200);
        jQuery(this).text("Show Filters");
        window.sessionStorage.setItem("filterDisplayState", "hidden");
    } 
  });

  // Toggle Display of "Show/Hide Filters" Button when screen is resized    
  jQuery(window).resize(function () {
    var screenMd = window.matchMedia("(min-width: 992px)");
    if (screenMd.matches === true) {
      if(jQuery("#filters").css("display") !== "block") {
        jQuery('#filters').css("display", "block");
      }
    }

    if (screenMd.matches === false) {
        if (window.sessionStorage.getItem("filterDisplayState") !== "displayed") {
            jQuery('#filters').css("display", "none");
        } else {
            jQuery('#filters').css("display", "block");
        }
    }
  });

});