jQuery(document).ready(function () {
    if (window.sessionStorage.getItem("isProviderSearchPage") === "true") {
        // ListJS Pagination Settings
        var resultsPerPage = 10;
        var topPaginationOptions = {
            name: "topPagination",
            paginationClass: "providers-paginationTop",
            innerWindow: 2,
            outerWindow: 2,
            left: 1,
            right: 1
        };

        var bottomPaginationOptions = {
            name: "bottomPagination",
            paginationClass: "providers-paginationBottom",
            innerWindow: 2,
            outerWindow: 2,
            left: 1,
            right: 1
        };

        // ListJS Options
        var listOptions = {
            valueNames: [
                'fullName',
                { data: ['gender'] },
                { name: 'languages', attr: 'data-languages' },
                { name: 'expertiseIds', attr: 'data-expertise-ids' },
                { name: 'expertiseNames', attr: 'data-expertise-names' }
            ],
            page: resultsPerPage,
            plugins: [
                ListPagination(topPaginationOptions),
                ListPagination(bottomPaginationOptions),
            ],
            searchClass: '' // manually invoke search
        };

        // Instantiate ListJS on the Providers
        var providerList = new List('paginated-provider-search', listOptions);

        // Helper Functions
        // ListJS Custom Search Function
        var customNameExpertiseSearch = function (searchString) {
            // remove punctuation and extra spaces from search string, convert to lowercase and split on 1 or more spaces or '-'
            var query = searchString.replace(/['!"#$%&\\()*+,.\/:;<=>?@\[\\\]^_`{|}~']/g, "").replace(/\s{2,}/g, " ").toLowerCase().split(/[\s+]|[\-]/);

            providerList.items.forEach(function (provider, index) {
                var nameParts = provider.values().fullName.toLowerCase().split(/[\s+]|[\-]/);
                var expertiseParts = provider.values().expertiseNames.toLowerCase().split(/[\s+]/);
                var matches = { "N": false, "E": false };

                query.forEach(function (queryTerm, index) {
                    if (queryTerm !== " ") {
                        var re = new RegExp(queryTerm);
                        matches.N = nameParts.find(function (namePart) {
                            return re.test(namePart);
                        });
                        matches.E = expertiseParts.find(function (expertisePart) {
                            return re.test(expertisePart);
                        });
                    }
                });
                provider.found = (matches.N || matches.E);
            });
        };

        // ListJS Custom Filter Function
        var filterProviders = function () {
            var expertiseInput = jQuery("select[name='specialty'] option:selected").val().split(",");
            var languageInput = jQuery("select[name='language'] option:selected").val().split(",");
            var genderInput = jQuery("select[name='gender'] option:selected").val().split(",");

            providerList.filter(function (item) {
                var matchE = false, matchL = false, matchG = false;

                var expertises = item.values().expertiseIds.split(",");
                matchE = expertiseInput.some(function (expertiseId) {
                    return expertises.indexOf(expertiseId) >= 0;
                });

                var languages = item.values().languages.split(",");
                matchL = languageInput.some(function (languageName) {
                    return languages.indexOf(languageName) >= 0;
                });

                var gender = item.values().gender;
                matchG = genderInput.some(function (genderMarker) {
                    return gender.indexOf(genderMarker) >= 0;
                });

                if (matchE && matchL && matchG) {
                    return true;
                }
            });
        };

        var notifyActiveFilters = function () {
            var activeFilters = [];
            if (jQuery("select[name='specialty']").val() !== '') {
                activeFilters.push("specialty");
            }

            if (jQuery("select[name='language']").val() !== '') {
                activeFilters.push("language");
            }

            if (jQuery("select[name='gender']").val() !== 'F,M') {
                activeFilters.push("gender");
            }

            var notification = '';
            switch (activeFilters.length) {
                case 0:
                    jQuery("#activeFilterNotification").html('').css('display', 'none');
                    break;
                case 1:
                    notification = "These results are currently filtered by <strong>" + activeFilters[0] + "</strong>.";
                    jQuery("#activeFilterNotification").html(notification).css('display', 'block');
                    break;
                case 2:
                    notification = "These results are currently filtered by <strong>" + activeFilters[0] + "</strong> and <strong>" + activeFilters[1] + "</strong>.";
                    jQuery("#activeFilterNotification").html(notification).css('display', 'block');
                    break;
                case 3:
                    notification = "These results are currently filtered by <strong>" + activeFilters[0] + "</strong>, <strong>" + activeFilters[1] + "</strong> and <strong>" + activeFilters[2] + "</strong>.";
                    jQuery("#activeFilterNotification").html(notification).css('display', 'block');
                    break;
            }
        };

        var toggleClearButton = function () {
            if ((jQuery("select[name='specialty']").val() === "") && (jQuery("select[name='language']").val() === "") && (jQuery("select[name='gender']").val() === 'F,M')) {
                jQuery("#clearButton").addClass("disabled");
                jQuery("#clearButton").attr("disabled", true);
            } else {
                jQuery("#clearButton").removeClass("disabled");
                jQuery("#clearButton").attr("disabled", false);
            }
        };

        var setFilterDisplayState = function (state) {
            // save the toggle state of the filters to sessionStorage
            window.sessionStorage.setItem("filterDisplayState", state);
        };

        var checkNoResults = function () {
            if (providerList.visibleItems.length === 0) {
                jQuery("#no-results").toggle(true);
                jQuery(".providers-pagination-label").toggle(false);
            } else {
                jQuery("#no-results").toggle(false);
                jQuery(".providers-pagination-label").toggle(true);
                jQuery("#toggleFiltersButton").toggle(true);
            }
        };

        var clearFilters = function (ev) {
            jQuery("select[name='specialty']").val('');
            jQuery("select[name='language']").val('');
            jQuery("select[name='gender']").val('F,M');
            providerList.filter();
        };

        // Generate Language List
        var generateLanguageFilterList = function () {
            var providerLanguages = [];
            providerList.items.map(function (provider) {
                var pLangs = provider.values().languages.split(",");
                jQuery.each(pLangs, function (index, pLang) {
                    if (pLang !== "" && jQuery.inArray(pLang, providerLanguages) < 0) {
                        providerLanguages.push(pLang);
                    }
                });
            });

            // Sort Language List Alphabetically
            providerLanguages.sort(function (a, b) {
                a = a.toLowerCase();
                b = b.toLowerCase();
                if (a < b) { return -1; }
                if (a > b) { return 1; }
                return 0;
            });

            // Populate Language Filter Dropdown with Lanugage List
            var langSelect = jQuery("select[name='language']");
            jQuery.each(providerLanguages, function (index, pLang) {
                var langOption = jQuery("<option>", { value: pLang, text: pLang });
                langSelect.append(langOption);
            });
        };
        
        var getHashStringValue = function (key) {
            var hash = location.hash.split('#')[1];
            var reg = new RegExp(key + '=([^&?]*)', 'i');
            var string = reg.exec(hash);
            return string ? string[1] : null;
        };

        // Populate form fields based on hash string
        var populateFilters = function () {
            var hashQuery = decodeURIComponent(getHashStringValue('query'));
            var hashExpertise = decodeURIComponent(getHashStringValue('expertise'));
            var hashLanguage = decodeURIComponent(getHashStringValue('language'));
            var hashGender = decodeURIComponent(getHashStringValue('gender'));

            if (hashQuery !== "null") {
                jQuery("#provider-search input.search-field").val(hashQuery);
            } else if (hashQuery === "null") {
                jQuery("#provider-search input.search-field").val('');
            }
            
            if (hashExpertise !== "null") {
                jQuery("select[name='specialty']").val(hashExpertise);
                toggleClearButton();
            } else if (hashExpertise === "null") {
                jQuery("select[name='specialty']").val('');
                toggleClearButton();
            }
            
            if (hashLanguage !== "null") {
                jQuery("select[name='language']").val(hashLanguage);
                toggleClearButton();
            } else if (hashLanguage === "null") {
                jQuery("select[name='language']").val('');
                toggleClearButton();
            }

            if (hashGender !== "null") {
                jQuery("select[name='gender']").val(hashGender);
                toggleClearButton();
            } else if (hashGender === "null") {
                jQuery("select[name='gender']").val('F,M');
                toggleClearButton();
            }
        };

        var updateHashStringKey = function (key, value) {
            var currHash = location.hash.split("#")[1];

            if (currHash !== undefined ) {
                var reg = new RegExp('(&?)' + key + '=(&?)([^&?]*)(&?)', 'i');
                var string = reg.exec(currHash);
                
                if (string === null) {
                    // there's a hash string but this key doesn't exist
                    if (key && value) {
                        //append key to end of hash string
                        location.hash = currHash + "&" + key + "=" + value;    
                    }
                } else {
                    // there's a hash string and this key exists
                    if (key && value) {
                        //update the key with the new value
                        var updatedKey = '';
                        if (string[1] === "&") {
                            updatedKey = "&" + key + "=" + value;    
                        } else if(string[4] === "&") {
                            updatedKey = key + "=" + value + "&";
                        } else {
                            updatedKey = key + "=" + value;
                        }
                        location.hash = currHash.replace(string[0], updatedKey);
                    } else if (key) {
                        // remove the key and its value 
                        location.hash = currHash.replace(string[0], '');
                    }
                }
            } else {
                // no hash exists yet
                if (key && value) {
                    //append key to end of hash string
                    location.hash = key + "=" + value;    
                }
            }
        };

        // var replaceHashStringValue = function (key) {
        //     var hash = location.hash.split("#")[1];
        //     var reg = new RegExp(key + '=([^&?]*)(&?)', 'i');
        //     var string = reg.exec(hash);
        //     if (string !== null) {
        //         location.hash = location.hash.replace(string[0], )
        //     }
        // }
        
        var updateURLHash = function (key, value) {
            switch (key) {
                case "query":
                    var hashQuery = decodeURIComponent(getHashStringValue('query'));
                    if (!value) {
                        updateHashStringKey("query", null);
                    } else {
                        updateHashStringKey("query", encodeURIComponent(value));
                    }
                    break;
                case "expertise":
                    var hashExpertise = decodeURIComponent(getHashStringValue('expertise'));
                    if (!value) {
                        updateHashStringKey("expertise", null);
                    } else {
                        updateHashStringKey("expertise", encodeURIComponent(value));
                    }
                    break;
                case "language": 
                    var hashLanguage = decodeURIComponent(getHashStringValue('language'));
                    if (!value) {
                        updateHashStringKey("language", null);
                    } else {
                        updateHashStringKey("language", encodeURIComponent(value));
                    }
                    break;
                case "gender":
                    var hashGender = decodeURIComponent(getHashStringValue('gender'));
                    if (!value) {
                        updateHashStringKey("gender", null);
                    } else if (value === "F,M") {
                        updateHashStringKey("gender", null);
                    } else {
                        updateHashStringKey("gender", value);
                    }
                    break;
            }
            // var hashQuery = decodeURIComponent(getHashStringValue('query'));
            // var hashExpertise = decodeURIComponent(getHashStringValue('expertise'));
            // location.hash = "#query=" + encodeURIComponent(query);
        };

        // Set element events 
        jQuery("select[name='specialty']").change(function () {
            updateURLHash("expertise", jQuery("select[name='specialty']").val());
            filterProviders();
            notifyActiveFilters();
            toggleClearButton();
            checkNoResults();
        });

        jQuery("select[name='language']").change(function () {
            updateURLHash("language", jQuery("select[name='language']").val());
            filterProviders();
            notifyActiveFilters();
            toggleClearButton();
            checkNoResults();
        });

        jQuery("select[name='gender']").change(function () {
            updateURLHash("gender", jQuery("select[name='gender']").val());
            filterProviders();
            notifyActiveFilters();
            toggleClearButton();
            checkNoResults();
        });

        jQuery('#provider-search').submit(function (ev) {
            ev.preventDefault();
            var selectedSuggestionURL = window.sessionStorage.getItem("selectedSuggestionURL");
            if (selectedSuggestionURL) {
                return;
            } else {
                var activeTypeaheadInput = window.sessionStorage.getItem("activeTypeaheadInput");
                var activeTypeaheadQuery = window.sessionStorage.getItem("activeTypeaheadQuery");
                jQuery(activeTypeaheadInput).typeahead('close');
                providerList.search(activeTypeaheadQuery, ['full-name', 'expertiseNames'], customNameExpertiseSearch);
                updateURLHash("query", activeTypeaheadQuery);
                jQuery(activeTypeaheadInput).blur();
                checkNoResults();
            }
        });

        jQuery("#typeahead-providers").keydown(function (ev) {
            if (ev.which === 13) {
                jQuery("#provider-search").submit();
            }
        });

        jQuery("#clearButton").on("click keydown", function (ev) {
            if (ev.which !== 9) {
                clearFilters(ev);
                notifyActiveFilters();
                toggleClearButton();
                checkNoResults();
                updateURLHash("expertise");
                updateURLHash("gender");
                updateURLHash("language");
            }
        });

        // Toggle display of "Show/Hide Filters" Button and save state to sessionStorage    
        jQuery("#toggleFiltersButton").on("click", function (ev) {
            if (jQuery(this).text() === "Show Filters") {
                jQuery("#filters").show(200);
                jQuery(this).text("Hide Filters");
                setFilterDisplayState("displayed");
            } else if(jQuery(this).text() === "Hide Filters") {
                jQuery("#filters").hide(200);
                jQuery(this).text("Show Filters");
                setFilterDisplayState("hidden");
            } 
        });

        // Toggle Display of "Show/Hide Filters" Button when screen is resized    
        jQuery(window).resize(function () {
            var screenMd = window.matchMedia("(min-width: 992px)");
            if (screenMd.matches === true && (jQuery("#filters").css("display") !== "block")) {
                jQuery('#filters').css("display", "block");
            }

            if (screenMd.matches === false) {
                if (window.sessionStorage.getItem("filterDisplayState") !== "displayed") {
                    jQuery('#filters').css("display", "none");
                } else {
                    jQuery('#filters').css("display", "block");
                }
            }
        });

        // Listen for hashchange and apply filters if appropriate
        jQuery(window).on("hashchange", function () {
            populateFilters();
            var query = (jQuery("#provider-search input.search-field").val());
            if (query !== "" && query !== undefined) {
                window.sessionStorage.setItem("activeTypeaheadInput", "#typeahead-providers");
                window.sessionStorage.setItem("activeTypeaheadQuery", query);
                providerList.search(query, ['full-name', 'expertiseNames'], customNameExpertiseSearch);
            }
            
            filterProviders();
            notifyActiveFilters();
            checkNoResults();
            // Check if the hashchange was a result of selecting a suggested expertise from the typeahead
            var selectedSuggestionURL = window.sessionStorage.getItem("selectedSuggestionURL");
            if (selectedSuggestionURL) {       
                window.sessionStorage.removeItem("selectedSuggestionURL");
                jQuery("#provider-search input.search-field").val('');
            }
        });
            
        // Once everything is set on page load: search, filter, notify, and check for no results found
        generateLanguageFilterList();
        populateFilters();
        var query = (jQuery("#provider-search input.search-field").val());
        if (query !== "" && query !== undefined) {
            window.sessionStorage.setItem("activeTypeaheadInput", "#typeahead-providers");
            window.sessionStorage.setItem("activeTypeaheadQuery", query);
            providerList.search(query, ['full-name', 'expertiseNames'], customNameExpertiseSearch);
        }
        
        filterProviders();
        notifyActiveFilters();
        checkNoResults();
                

    }

});
