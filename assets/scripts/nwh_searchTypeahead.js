jQuery(document).ready(function () {
  var servicePages = php_vars.servicePages;

  //Instantiate the Bloodhound suggestion engine for services
  var services = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: function () {
      return jQuery.map(servicePages, function (page) {
        return {
          name: page.post_title,
          id: page.ID,
          url: page.permalink,
          content: page.post_content
        };
      });
    },
  });

  var enabledClinics = php_vars.enabledClinics;

  // Instantiate the Bloodhound suggestion engine for clinics
  var clinics = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: {
      url: 'https://webservices.uwmedicine.org/api/search?affiliateid=16',
      filter: function (clinicData) {
        return jQuery.map(clinicData.clinics, function (clinic) {
          var clinicId = clinic.id;
          if (enabledClinics.hasOwnProperty(clinicId)) {
            return {
              name: clinic.name,
              id: clinic.id,
              url: enabledClinics[clinicId].WpUrl
            };
          }
        });
      },
      ttl: 10800000,
      thumbprint: 'homepageClinics'
    },
    remote: {
      url: 'https://webservices.uwmedicine.org/api/search?affiliateid=16&namecontains=%QUERY',
      wildcard: '%QUERY',
      filter: function (clinicResponse) {
        return jQuery.map(clinicResponse.clinics, function (clinic) {
          clinicId = clinic.id;
          if (enabledClinics.hasOwnProperty(clinicId)) {
            return {
              name: clinic.name,
              id: clinic.id,
              url: enabledClinics[clinicId].WpUrl
            };
          }
        });
      },
    },
  });

  var providers = new Bloodhound({
    datumTokenizer: function (datum) {
      return Bloodhound.tokenizers.whitespace(datum.name.replace(/\s\s+/g, ' ').trim());
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: {
      url: 'https://webservices.uwmedicine.org/api/search?affiliateid=16',
      filter: function (providerData) {
        return jQuery.map(providerData.bioinformations, function (bio) {
          return {
            name: bio.name.replace(/\s\s+/g, ' ').trim(),
            friendlyUrl: bio.friendlyUrl,
            url: '//' + location.hostname + '/bios/' + bio.friendlyUrl
          };
        });
      },
      ttl: 10800000,
      thumbprint: 'homepageProviders'
    },
    remote: {
      url: 'https://webservices.uwmedicine.org/api/search?affiliateid=16&namecontains=%QUERY',
      wildcard: '%QUERY',
      filter: function (providerResponse) {
        return jQuery.map(providerResponse.bioinformations, function (bio) {
          return {
            name: bio.name.replace(/\s\s+/g, ' ').trim(),
            friendlyUrl: bio.friendlyUrl,
            url: '//' + location.hostname + '/bios/' + bio.friendlyUrl
          };
        });
      },
    },
  });
  
  var expertises = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: function () {
      return jQuery.map(expertiseMapping, function (entry) {
        return {
          name: entry.expertise,
          url: '//' + location.hostname + "/bios/#expertise=" + entry.value,
          value: entry.value
        };
      });
    },
  });

  var typeaheadOptions = {
    hint: false,
    highlight: true,
  };

  var typeaheadClinicData = {
    name: 'clinics',
    display: 'name',
    source: clinics,
    templates: {
      header: '<h4>Clinic Locations</h4>',
      suggestion: Handlebars.compile('<a href="{{url}}">{{name}}</a>'),
      footer: Handlebars.compile('<a class="btn btn-blue tt-suggestion more" href="/search/?query={{query}}">View all results</a>')
    }
  };

  var typeaheadProviderData = {
    name: 'providers',
    display: 'name',
    source: providers,
    templates: {
      header: '<h4>Care Providers</h4>',
      suggestion: Handlebars.compile('<a data-value="{{friendlyUrl}}" href="{{url}}">{{name}}</a>'),
      footer: Handlebars.compile('<a class="btn btn-blue tt-suggestion  more" href="/search/?query={{query}}">View all results</a>')
    }
  };

  var typeaheadExpertiseData = {
    name: 'expertises',
    display: 'name',
    source: expertises,
    templates: {
      header: '<h4>Providers Who Specialize In</h4>',
      suggestion: Handlebars.compile('<a data-value="{{value}}" href="{{url}}">{{name}}</a>'),
      footer: Handlebars.compile('<a class="btn btn-blue tt-suggestion  more" href="/search/?query={{query}}">View all results</a>')
    }
  };

  var typeaheadServiceData = {
    name: 'services',
    display: 'name',
    source: services,
    templates: {
      header: '<h4>Medical Services</h4>',
      suggestion: Handlebars.compile('<a href="{{url}}">{{name}}</a>'),
      footer: Handlebars.compile('<a class="btn btn-blue tt-suggestion  more" href="/search/?query={{query}}">View all results</a>')
    }
  };

  // Add typeahead to the relevant search elements
  jQuery('#typeahead-default').typeahead(typeaheadOptions, [typeaheadProviderData, typeaheadExpertiseData, typeaheadClinicData, typeaheadServiceData]);
  jQuery('#typeahead-header').typeahead(typeaheadOptions, [typeaheadProviderData, typeaheadExpertiseData, typeaheadClinicData, typeaheadServiceData]);
  jQuery('#typeahead-footer').typeahead(typeaheadOptions, [typeaheadProviderData, typeaheadExpertiseData, typeaheadClinicData, typeaheadServiceData]);
  jQuery('#typeahead-providers').typeahead(typeaheadOptions, [typeaheadProviderData, typeaheadExpertiseData]);
  jQuery('#typeahead-locations').typeahead(typeaheadOptions, [typeaheadClinicData]);

  // Helpers Functions  
  var setActiveTypeaheadInput = function (ev) {
    // save the active input to sessionStorage
    var storedInput = window.sessionStorage.getItem("activeTypeaheadInput");
    var currentInput = "#" + ev.target.id;
    if (storedInput !== currentInput) {
      window.sessionStorage.setItem("activeTypeaheadInput", currentInput);
    }
  };

  var setActiveTypeaheadQuery = function () {
    // save the active input's query value to sessionStorage
    var currentInput = window.sessionStorage.getItem("activeTypeaheadInput");
    var storedQuery = window.sessionStorage.getItem("activeTypeaheadQuery");
    var currentQuery = jQuery(currentInput).typeahead('val');
    if (storedQuery !== currentQuery) {
      window.sessionStorage.setItem("activeTypeaheadQuery", currentQuery);
    }
    if (currentQuery === "") {
      window.sessionStorage.removeItem("activeTypeaheadQuery");
    }
  };

  var setActiveTypeaheadInputAction = function (suggestion) {
    // add data attributes to search form and active input to keep track of defaults
    var activeInput = window.sessionStorage.getItem("activeTypeaheadInput");
    var activeSearchForm = jQuery(activeInput).parents(".search-form").first();
    var action = jQuery(activeSearchForm).attr("action");
    var defaultAction = jQuery(activeSearchForm).attr("data-default-action");
    var inputName = jQuery(activeInput).attr("name");
    var defaultInputName = jQuery(activeInput).attr("data-default-name");

    if (suggestion) {
      // save the attribute values in data-default attributes
      if (defaultAction === undefined) {
        jQuery(activeSearchForm).attr("data-default-action", action);
      }

      if (defaultInputName === undefined) {
        jQuery(activeInput).attr("data-default-name", inputName);
      }
      
      var suggestionURL = suggestion.url ? suggestion.url : jQuery(suggestion).attr("href");

      jQuery(activeSearchForm).attr("action", suggestionURL);
      jQuery(activeInput).attr("name", "");
    } else {
      // restore the attributes to default values
      if (defaultAction !== undefined) {
        jQuery(activeSearchForm).attr("action", defaultAction);
      }

      if (defaultInputName !== undefined) {
        jQuery(activeInput).attr("name", defaultInputName);
      }

      // remove the data-default attributes
      jQuery(activeSearchForm).removeAttr("data-default-action");
      jQuery(activeInput).removeAttr("data-default-name");
    }
  };

  var setActiveTypeaheadSelectedSuggestionURL = function (suggestion) {
    var suggestionURL = suggestion.url ? suggestion.url : jQuery(suggestion).attr("href");
    window.sessionStorage.setItem("selectedSuggestionURL", suggestionURL);
  };
  
  var setSuggestedExpertises = function () {
    var activeInput = window.sessionStorage.getItem("activeTypeaheadInput");
    var expertiseSuggestions = jQuery(activeInput).siblings('.tt-menu').children('.tt-dataset-expertises').children('.tt-suggestion.tt-selectable');

    if (expertiseSuggestions.length > 0) {
      var suggestedExpertises = [];
      jQuery.each(expertiseSuggestions, function (index, expertiseSuggestion) {
        suggestedExpertises.push({ name: jQuery(expertiseSuggestion).text(), id: jQuery(expertiseSuggestion).attr("data-value") });
      });

      sessionStorage.setItem("suggestedExpertises", JSON.stringify(suggestedExpertises));
    } else {
      sessionStorage.removeItem("suggestedExpertises");
    }
  };

  var setSuggestedProviders = function () {
    var activeInput = window.sessionStorage.getItem("activeTypeaheadInput");
    var activeQuery = window.sessionStorage.getItem("activeTypeaheadQuery");
    var providerSuggestions = jQuery(activeInput).siblings('.tt-menu').children('.tt-dataset-providers').children('.tt-suggestion.tt-selectable');

    if (providerSuggestions.length > 0) {
      var suggestedProviders = [];
      jQuery.each(providerSuggestions, function (index, providerSuggestion) {
        suggestedProviders.push({ name: jQuery(providerSuggestion).text(), friendlyUrl: jQuery(providerSuggestion).attr("data-value") });
      });

      sessionStorage.setItem("suggestedProviders", JSON.stringify(suggestedProviders));
    } else {
      sessionStorage.removeItem("suggestedProviders");
    }
  };
  
  // Bind Events
  jQuery('.typeahead').on('typeahead:open', function (ev) {
    setActiveTypeaheadInput(ev);

    // only display the last "More" button
    jQuery(ev.target).parent(".twitter-typeahead").find('.more').hide();
    jQuery(ev.target).parent(".twitter-typeahead").find('.more').last().show();
  });

  jQuery('.typeahead').on('typeahead:close', function (ev) {
    setActiveTypeaheadQuery();
    setActiveTypeaheadInputAction();
    setSuggestedExpertises();
    setSuggestedProviders();
  });

  jQuery(".search-form").submit(function (ev) {
    setActiveTypeaheadQuery();
    setActiveTypeaheadInputAction();
    setSuggestedExpertises();
    setSuggestedProviders();
    jQuery(ev.target).find("button[type='submit']").focus();
  });
  
  // Accomodate for keyboard highlighting of suggestions. Default behavior is to go to the suggestion url
  // on click, so it should be with keyboard navigation as well.
  jQuery(".typeahead").on("typeahead:cursorchange", function (ev, suggestion) {
    setActiveTypeaheadInputAction(suggestion);
  });

  // Accomodate the case where a user clicks a suggestion and then also clicks the submit button  
  jQuery(".search-form").on("click", "a.tt-suggestion", function (ev) {
    var suggestion = ev.target;
    setActiveTypeaheadInputAction(suggestion);
  });

  // When a user selects a suggestion with the keyboard, ensure the action is the same as if they were clicking
  jQuery(".typeahead").on("typeahead:select", function (ev, suggestion) {
    setActiveTypeaheadInputAction(suggestion);
    setActiveTypeaheadSelectedSuggestionURL(suggestion);
    var activeSearchForm = jQuery(ev.target).parents(".search-form").first();
    var activeSearchFormAction = jQuery(activeSearchForm).attr("action");
    window.location = activeSearchFormAction;
  });

  jQuery(".typeahead").on("click", function (ev) {
    ev.target.focus();
    ev.preventDefault();
  });
  
  // Move tab focus to submit button when tab is pressed and autocomplete fired
  jQuery(".typeahead").on("typeahead:autocomplete", function () {
    var activeTypeahead = window.sessionStorage.getItem("activeTypeaheadInput");
    jQuery(activeTypeahead).closest(".search-form").children().find("button[type='submit']").focus();
  });

  // Remove the selected suggestion from sessionStorageURL on each page load
  window.sessionStorage.removeItem("selectedSuggestionURL");
});
