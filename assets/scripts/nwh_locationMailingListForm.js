jQuery(function () {
  var clinicName = jQuery("h1").text();
  jQuery("#clinicName").replaceWith(clinicName);

  jQuery("#form").validate({
    rules: {
      firstName: {
        required: true
      },
      lastName: {
        required: true
      },
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      firstName: {
        required: "Please enter your first name"
      },
      lastName: {
        required: "Please enter your last name"
      },
      email: {
        required: "Please enter your email",
        email: "Please enter a valid email"
      }
    },
    submitHandler: function(e) {
      lplib.submitForm('#form', function() {});
      jQuery('#submitForm').fadeOut();
      setTimeout(function() {
        jQuery('#formFlash').append("<p>Thank you for subscribing!</p>");
      }, 1000);
      return false;
    }
  });
});