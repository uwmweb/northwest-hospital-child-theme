var expertiseMapping = [
  {
    "expertise": "Allergy & Immunology",
    "value": "63,140"
  }, {
    "expertise": "Alternative Medicine",
    "value": "278"
  }, {
    "expertise": "Anesthesiology",
    "value": "51"
  }, {
    "expertise": "Audiology",
    "value": "4"
  }, {
    "expertise": "Behavioral Health",
    "value": "174,173"
  }, {
    "expertise": "Burn Care",
    "value": "6"
  }, {
    "expertise": "Cancer Care",
    "value": "105,153,189,252,125,52,145,99,295,146,154,155,158,156"
  }, {
    "expertise": "Cardiac Services/Cardiology",
    "value": "7,130,86"
  }, {
    "expertise": "Chronic Disease Management",
    "value": "168"
  }, {
    "expertise": "Colorectal Services",
    "value": "200,198"
  }, {
    "expertise": "Critical Care Medicine",
    "value": "101"
  }, {
    "expertise": "Dermatology",
    "value": "11"
  }, {
    "expertise": "Diabetes Care",
    "value": "12,14,138"
  }, {
    "expertise": "Ear, Nose & Throat (Otolaryngology)",
    "value": "33"
  }, {
    "expertise": "Electrophysiology-Arrhythmias",
    "value": "286,276"
  }, {
    "expertise": "Emergency Medicine",
    "value": "13,291"
  }, {
    "expertise": "Endocrinology",
    "value": "12,14,138"
  }, {
    "expertise": "Eye Care",
    "value": "31"
  }, {
    "expertise": "Family Medicine",
    "value": "53,36"
  }, {
    "expertise": "Gastroenterology",
    "value": "17"
  }, {
    "expertise": "Geriatrics",
    "value": "172,19"
  }, {
    "expertise": "Gynecology",
    "value": "54,125"
  }, {
    "expertise": "Hematology",
    "value": "21"
  }, {
    "expertise": "Hepatology",
    "value": "22"
  }, {
    "expertise": "Hospice & Palliative Medicine",
    "value": "201"
  }, {
    "expertise": "Hospital Medicine",
    "value": "79"
  }, {
    "expertise": "Infectious Disease Medicine",
    "value": "2"
  }, {
    "expertise": "Internal Medicine",
    "value": "23"
  }, {
    "expertise": "Kidney Care",
    "value": "25"
  }, {
    "expertise": "Lactation Services",
    "value": "165"
  }, {
    "expertise": "Lung Care",
    "value": "38"
  }, {
    "expertise": "Midwifery",
    "value": "126"
  }, {
    "expertise": "Nephrology",
    "value": "25"
  }, {
    "expertise": "Neurology",
    "value": "77,16,24,26,109,110,191,45"
  }, {
    "expertise": "Neurosurgery",
    "value": "27,104"
  }, {
    "expertise": "Nutrition",
    "value": "28,126"
  }, {
    "expertise": "Obstetrics & Gynecology",
    "value": "126,102"
  }, {
    "expertise": "Oncology",
    "value": "105,154,155,153,158,189,156,252,125,52,145,99,295,146"
  }, {
    "expertise": "Ophthalmology",
    "value": "190,31"
  }, {
    "expertise": "Oral & Maxillofacial Surgery",
    "value": "277,57"
  }, {
    "expertise": "Orthopedics",
    "value": "92,32,93,95,96,94,304,98,133,134"
  }, {
    "expertise": "Pain Medicine",
    "value": "8,60"
  }, {
    "expertise": "Pathology",
    "value": "62,59"
  }, {
    "expertise": "Pediatrics",
    "value": "34"
  }, {
    "expertise": "Physical Therapy",
    "value": "121"
  }, {
    "expertise": "Plastic & Reconstructive Surgery ",
    "value": "139,9,183"
  }, {
    "expertise": "Podiatry",
    "value": "89"
  }, {
    "expertise": "Preventive Medicine",
    "value": "65,35"
  }, {
    "expertise": "Primary Care",
    "value": "53,36"
  }, {
    "expertise": "Psychiatry",
    "value": "37"
  }, {
    "expertise": "Pulmonology",
    "value": "38"
  }, {
    "expertise": "Radiation Oncology",
    "value": "58"
  }, {
    "expertise": "Radiology & Imaging Services",
    "value": "285,3,39"
  }, {
    "expertise": "Reproductive Care",
    "value": "159,299,49"
  }, {
    "expertise": "Rheumatology",
    "value": "56"
  }, {
    "expertise": "Sleep Medicine",
    "value": "42,149"
  }, {
    "expertise": "Spine",
    "value": "43"
  }, {
    "expertise": "Sports Medicine",
    "value": "44"
  }, {
    "expertise": "Surgery",
    "value": "215,114,199,216,214,46,76"
  }, {
    "expertise": "Thoracic & Cardiovascular Surgery",
    "value": "130,303"
  }, {
    "expertise": "Transplantation Services",
    "value": "210,204,207,47"
  }, {
    "expertise": "Travel Medicine",
    "value": "48"
  }, {
    "expertise": "Urology",
    "value": "187,49"
  }, {
    "expertise": "Vascular Surgery",
    "value": "74,50"
  }, {
    "expertise": "Weight-Loss Surgery/Bariatrics",
    "value": "66,176"
  }, {
    "expertise": "Women's Health",
    "value": "159,54,126,102,73"
  }, {
    "expertise": "Wound Care",
    "value": "181"
  }];
