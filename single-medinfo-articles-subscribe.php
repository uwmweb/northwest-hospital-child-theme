<?php
\Roots\Sage\Setup\define_current_template('single-medinfo-articles-subscribe.php'); ?>


<p id="breadcrumbs">
  <span xmlns:v="http://rdf.data-vocabulary.org/#">
    <span typeof="v:Breadcrumb">
      <a href="http://nwhospital.wpengine.com" rel="v:url" property="v:title">Home</a> &raquo;
      <span rel="v:child" typeof="v:Breadcrumb">
        <a href="<?php echo trailingslashit(home_url('medinfo')) ?>" rel="v:url" property="v:title">MedInfo</a> &raquo;

            <span class="breadcrumb_last">
              Subscribe
            </span>

      </span>
    </span>
  </span>
</p>

<article <?php post_class(); ?>>
  <header>
    <h1 class="entry-title"><?php the_title(); ?></h1>
  </header>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.js"></script>
  <script src="<?php echo \Roots\Sage\Assets\asset_path('scripts/advisory-board-subscribe.js') ?>"></script>

  <?php the_content(); ?>
  <h2>Subscribe to MedInfo Magazine</h2>
  <div class="form-container usa-form" style="margin-top: 10px;">
  	<form id="form">
  <div class="row">
  		<div class="form-group col-md-6">
  			<label for="firstName">First Name
  				<span style="color: red;">*</span>
  			</label>
  			<input id="firstName" class="form-control" style="background: #f8f5e0;" name="firstName" type="text" />
  		</div>
  		<div class="form-group col-md-6">
  			<label for="lastName">Last Name
  				<span style="color: red;">*</span>
  			</label>
  			<input id="lastName" class="form-control" style="background: #f8f5e0;" name="lastName" type="text" />
  		</div>
  </div>
  <div class="row">
  		<div class="form-group col-md-12">
  			<label for="email">Email Address
  				<span style="color: red;">*</span>
  			</label>
  			<input id="email" class="form-control" style="background: #f8f5e0;" name="email" type="email" />
  		</div>
  </div>
  <div class="row">
    <div class="form-group col-md-4">
      <label for="zip">ZIP Code</label>
      <input id="zip" class="usa-input-medium form-control" style="background: #f8f5e0;" name="zip" type="text" />
    </div>
    <div class="form-group col-md-4">
      <label for="age">Age
      </label>
      <select id="age" class="form-control" style="background: #f8f5e0;" name="age">
        <option value="" disabled="disabled" selected="selected">Choose One</option>
        <option value="1">13 to 18</option>
        <option value="2">18 to 24</option>
        <option value="3">25 to 34</option>
        <option value="4">35 to 44</option>
        <option value="5">45 to 54</option>
        <option value="6">55 to 64</option>
        <option value="7">65+</option>
        <option value="0">Rather Not Say</option>
      </select>
    </div>
  </div>
  		<div class="form-group">
  			<fieldset>
  				<legend>Areas of Interest</legend>
  				<ul class="list-unstyled row">
  					<li class="col-sm-6">
  						<label>
  							<input id="cancer_care" type="checkbox" value="cancer_care" /> Cancer Care</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="cardiac_care" type="checkbox" value="cardiac_care" /> Cardiac Care</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="childbirth_obstetrics" type="checkbox" value="childbirth_obstetrics" /> Childbirth / Obstetrics</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="classes_support_groups" type="checkbox" value="classes_support_groups" /> Classes and Support Groups</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="diabetes_care" type="checkbox" value="diabetes_care" /> Diabetes Care</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="health_wellness" type="checkbox" value="health_wellness" /> Health &amp; Wellness</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="mens_health" type="checkbox" value="mens_health" /> Men's Health</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="neurosciences" type="checkbox" value="neurosciences" /> Neurosciences</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="orthopedics" type="checkbox" value="orthopedics" /> Orthopedics</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="primary_care" type="checkbox" value="primary_care" /> Primary Care</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="sports_medicine" type="checkbox" value="sports_medicine" /> Sports Medicine</label>
  					</li>
  					<li class="col-sm-6">
  						<label>
  							<input id="womens_health" type="checkbox" value="womens_health" /> Women's Health</label>
  					</li>
  					<li class="col-sm-12">
  						<label style="width: 50%;">
  							<input id="other" name="other" type="checkbox" value="other" /> Other</label>
            </li>
            <li class="col-sm-12 other_text_input">
              <label for="other_text">
  							<input id="other_text" class="form-control" style="background: #f8f5e0; margin-top: 5px;" name="other_text" type="text" /> <span id="other_text_label">Please Describe Other</span>
  						</label>
  					</li>
  				</ul>
  			</fieldset>
  		</div>

  		<button id="submitForm" class="btn btn-gold" type="submit">
  			<span class="usa-search-submit-text usa-button-big" style="color: white;">Submit</span>
  		</button>

  		<div id="formFlash"></div>
  	</form>
  </div>
  <!-- advisoryboard subscription form -->
  <script>
  (function(e, t, n, a, c) {a = e.createElement(t), c = e.getElementsByTagName(t)[0], a.async = 1, a.src = n, c.parentNode.insertBefore(a, c)})(document, "script", "https://mktgsres.advisory.com/resources/lp_snippet/prod/lplib.js");
  window.addEventListener ? window.addEventListener("load", init, false) : window.attachEvent && window.attachEvent("onload", init);

  function init() {
    lplib.set('uw_med_pb7', 'index.php');
    lplib.sendReferrer();
  }
  </script>
  <!-- /advisoryboard subscription form  -->
</article>
