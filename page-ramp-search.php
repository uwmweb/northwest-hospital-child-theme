<?php
/*
Template Name: Search Page
*/
?>
<?php \Roots\Sage\Setup\define_current_template('page-ramp-search.php'); ?>

<div class="page-header">
  <h1>Search Results</h1>
</div>
<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
} //if

$location_search_url = "locations?query=" . $search_query["query"] . "&restrict=clinics";
$location_search_url = home_url($location_search_url, 'relative');

$provider_search_url = "bios#query=" . $search_query["query"];
$provider_search_url = home_url($provider_search_url, 'relative');

/* Exclude the Locations and Services from WP Search */
$search_meta_query_args = array(
  'relation' => 'AND',
  array(
    'relation' => 'OR',
    array(
      'key' => 'ramp_service_page_mainPage',
      'compare' => 'NOT EXISTS',
    ),
    array(
      'key' => 'ramp_service_page_mainPage',
      'value' => 0,
      'compare' => '=',
    ),
  ),
  array(
    'relation' => 'OR',
    array(
      'key' => 'ramp_load_clinic_page_clinic_mainPage',
      'compare' => 'NOT EXISTS',
    ),
    array(
      'key' => 'ramp_load_clinic_page_clinic_mainPage',
      'value' => 0,
      'compare' => '=',
    )
  ),
);

$posts_per_page = 10;
$search_query_args = array(
  's' => trim($search_query["query"]),
  'posts_per_page' => $posts_per_page,
  'meta_query' => $search_meta_query_args,
); ?>

<?php if( $search_query['query'] ) : ?>
	<?php $wp_search = new WP_Query( $search_query_args );
	$wp_num_results = $wp_search->found_posts;

	/* Load Ramp Search Results */
	$ramp_search = ramp_search();
	$locations = $ramp_search["clinics"];
  $num_locations = $locations ? count($locations) : 0;

  $providers = $ramp_search["providers"];
  $num_providers = $providers ? count($providers) : 0;

  $services = $ramp_search["services"];
  $num_services = $services ? count($services) : 0;

  $num_results = $wp_num_results + $num_locations + $num_providers + $num_services;
	?>
<?php endif ?>

<?php if(!$wp_search) : ?>
	<div class="search">
		<?php get_search_form(); ?>
	</div>
<?php else : ?>
  <div class="search">
    <?php get_search_form(); ?>
  </div>
  <p class="search-terms">Your search for "<strong><?php echo $search_query["query"]; ?></strong>" returned <span id="num-search-results"><?php echo $num_results; ?></span> results.</p>
	<div class="row">
    <div class="col-md-4 col-md-push-8">
      <div class="location-search-results">
        <?php include(locate_template('templates/content-search-location.php')); ?>
        <hr />
      </div><!--/ .location-search-results -->

      <div class="provider-search-results">
          <?php include(locate_template('templates/content-search-provider.php')); ?>
          <hr class="visible-xs-block visible-md-block" />
      </div><!-- /.provider-search-results -->
    </div><!--/.col-md-4 col-md-push-8 -->
	  <div class="col-md-8 col-md-pull-4 page-search-results">
      <?php if($services) : ?>
        <div class="services-search-results">
          <?php include(locate_template('templates/content-search-services.php')); ?>
        </div>
        <hr />
      <?php endif ?>

      <h2>General Search Results</h2>
      <?php if (!$wp_search->have_posts() ) : ?>
      <div class="alert alert-warning">
        <p><?php _e('Sorry, no results were found.', 'sage'); ?></p>
      </div><!-- /.alert alert-warning -->
      <?php endif ?><!-- /if (!$wp_search->have_posts() ) -->

      <?php if( $wp_search->have_posts()) : ?>
    		<?php while ($wp_search->have_posts()) : $wp_search->the_post(); ?>
  		  	<?php $search_type = "search-" . get_post_type();?>
  		  	<?php get_template_part('templates/content', $search_type); ?>
  	    <?php endwhile; ?>
    		<?php if( $wp_num_results > $posts_per_page ) : ?>
    			<p>
    				<a class="btn btn-blue" href="<?php echo trailingslashit( home_url('/') );?>page/2/?s=<?php echo urlencode($search_query['query']); ?>">More Results</a>
    			</p>
		    <?php endif ?><!-- /if( $wp_num_results > $posts_per_page ) -->
      <?php endif ?><!-- /if( $wp_search->have_posts()) -->
      </div><!--/ .search -->
     </div><!-- /page-search-results -->

  </div><!-- /.row -->
<?php endif ?><!-- /if(!$wp_search) -->

<?php
$medical_service_page_args = array(
	'name'           => 'medical-services',
	'post_type'      => 'page',
	'post_status'    => 'publish',
	'posts_per_page' => 1
);
$medical_service_pages = get_posts($medical_service_page_args);
$medical_service_page_id = wp_list_pluck($medical_service_pages, 'ID');
$service_pages_args = array(
	'post_parent'    => $medical_service_page_id[0],
	'post_type'      => 'page',
	'post_status'    => 'publish',
  'posts_per_page' => -1
);
$service_pages = get_posts($service_pages_args);
$services_pages_cleaned = [];
foreach($service_pages as $page) {
  $service_pages_cleaned[] = array("ID" => $page->ID, "post_title" => $page->post_title, "post_content" => $page->post_content, "permalink" => get_permalink($page));
}

$enabled_clinics = ramp_facilities_linked();

?>

<script id="suggestion-template" type="text/x-handlebars-template">
  {{#if suggestedExpertises}}
    Are you searching for a provider who specializes in {{#listSuggestions suggestedExpertises}}{{/listSuggestions}}
  {{/if}}
</script>

<script id="suggested-providers-template" type="text/x-handlebars-template">
  {{#if suggestedProviders}}
    {{#displaySuggestedProviders suggestedProviders}}{{/displaySuggestedProviders}}
  {{/if}}
</script>

<script>
jQuery(document).ready(function() {
  if(sessionStorage.getItem("suggestedExpertises")) {
    window.suggestedExpertises = JSON.parse(sessionStorage.getItem("suggestedExpertises"));

    Handlebars.registerHelper('listSuggestions', function(context, options) {
      var ret = "";

      if(context.length > 1 ) {
        for(var i=0, j=context.length; i<j; i++ ) {
          if( i > 0 && i !== j - 1 ) {
             ret = ret + ", ";
          } else if (i === 1 && j === 2) {
            ret = ret + " or ";
          } else if (i === j - 1) {
            ret = ret + ", or ";
          }
          ret = ret + "<a href='/bios/#expertise=" + context[i]['id'] + "'>" + context[i]['name'] + "</a>";
        };
      } else {
        ret = ret + "<a href='/bios/#expertise=" + context[0]['id'] + "'>" + context[0]['name'] + "</a>";
      };

      return ret + "?";
    });

    if(window.suggestedExpertises.length > 0 ) {
      var suggestionTemplateScript = jQuery("#suggestion-template").html();
      var suggestionTemplate = Handlebars.compile(suggestionTemplateScript);
      var suggestionContext = {suggestedExpertises: window.suggestedExpertises}
      var compiledSuggestions = suggestionTemplate(suggestionContext);
      jQuery(".suggested-expertise-search").html(compiledSuggestions);
      jQuery(".suggested-expertise-search").addClass("alert alert-warning");
    }
  }

  if(sessionStorage.getItem("suggestedProviders")) {
    window.suggestedProviders = JSON.parse(sessionStorage.getItem("suggestedProviders"));
    Handlebars.registerHelper('displaySuggestedProviders', function(context, options) {
      var homeURL = "//" + location.hostname;
      var ret ="";
      for(var i=0, j=context.length; i<j; i++ ) {
        var imgSrc = context[i].bioImageUrl;
        if(imgSrc === "http://apps.uwmedicine.org/sharedcontent/media/images/noimage.jpg") {
          imgSrc = "/wp-content/themes/northwest-hospital-child-theme/dist/images/provider-image-not-available.jpg"
        }
        ret = ret + "<div class='provider-entry'>" +
        "<h3><a href='" + homeURL + "/bios/" + context[i].friendlyUrl + "'>" + context[i].fullName + "</a></h3>" +
        "<a href='" + homeURL + "/bios/" + context[i].friendlyUrl + "'>" +
          "<img src='" + imgSrc + "' alt='" + context[i].fullName + "' />" +
        "</a>" +
        "<p>" + context[i].bioIntro + "</p>" +
        "</div>";
      };
      ret = ret + "<a id='provider-search-button' class='btn btn-blue' href='" + homeURL + "/bios/'>Search All Providers</a>";
      return ret;
    });

    var providerNoResults = jQuery("#provider-no-results");
    if((providerNoResults.length > 0) && (window.suggestedProviders.length > 0) ) {
      var providerData = [];
      var providersLoaded = 0;

      // helper function to fetch provider data
      var getProviderData = function(providerList, callback) {
        providerList.forEach(function(provider) {
          jQuery.getJSON('https://webservices.uwmedicine.org/api/bioinformation?friendlyUrl=' + provider.friendlyUrl, function(data) {
            providerData.push(data[0]);
            providersLoaded++;
            if(providersLoaded === providerList.length) {
              callback();
            }
          });
        });
      }

      var maxProviders = Math.min(window.suggestedProviders.length, 3);
      getProviderData(window.suggestedProviders.slice(0,maxProviders), function() {
        var suggestedProvidersTemplateScript = jQuery("#suggested-providers-template").html();
        var suggestedProvidersTemplate = Handlebars.compile(suggestedProvidersTemplateScript);
        var suggestedProvidersContext = {suggestedProviders: providerData}
        var compiledSuggestedProviders = suggestedProvidersTemplate(suggestedProvidersContext);
        var numSearchResults = parseInt(jQuery("#num-search-results").text()) + providersLoaded;
        jQuery("#num-search-results").text(numSearchResults)
        providerNoResults.remove();
        jQuery(".suggested-providers-returned").html(compiledSuggestedProviders);
        if(window.suggestedProviders.length > maxProviders) {
          var provSearchBtn = jQuery("#provider-search-button");
          var query = "#query=" + encodeURIComponent(location.search.split("=")[1].replace("+", " "));
          provSearchBtn[0].href += query;
        }
      });
    }
  }
  
});
</script>
