<?php \Roots\Sage\Setup\define_current_template('archive-clinic-resources.php'); ?>

<div class="page-header">
  <h1>Clinic Resources</h1>
</div>

<?php
  $terms = get_terms('clinic-resource-categories');
  $term_ids = wp_list_pluck($terms, 'term_id');

  $args = array(
    'posts_per_page' => 0,
    'post_status' => array('publish', 'inherit'),
    'post_type' => 'attachment',
    'orderby' => 'title',
    'order' => 'ASC',
  	'tax_query' => array(
      array(
	      'taxonomy' => 'clinic-resource-categories',
        'field' => 'term_id',
        'terms' => $term_ids
      ),
		),
	);
  $clinic_resource_media = get_posts( $args ); ?>

<?php if (!have_posts() && !$clinic_resource_media) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<h2>Articles</h2>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>

<h2>Forms</h2>
<?php if($clinic_resource_media) : ?>
  <?php foreach($clinic_resource_media as $post) : setup_postdata($post); ?>
    <?php echo the_attachment_link(); ?></li>
  <?php endforeach ?>
<?php endif ?>
