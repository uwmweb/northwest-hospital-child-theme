<?php
/**
 * Template Name: Location Page
 */
?>
<?php use \Roots\Sage\NWH_Extras; ?>
<?php \Roots\Sage\Setup\define_current_template('page-location.php'); ?>



<?php if ( function_exists('yoast_breadcrumb') )
{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
<?php endwhile; ?>

<?php global $location;
$location = ramp_facility_linked(); ?>

<?php $address_string = '';
if($location["LocationName"]) { $address_string .= $location["LocationName"] . ', ' . $location["AddressLine1"]; } else {$address_string .= $location["AddressLine1"];};
if($location["AddressLine2"] ) { $address_string .= " " . $location["AddressLine2"]; }
if($location["RoomNumber"] ) { $address_string .= " " . $location["RoomNumber"]; }
$address_string .= ", " . $location["City"] . ", " . $location["State"] . " " . trim($location["ZipCode"], " "); ?>

<section class="location">

  <?php $number_slider_images = count(get_field('location_slider_images')) ?>
  <?php if ($number_slider_images == 1) : ?>
    <?php while( have_rows('location_slider_images')): the_row();
    $image = get_sub_field('image'); ?>
      <img class="location-photo" src="<?php echo $image["url"] ?>" alt="<?php echo $location["Name"] ?>" />
    <?php endwhile; ?>
  <?php else : ?>
    <div id="location-slider-images" class="carousel slide" data-ride="carousel">
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
      <?php $slide_count = 0; ?>
      <?php while (have_rows('location_slider_images')) : the_row();
        $image = get_sub_field('image');
        ?>
      <?php if ($slide_count === 0) : ?>
      <div class="item active">
      <?php else: ?>
      <div class="item">
      <?php endif ?>
        <img src="<?php echo $image['url'];?>" alt="<?php echo $location['Name'];?>">
      </div><!-- /.item [active] -->
      <?php $slide_count++;
      endwhile; ?>
      </div><!-- /.carousel-inner -->

    <!-- Indicators -->
    <ol class="carousel-indicators">
      <?php $indicator_count = 0;
      while (have_rows('location_slider_images')) : the_row();
        if ($indicator_count === 0) : ?>
      <li data-target="#location-slider-images" data-slide-to="0" class="active"></li>
      <?php else: ?>
      <li data-target="#location-slider-images" data-slide-to="<?php echo $indicator_count; ?>"></li>
      <?php endif;
      $indicator_count++;
      endwhile; ?>
    </ol><!-- /.carousel-indicators -->
  </div><!-- /#homepage-slider-images -->
  <?php endif; ?>

  <div class="contact">
    <p class="address"><?php echo($location["Name"] . "<br />" . $address_string ); ?></p>
    <?php $phone_number = $location["PhoneNumber"]?>
      <?php if($phone_number) :?>
      <p class="phone" >
        <a href="tel:+1<?php echo $phone_number; ?>">
        <?php echo substr($phone_number, 0, 3);
        echo ".";
        echo substr($phone_number, 3, 3);
        echo ".";
        echo substr($phone_number, 6, 4);?></a>
      </p>
      <?php endif ?>
    <p class="buttons">
      <a class="getting-here btn" href="directions">Getting Here</a>
      <a class="map-it btn" target="_blank" title="View this location on a Google Map" class="map" href="https://maps.google.com/maps?q=<?php echo urlencode($address_string); ?>&amp;hnear=<?php echo urlencode($address_string); ?>&amp;t=m&amp;z=14&amp;gl=us/">Map It </a>
    </p>
  </div>
  <div class="main-body">

    <?php $hours_of_operation = $location["HoursOfOperation"]; ?>
    <?php $right_sidebar_widgets = false;
    if($location["ShowAppointmentRequest"] == 1 || $location["ShowReturningPatient"] == 1 || $location["ShowVirtualClinic"] == 1) {
      $right_sidebar_widgets = true;
    }
    $right_sidebar = get_field("right_sidebar");
    $sidebar_mailing_list_form = get_field("sidebar_mailing_list_form");
    ?>
    <?php if( !empty($hours_of_operation) || $right_sidebar_widgets || $right_sidebar || $sidebar_mailing_list_form ) : ?>
      <div class="content">
    <?php else : ?>
      <div class="content-has-sidebar">
    <?php endif ?>
      <?php the_content(); ?>
      <?php $fax_number = $location["FaxNumber"];
      if( $fax_number ) : ?>
      <?php $fax_number_formatted = substr($fax_number, 0, 3) ."." . substr($fax_number, 3, 3) . "." . substr($fax_number, 6, 4); ?>
      <p><?php echo "To fax documents, please use the clinic fax number: " . $fax_number_formatted;?> </p>
      <?php endif ?>
      <?php $main_content_mailing_list_form = get_field("main_content_mailing_list_form");
      if($main_content_mailing_list_form) :?>
      <div class="main-content-maiing-list-fomr">
        <hr> 
          <h3>Mailing List</h3> 
          <p>Join our mailing list to receive future emails about events and updates associated with the <span id="clinicName">clinic</span>.</p> 
          <div class="form-container usa-form">
          <form id="form">
            <div class="row">
            <div class="col-md-6"><label for="firstName">First Name<span class="clinic_form_span">*</span></label> 
            <input id="firstName" name="firstName" type="text"></div> 
            <div class="col-md-6"><label for="lastName">Last Name<span class="clinic_form_span">*</span></label> 
            <input id="lastName" name="lastName" type="text"></div></div> 
            <div class="row"><div class="col-md-9"><label for="email">Email<span class="clinic_form_span">*</span></label> 
            <input id="email" name="email" type="email"></div> 
                <div class="col-md-3"><label for="zip">ZIP</label> 
            <input class="usa-input-medium" id="zip" name="zip" type="text"></div></div> 
            <div class="row"><div class="col-md-3"><button id="submitForm" type="submit" class="btn btn-gold"> 
                <span class="usa-search-submit-text usa-button-big">Submit</span> 
            </button></div></div> 
            <div class="row"><div id="formFlash"></div></div> 
            </div>
          </form>
        </div>
      <?php endif ?>
      
    </div>

    <?php if(!empty($hours_of_operation) || $right_sidebar_widgets || $right_sidebar || $sidebar_mailing_list_form ) : ?>
    <div class="right-sidebar">
      <?php if( $hours_of_operation ) : ?>
      <div class="hours-of-operation">
        <?php $indexed_hours = NWH_Extras\index_hours_of_operation($hours_of_operation); ?>
        <h3>Hours of Operation</h3>
        <table class="table">
          <?php for ($i = 1; $i <= 7; $i++) : ?>
            <?php
            if( $i == 7 ) {
              $entry = $indexed_hours[0];
            } else {
              $entry = $indexed_hours[$i];
            }

            $day = $entry["dayOfWeek"];

            if( !array_key_exists("isClosed", $entry)) :
              $open = strtotime($entry["startTime"]);
              $close = strtotime($entry["endTime"]);
            ?>
            <tr>
              <td><?php echo JDDayOfWeek($day-1, 1); ?></td>
              <td><?php echo (rtrim(date('g:ia', $open), "m") . " - " . rtrim(date('g:ia', $close), "m")); ?></td>
            </tr>
          <?php else : ?>
          <tr>
            <td><?php echo JDDayOfWeek($day-1, 1); ?></td>
            <td>Closed</td>
          </tr>
        <?php endif ?>
        <?php endfor ?>

        </table>
      </div>
      <?php endif ?>

      <?php if($location["ShowAppointmentRequest"] == 1 || $location["ShowReturningPatient"] == 1 || $location["ShowVirtualClinic"] == 1) : ?>
        <div class="uwm-widgets">
          <?php if($location["ShowAppointmentRequest"] == 1) : ?>
            <a class="request-an-appointment widget btn" target="_blank" href="https://www.uwmedicine.org/request-an-appointment?provider=<?php echo (urlencode(trim($provider["FullName"])) . "&" . "providerid=" . $provider["Id"]) ?>">Request an Appointment</a>
          <?php endif ?>
          <?php if ($location["ShowReturningPatient"] == 1) : ?>
            <a class="eCare widget btn" href="http://www.uwmedicineecare.org">Log in to eCare</a>
          <?php endif ?>
          <?php if ($location["ShowVirtualClinic"] == 1) : ?>
            <a class="virtual-care widget btn" href="http://www.uwmedicine.org/locations/virtual-clinic">Get Virtual Care</a>
          <?php endif ?>
        </div>
      <?php endif ?><!-- /ShowAppointmentRequest, ShowReturningPatient -->
      <?php if($sidebar_mailing_list_form) :?>
        <div class="sidebar-mailing-list-form">
        <h3>Mailing List</h3> 
          <p>Join our mailing list to receive future emails about events and updates associated with the <span id="clinicName">clinic</span>.</p> 
          <div class="form-container usa-form"> 
          <form id="form">
            <label for="firstName">First Name<span class="clinic_form_span">*</span></label> 
            <input id="firstName" name="firstName" type="text"> 
            <label for="lastName">Last Name<span class="clinic_form_span">*</span></label> 
            <input id="lastName" name="lastName" type="text"> 
            <label for="email">Email<span class="clinic_form_span">*</span></label> 
            <input id="email" name="email" type="email"> 
            <label for="zip">ZIP</label> 
            <input class="usa-input-medium" id="zip" name="zip" type="text"> 
            <button id="submitForm" type="submit" class="btn btn-gold"> 
                <span class="usa-search-submit-text usa-button-big">Submit</span> 
            </button> 
            <div id="formFlash"></div> 
            </div>
          </form>
        </div>
      <?php endif ?>

      <?php if( $right_sidebar ) : ?>
        <div class="right-sidebar-content">
          <?php echo $right_sidebar; ?>
        </div>
      <?php endif ?>
    </div>
  <?php endif ?>
  </div>
</section>
<!-- advisoryboard subscription form -->
<script>
(function(e, t, n, a, c) {a = e.createElement(t), c = e.getElementsByTagName(t)[0], a.async = 1, a.src = n, c.parentNode.insertBefore(a, c)})(document, "script", "https://mktgsres.advisory.com/resources/lp_snippet/prod/lplib.js");
window.addEventListener ? window.addEventListener("load", init, false) : window.attachEvent && window.attachEvent("onload", init);

function init() {
  lplib.set('uw_med_pb7', 'index.php');
  lplib.sendReferrer();
}
</script>
<!-- /advisoryboard subscription form  -->