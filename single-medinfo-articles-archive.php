<?php
\Roots\Sage\Setup\define_current_template('single-medinfo-articles-archive.php'); ?>


<p id="breadcrumbs">
  <span xmlns:v="http://rdf.data-vocabulary.org/#">
    <span typeof="v:Breadcrumb">
      <a href="http://nwhospital.wpengine.com" rel="v:url" property="v:title">Home</a> &raquo;
      <span rel="v:child" typeof="v:Breadcrumb">
        <a href="<?php echo trailingslashit(home_url('medinfo')) ?>" rel="v:url" property="v:title">MedInfo</a> &raquo;
            <span class="breadcrumb_last">
              Archive
            </span>
      </span>
    </span>
  </span>
</p>

<article <?php post_class(); ?>>
  <header>
    <h1 class="entry-title"><?php the_title(); ?></h1>
</header>

  <?php the_content(); ?>

<?php $medinfo_editions = get_terms('medinfo-edition'); ?>
  <?php if ($medinfo_editions) : ?>
    <div id="archives" class="row">
      <ul class="list-unstyled">
  <?php foreach ($medinfo_editions as $edition ) : ?>
        <li class="col-md-3"><a href="<?php echo trailingslashit(home_url('medinfo-edition/' . $edition->slug)); ?>"><?php echo $edition->name; ?></a></li>
  <?php endforeach ?>
    </ul>
  </div>
<?php endif ?>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          Print Publication Archives
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <div class="row">
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2016</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/medinfo-2016-spring.pdf" target="_blank">Spring 2016</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2016_Winter.pdf" target="_blank" rel="">Winter 2016</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2015</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/fall_2015.pdf" target="_blank" rel="">Fall 2015</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/summer_2015.pdf" target="_blank" rel="">Summer 2015</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2015_Spring.pdf" target="_blank" rel="">Spring 2015</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2014</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2014_Winter.pdf" target="_blank" rel="">Winter 2014</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2014-Fall.pdf" target="_blank" rel="">Fall 2014</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2014-Summer.pdf" target="_blank" rel="">Summer 2014</a></li>
</ul>
</div>
<div class="clearfix"></div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2013</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2013_Winter.pdf" target="_blank" rel="">Winter 2013</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2013_Fall.pdf" rel="">Fall 2013</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2013_Summer.pdf" target="_blank" rel="">Summer 2013</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2013_Spring.pdf" target="_blank" rel="">Spring 2013</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2013_Jan_Feb.pdf" target="_blank" rel="">January/February 2013</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2012</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2012_Sept_Oct.pdf" target="_blank" rel="">September/October 2012</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2012_July_Aug.pdf" target="_blank" rel="">July/August 2012</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2012_May_June.pdf" target="_blank" rel="">May/June 2012</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2012_Mar_Apr.pdf" target="_blank" rel="">March/April 2012</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2012_Jan_Feb.pdf" target="_blank" rel="">January/February 2012</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2011</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2011_Nov_Dec.pdf" target="_blank" rel="">November/December 2011</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2011_Sept_Oct.pdf" target="_blank" rel="">September/October 2011</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2011_July_Aug.pdf" target="_blank" rel="">July/August 2011</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2011_May_June.pdf" target="_blank" rel="">May/June 2011</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2011_Mar_Apr.pdf" target="_blank" rel="">March/April 2011</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2011_Jan_Feb.pdf" target="_blank" rel="">January/February 2011</a></li>
</ul>
</div>
<div class="clearfix"></div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2010</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2010_Nov_Dec.pdf" target="_blank" rel="">November/December 2010</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2010_Sept_Oct.pdf" target="_blank" rel="">September/October 2010</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2010_July_August-1.pdf" target="_blank" rel="">July/August 2010</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2010_May_June.pdf" target="_blank" rel="">May/June 2010</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2010_Mar_Apr.pdf" target="_blank" rel="">March/April 2010</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2010_Jan_Feb.pdf" target="_blank" rel="">January/February 2010</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2009</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2009_Nov_Dec.pdf" target="_blank" rel="">November/December 2009</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2009_Sept_Oct.pdf" target="_blank" rel="">September/October 2009</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2009_July_August.pdf" target="_blank" rel="">July/August 2009</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2009_May_June.pdf" target="_blank" rel="">May/June 2009</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2009_Mar_Apr.pdf" target="_blank" rel="">March/April 2009</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2009_Jan_Feb.pdf" target="_blank" rel="">January/February 2009</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2008</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2008_Nov_Dec.pdf" target="_blank" rel="">November/December 2008</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2008_Sept_Oct.pdf" target="_blank" rel="">September/October 2008</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2008_July_Aug.pdf" target="_blank" rel="">July/August 2008</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2008_May_June.pdf" rel="">May/June 2008</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2008_Mar_Apr.pdf" rel="">March/April 2008</a></li>
</ul>
</div>
<div class="clearfix"></div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2007</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2007_Nov_Dec.pdf" target="_blank" rel="">November/December 2007</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2007_Sept_Oct.pdf" target="_blank" rel="">September/October 2007</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2007_July_August.pdf" target="_blank" rel="">July/August 2007</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2007_May_June.pdf" target="_blank" rel="">May/June 2007</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/March_April07_Medinfo_small.pdf" target="_blank" rel="">March/April 2007</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2007_Jan_Feb.pdf" target="_blank" rel="">January/February 2007</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<h2>2006</h2>
<ul>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2006_November_December.pdf" target="_blank" rel="">November/December 2006</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/SeptemberOctober2006Final.pdf" target="_blank" rel="">September/October 2006</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/JulyAugust2006.pdf" target="_blank" rel="">July/August 2006</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2006_May_June.pdf" target="_blank" rel="">May/June 2006</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2006_Mar_Apr-1.pdf" target="_blank" rel="">March/April 2006</a></li>
 	<li><a href="https://nwhospital.org/wp-content/uploads/about-northwest-hospital/news/medinfo-magazine/medinfo-magazine-archive/2006_Jan_Feb.pdf" rel="">January/February 2006</a></li>
</ul>
</div>
</div>
      </div>
    </div>
  </div>
</div>

</article>
