<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\NWH_Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="lobby-screen">
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <div class="content row">
        <main class="main">
            <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
    </div><!-- /.content -->
  </body>
</html>
