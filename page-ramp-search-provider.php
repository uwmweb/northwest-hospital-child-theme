<?php
/*
Template Name: Provider Search Page

The loading of this template is determined by page-bios.php
*/
?>

<?php \Roots\Sage\Setup\define_current_template('page-ramp-search-provider.php'); ?>
<?php get_template_part('templates/page', 'header'); ?>
<script>window.sessionStorage.setItem("isProviderSearchPage", "true");</script>
<?php
//$providers = get_transient('all_providers');
//if( false === $providers ) {
  $ramp_search = ramp_search('providers');
  $providers = $ramp_search["providers"];
  //set_transient('all_providers', $providers, 2 * HOUR_IN_SECONDS );
//}
?>

<?php if (http_response_code() == 404 ) : ?>
  <div class="alert alert-warning">
    <p>Sorry, no providers matched the provided URL.</p>
  </div>
<?php endif ?>

<div id="paginated-provider-search">
  <div class="search">
    <?php get_template_part('templates/search-form', 'providers'); ?>
  </div>

  <?php $search_term = get_query_var("query"); ?>
  <?php if( $search_term ) : ?>
    <p class="search-terms">You Searched For: "<strong><?php echo get_query_var("query"); ?></strong>". Search returned <?php echo count($providers); ?> results.</p>
  <?php endif ?>

  <?php if(count($providers) > 10 ) : ?>
  <p class="providers-pagination-label hidden-xs">Page: </p>
  <ul class="providers-paginationTop list-inline"></ul>
<?php endif ?>
  <p class="alert alert-warning" id="no-results" style="display:none">Sorry, no results found. Please try your search again with different search terms or filters, or <a href="/bios">browse a full list of providers</a>.</p>
  <ul class="providers list list-unstyled row">
    <?php foreach($providers as $provider ) : ?>
      <?php include(locate_template('templates/content-providers.php')); ?>
    <?php endforeach ?>
  </ul>
<?php if(count($providers) > 10 ) : ?>
  <p class="providers-pagination-label hidden-xs">Page: </p>
  <ul class="providers-paginationBottom list-inline"></ul>
<?php endif ?>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.2.0/list.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.pagination.js/0.1.1/list.pagination.min.js"></script>

<script>var numResults = <?php echo(count($providers)); ?>;</script>
