<?php \Roots\Sage\Setup\define_current_template('taxonomy-patient-story-category.php'); ?>

<?php if ( function_exists('yoast_breadcrumb') )
{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

<?php
  global $wp_query;
  $term = $wp_query->get_queried_object();
  $title = $term->name; ?>

<div class="page-header">
  <h1><?php echo $title; ?> Patient Stories</h1>
</div>
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<div class="patient-story-row">
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>
</div>
