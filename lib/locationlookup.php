<?php

namespace Roots\Sage\LocationLookup;

function get_location($unique_name) {
  $stored_locations = array();

  // $stored_locations["gabriel-aldea"] = array(0 => array(
  //     Id => "5975",
  //     Name => "Hip & Knee Center at Northwest Outpatient Medical Center",
  //     UniqueName => "hip-knee-nwh-opmc",
  //     LocationName => "Northwest Outpatient Medical Center",
  //     AddressLine1 => "10330 Meridian Ave. N",
  //     AddressLine2 => "",
  //     RoomNumber => "",
  //     City => "Seattle",
  //     State => "WA",
  //     ZipCode => "98133",
  //     PhoneNumber => "2065205000",
  //     FaxNumber => "2063686361",
  //     Latitude => "47.7045743",
  //     Longitude => "-122.3328024",
  //     PageId => "1727",
  //     WpUrl => "http://nwhospital.staging.wpengine.com/locations/hip-knee-center/",
  //     ImageUrl => "https://webservices.uwmedicine.org/api/Location/2000129/picture",
  //     IsPrimary => True,
  //   ));

  $serialized_locations = file_get_contents(get_stylesheet_directory() . '/lib/serialized_locations.txt');
  $stored_locations = unserialize($serialized_locations);

  return $stored_locations[$unique_name];
}
