<?PHP

namespace Roots\Sage\NWH_Setup;

use Roots\Sage\NWH_Assets;
use Roots\Sage\NWH_Extras;

register_nav_menus(array(
  'footer_visitor_menu' => __('Footer Visitor Menu', 'nwh'),
  'footer_administrative_menu'   => __('Footer Administrative Menu', 'nwh')
));

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar(array(
    'name'          => __('eCare Button', 'nwh'),
    'id'            => 'ecare-button',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ));

  register_sidebar(array(
    'name'          => __('Header Right', 'nwh'),
    'id'            => 'header-right',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ));

  register_sidebar(array(
    'name'          => __('Footer Contact Information', 'nwh'),
    'id'            => 'footer-contact-information',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ));

  register_sidebar(array(
    'name'          => __('Make a Gift Button', 'nwh'),
    'id'            => 'make-a-gift',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ));
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function nwh_display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, array(
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_search(),
    is_page_template('page-ramp-search.php'),
    is_page_template('page-ramp-search-provider.php'),
    is_page_template('page-ramp-search-location.php'),
    is_page_template('archive-medinfo-articles.php'),
    is_page_template('page-landing.php'),
    is_page_template('page-lobby-screen-events.php')
  ));

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('nwh', NWH_Assets\asset_path('styles/main.css'), false, null);

  wp_enqueue_style('fa', '//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
  wp_enqueue_script('trumba', '//www.trumba.com/scripts/spuds.js');
  wp_enqueue_script('picturefill', '//cdn.rawgit.com/scottjehl/picturefill/master/dist/picturefill.min.js');

  wp_enqueue_script('nhw/js', NWH_Assets\asset_path('scripts/nwh.js'), array('jquery'), null, true);

  wp_enqueue_script('nwh_typeahead-handlebars/js', 'https://cdn.jsdelivr.net/g/typeahead.js@0.11.1,handlebarsjs@4.0.5', array('jquery'), null, true);
  wp_enqueue_script('nwh_searchTypeahead/js', NWH_Assets\asset_path('scripts/nwh_searchTypeahead.js'), array('nwh_typeahead-handlebars/js'), null, true);
  
  if(is_page_template("page-bios.php")) {
    wp_enqueue_script('nwh_providerSearch/js', NWH_Assets\asset_path('scripts/nwh_providerSearch.js'), array('nwh_typeahead-handlebars/js'), null, true);
  }

  if(is_page_template("page-location.php")) {
    wp_enqueue_script('jQuery_valdiate/js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.js', array('jquery'), null, true);
    wp_enqueue_script('nwh_locationMailingListForm/js', NWH_Assets\asset_path('scripts/nwh_locationMailingListForm.js'), array('jquery'), null, true);
  }

  if(is_page_template("page-ramp-search-location.php")) {
    wp_enqueue_script('nwh_google-map-api/js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAClfj1QHSxjcwOYbXdrwT4w9ocopzoJo0');
    wp_enqueue_script('nwh_locationSearch/js', NWH_Assets\asset_path('scripts/nwh_locationSearch.js'));
  }
  
  $searchData = array(
    'enabledClinics' => NWH_Extras\get_enabled_clinics(),
    'servicePages' => NWH_Extras\get_medical_services()
  );
  wp_localize_script('nwh_searchTypeahead/js', 'php_vars', $searchData);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 101);

/**
 * Image sizes
 */
add_image_size( 'xxl', 1800, 1800, false );
add_image_size( 'xl', 1200,  1200, false );

/**
 * Increase max image size included in srcset, to accomodate sliders
 */
 function remove_max_srcset_image_width($max_width) {
   return false;
 }
 add_filter('max_srcset_image_width', __NAMESPACE__ . '\\remove_max_srcset_image_width');

/*
 * Turn off default 'link to' when adding an image to a page
 */

function imagelink_setup() {
  $image_link_to = get_option( 'image_default_link_type' );
  if ($image_link_to !== 'none') {
    update_option('image_default_link_type', 'none');
  }
}
add_action('admin_init', __NAMESPACE__ . '\\imagelink_setup', 10);

/*
 * Set number of posts to display on patient stories and press releases
 * archive pages.
 */
function set_number_of_posts_displayed( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_post_type_archive( 'press-releases' ) ) {
        // Display 50 posts on the 'press releases' archive page
        $query->set( 'posts_per_page', 10 );
        return;
    }

    if ( is_post_type_archive( 'patient-stories' ) ) {
        // Display 4 posts on the 'patient stories' archive page
        $query->set( 'posts_per_page', 4 );
        $query->set( 'orderby', 'menu_order');
        $query->set( 'order', 'DESC');
        return;
    }

    if ( is_post_type_archive( 'clinic-resources' ) ) {
        // Display all posts on the 'health resources' archive page
        $query->set( 'posts_per_page', 0 );
        $query->set( 'orderby', 'title');
        $query->set( 'order', 'ASC');
        return;
    }
}
add_action( 'pre_get_posts', __NAMESPACE__ . '\\set_number_of_posts_displayed', 1 );

/* Add Google Tag Manager Code */
function add_googletagmanager() { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KPKFJXQ');</script>
<!-- End Google Tag Manager -->

<?php }
add_action('wp_head', __NAMESPACE__ . '\\add_googletagmanager');


/**
 * Remove Location and Services Pages from results
 *
 * Since we have custom functions to search for locations and Services
 * with the ramp_access() plugin, we want to remove these two types of results
 * from main page search results.
 *
 * In general, the standard wordpress search will only be used to display the
 * second page of search results or a search of only pages.
 */
function remove_location_services_from_query( $query ) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
      $search_meta_query_args = array(
        'relation' => 'AND',
        array(
          'relation' => 'OR',
          array(
            'key' => 'ramp_service_page_mainPage',
            'compare' => 'NOT EXISTS',
          ),
          array(
            'key' => 'ramp_service_page_mainPage',
            'value' => 0,
            'compare' => '=',
          ),
        ),
        array(
          'relation' => 'OR',
          array(
            'key' => 'ramp_load_clinic_page_clinic_mainPage',
            'compare' => 'NOT EXISTS',
          ),
          array(
            'key' => 'ramp_load_clinic_page_clinic_mainPage',
            'value' => 0,
            'compare' => '=',
          )
        )
      );

      $query->set('meta_query', $search_meta_query_args);
    }
  }
}
add_action('pre_get_posts', __NAMESPACE__ . '\\remove_location_services_from_query');

/**
 * Register Query Vars for Custom Lat/Lng Search
 */

 function add_query_vars_filter($vars) {
   array_push($vars, "uLat", "uLng", "uZip", "uDist");
   return $vars;
 }
 add_filter( 'query_vars', __NAMESPACE__ . '\\add_query_vars_filter');