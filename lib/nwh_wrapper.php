<?php
namespace Roots\Sage\NWH_Wrapper;

use Roots\Sage\Wrapper;
use Roots\Sage\Setup;

function nwh_sidebar_path_move_to_bottom() {
  $current_template = Setup\get_current_template();

  if ($current_template == ('page-providers.php')) {
    return new Wrapper\SageWrapping('templates/sidebar-provider.php');
  }
  else {
    return false;
  }
}

function nwh_sidebar_path_move_to_top() {
  $current_template = Setup\get_current_template();

  if (($current_template == 'content-single-patient-stories.php') ||
      ($current_template == 'archive-patient-stories.php') ||
      ($current_template == 'taxonomy-patient-story-category.php')) {
    return new Wrapper\SageWrapping('templates/sidebar-patient-stories.php');
  }
  elseif (($current_template == 'content-single-press-releases.php') ||
          ($current_template == 'archive-press-releases.php')) {
    return new Wrapper\SageWrapping('templates/sidebar-press-releases.php');
  }
  elseif (($current_template == 'content-single-clinic-resources.php') ||
          ($current_template == 'archive-clinic-resources.php') ||
          ($current_template == 'taxonomy-clinic-resource-categories.php')) {
    return new Wrapper\SageWrapping('templates/sidebar-clinic-resources.php');
          }
  elseif(($current_template == 'page-ramp-search.php') ||
         ($current_template == 'page-ramp-search-provider.php') ||
         ($current_template == 'page-ramp-search-location.php')) {
    // turn off provider and location search templates until we have filters
    return false;
  }
  elseif(($current_template == 'archive-medinfo-articles.php') || 
          ($current_template == 'page-lobby-screen-events.php') ||
         ($current_template == 'page-landing.php')) {
    return false;
    //no sidebar on these pages
  }
  elseif(($current_template == 'content-single-medinfo-articles.php') ||
          ($current_template == 'taxonomy-medinfo-edition.php') ||
          ($current_template == 'single-medinfo-articles-archive.php') ||
          ($current_template == 'single-medinfo-articles-subscribe.php')) {
    return new Wrapper\SageWrapping('templates/sidebar-medinfo-articles.php');
  }
  else {
    return new Wrapper\SageWrapping('templates/sidebar-on-page-navigation.php');
  }
}

add_filter('sage/wrap_base', __NAMESPACE__ . '\nwh_wrap_full_width');

function nwh_wrap_full_width($templates) {
  $home = is_front_page();
  if($home) {
    array_unshift($templates, 'base-home.php');
  }

  $medinfo = is_page_template('archive-medinfo-articles.php');
  if( $medinfo ) {
    array_unshift($templates, 'base-full-width-fluid.php');
  }

  $landingPage = is_page_template('page-landing.php');
  if( $landingPage ) {
    array_unshift($templates, 'base-full-width.php');
  }
  return $templates;
}

add_filter('sage/wrap_base', __NAMESPACE__ . '\nwh_wrap_lobby_screen');

function nwh_wrap_lobby_screen($templates) {
  $lobby_screen_events = is_page_template('page-lobby-screen-events.php');
  if( $lobby_screen_events ) {
    array_unshift($templates, 'base-lobby-screen.php');
  }
  return $templates;
}