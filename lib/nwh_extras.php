<?php
namespace Roots\Sage\NWH_Extras;

use Roots\Sage\NWH_Setup;

/**
 * Add <body> classes
 */
function nwh_body_class($classes) {

  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (NWH_Setup\nwh_display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  // Add class to identify provider individual page since the main template
  // for bios is page-bios.php
  if(is_page_template('page-bios.php')) {
    $provider = ramp_provider();
    if( $provider ) {
      $classes[] = 'page-template-page-providers-php providers';

      if( empty($provider['Locations']) ) {
        //$classes[] = 'hide-sidebar';
      }
    } else {
      $classes[] = 'page-template-page-ramp-search-provider-php';
      $classes[] = 'hide-sidebar';
    }
  }

  // Add full-width class if page uses full-width base (as specified by wrapper)
  if(is_front_page() || is_page_template('archive-medinfo-articles.php') || is_page_template("page-lobby-screen-events.php")) {
    $classes[] = 'full-width';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\nwh_body_class', 5);


/* Ability to check for pages based on ancestor, helpful for adding
 * theme conditionals without creating a custom template.
 */
function is_descendant($ancestor, $tofind = 0) {
    global $post;
    if( $post ) {
      if ($tofind == 0) $tofind = $post->ID;
      $arrpostids = get_post_ancestors($tofind);
      $arrpostslugs = array();
      foreach($arrpostids as $postid) {
          $temppost = get_post($postid);
          array_push($arrpostslugs, $temppost->post_name);
      }
      return (in_array($ancestor, $arrpostids) || in_array($ancestor, $arrpostslugs));
    }
}

/**
 * Check for presence of hide_providers_and_locations_navigation and determine
 * value of field if it does exist.
 */
function add_medical_services_nav ($post_id) {
  $hide_field = get_field('hide_providers_and_locations_navigation', $post_id);

  // if null, see if it's a medical services page
  if( $hide_field == null ) {
    if( is_descendant('medical-services') ) {
      // show if the field is uninitialized and this is a medical services page
      return true;
    } else {
      // don't show if field is uninitialized and this is NOT a medical services page
      return false;
    }
  }
  elseif( $hide_field == true) {
    // do not add the medical services nav
    return false;
  }
  elseif( $hide_field == false ) {
    // show because the field is initialized and set to false
    return true;
  }
}

function add_provider_location_buttons($post_id) {
  $children = get_children(array('post_parent'=>$post_id, 'post_status'=>'published'));
  $found_provider;
  $found_location;
  $found_directions;
  // check if page has a provider child page
  if( $children ) {
    foreach( $children as $child ) {
      if(strpos($child->title, "provider")) {
        $found_provider = $chlid->ID;
      }
      elseif(strpos($child->title, "location")) {
        $found_location = $child->ID;
      }
      elseif(strpos($child->title, "direction")) {
        $found_directions = $child->ID;
      }
    }
  }

  //echo $found_provider . $found_location . $found_direction;
  // check if page has a location child page
}

/**
 * Clean up the_excerpt()
 */
function nwh_excerpt_more() {
  if ( is_post_type_archive( 'patient-stories' ) || is_tax('patient-story-category') ) {
    // Remove the 'more' text from the patient story entry
    return ' &hellip; <a href="' . get_permalink() . '">' . __('', 'sage') . '</a>';
  }
  else {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
  }
}
add_filter('excerpt_more', __NAMESPACE__ . '\\nwh_excerpt_more', 100);

/**
 * Generate a link to the provider search or provider page
 */
function get_provider_url($name = NULL ) {
  $ramp_provider_slug = get_option('ramp_provider_page');

  if( is_null($name) ) {
    return trailingslashit(home_url($ramp_provider_slug));
  }
  else {
    return trailingslashit(home_url($ramp_provider_slug . "/" . $name));
  }
}

/**
 * Return provider image or generate dummy image link
 */
function get_provider_image($url) {
  if (strpos($url, 'noimage.jpg') !==FALSE ) {
    $return_url = get_stylesheet_directory_uri() . "/dist/images/provider-image-not-available.jpg";
    return $return_url;
  } else {
    return $url;
  }
}

/**
 * Given an array with hours of operation entries, build an array of entries
 * indexed by dayOfWeek and add 'isClosed' flag for days with no hours.
 */

function index_hours_of_operation($open_hours) {

  if( count($open_hours) > 0 ) {
    $hours_of_operation_by_day = array();

    foreach( $open_hours as $open_entry ) {
      $day = $open_entry["dayOfWeek"];
      $hours_of_operation_by_day[$day] = $open_entry;
    }

    $days_per_week = 7;
    for( $i = 0; $i < $days_per_week; $i++ ) {
      if( !array_key_exists($i, $hours_of_operation_by_day) ) {
        $hours_of_operation_by_day[$i] = array('dayOfWeek' => $i, 'isClosed' => true );
      }
    }

    return $hours_of_operation_by_day;
  }
}

function get_enabled_clinics() {
  $enabled_clinics = get_transient('enabled_clinics');
  if( false === $enabled_clinics ) {
    $enabled_clinics = ramp_facilities_linked();
    set_transient('enabled_clinics', $enabled_clinics, 2 * DAY_IN_SECONDS );
  }
  return $enabled_clinics;
}

function get_medical_services() {
  $medical_service_section_parent_page_id = false; //get_transient('medical_service_section_parent_page_id');

  if( false === $medical_service_section_parent_page_id ) {
    $medical_service_section_parent_page_args = array(
    	'name'           => 'medical-services',
    	'post_type'      => 'page',
    	'post_status'    => 'publish',
    	'posts_per_page' => 1
    );
    $medical_service_section_parent_page = get_posts($medical_service_section_parent_page_args);
    $medical_service_section_parent_page_id = wp_list_pluck($medical_service_section_parent_page, 'ID');
    $medical_service_section_parent_page_id = $medical_service_section_parent_page_id[0];
    set_transient('medical_service_section_parent_page_id', $medical_service_section_parent_page_id, 5 * DAY_IN_SECONDS);
  }
  $assembled_medical_service_pages = false; //get_transient('assembled_medical_service_pages');

  if( false === $assembled_medical_service_pages ) {
    $all_medical_service_pages_args = array(
    	'post_parent'    => $medical_service_section_parent_page_id,
    	'post_type'      => 'page',
    	'post_status'    => 'publish',
      'posts_per_page' => -1
    );
    $all_medical_service_pages = get_posts($all_medical_service_pages_args);
    $assembled_medical_service_pages = [];
    foreach($all_medical_service_pages as $page) {
      $assembled_medical_service_pages[] = array("ID" => $page->ID, "post_title" => $page->post_title, "post_content" => $page->post_content, "permalink" => get_permalink($page));
    }
    set_transient('assembled_medical_service_pages', $assembled_medical_service_pages);
  }

  return $assembled_medical_service_pages;
}

// Check if request is pointing to a bio page, return 404 header if provider page doesn't exist

function bad_provider_request_redirect() {
  if (preg_match('/\/bios\//', $_SERVER['REQUEST_URI'])) {
    // the url has /bios/ in it
    if ( preg_match('/\/bios\/\w+/', $_SERVER['REQUEST_URI']) ) {
      // the url has letters (as opposed to ? or # ) after /bios/      
      $provider = ramp_provider();

      if( !$provider ) {
        status_header(404);
      }
    }
  }
}
add_action('template_redirect', __NAMESPACE__ . '\\bad_provider_request_redirect' );